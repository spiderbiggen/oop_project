/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.assertTrue;
import game.Randomness;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Yuri
 *
 */
public class RandomnessTest {

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link game.Randomness#randomNumber()}.
     */
    @Test
    public void testRandomNumber() {
        Randomness.setNoTest(0);
        int n = Randomness.randomNumber();
        int i = Randomness.randomNumber();
        assertTrue(n != i);
    }
    
    /**
     * Test method for {@link game.Randomness#randomNumber()}.
     */
    @Test
    public void testNoTest1RandomNumber(){
        Randomness.setNoTest(1);
        assertTrue(Randomness.getNoTest() == 1);
    }
    
    /**
     * Test method for {@link game.Randomness#randomNumber()}.
     */
    @Test
    public void testNoTest5RandomNumber(){
        Randomness.setNoTest(5);
        assertTrue(Randomness.getNoTest() == 5);
    }
    
    /**
     * Test method for {@link game.Randomness#randomNumber()}.
     */
    @Test
    public void testNoTest25RandomNumber(){
        Randomness.setNoTest(25);
        assertTrue(Randomness.getNoTest() == 25);
    }
    
    /**
     * Test method for {@link game.Randomness#randomNumber()}.
     */
    @Test
    public void testNoTest50RandomNumber(){
        Randomness.setNoTest(50);
        assertTrue(Randomness.getNoTest() == 50);
    }
    
    /**
     * Test method for {@link game.Randomness#randomNumber()}.
     */
    @Test
    public void testNoTest75RandomNumber(){
        Randomness.setNoTest(75);
        assertTrue(Randomness.getNoTest() == 75);
    }
    
    /**
     * Test method for {@link game.Randomness#randomNumber()}.
     */
    @Test
    public void testNoTest100RandomNumber(){
        Randomness.setNoTest(100);
        assertTrue(Randomness.getNoTest() == 100);
    }
    
    

}
