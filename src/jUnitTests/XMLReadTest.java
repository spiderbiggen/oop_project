/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.*;
import game.Competition;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Karan
 *
 */
public class XMLReadTest {

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link game.XMLRead#getBooleanValue(org.w3c.dom.Element, java.lang.String)}
     * .
     */
    @Test
    public void testGetDateValue() {
        Competition comp = Competition.readCompetition("testRead.xml", true, null);
    }

    /**
     * Test method for
     * {@link game.XMLRead#getDateValue(org.w3c.dom.Element, java.lang.String)}.
     */
    
    @Test
    public void testGetFloatValue() {
        Competition comp = Competition.readCompetition("testRead.xml", true, null);
        float pay = comp.getTeams().get(0).getBudget();
        float budg = (float) 1000000.0;
        assertTrue(pay == budg);
    }
    /**
     * Test method for
     * {@link game.XMLRead#getFloatValue(org.w3c.dom.Element, java.lang.String)}
     * .
     */
    @Test
    public void testGetIntValue() {
        Competition comp = Competition.readCompetition("testRead.xml", true, null);
        int att = comp.getTeams().get(0).getPlayers().get(0).getStats().getOffense();
        int attack = 45;
        int def = comp.getTeams().get(0).getPlayers().get(0).getStats().getDefense();
        int defense = 31;
        int sta = comp.getTeams().get(0).getPlayers().get(0).getStats().getStamina();
        int stamina = 92;
        assertEquals(att, attack);
        assertEquals(def,defense);
        assertEquals(sta,stamina);
    }

    /**
     * Test method for
     * {@link game.XMLRead#getTextValue(org.w3c.dom.Element, java.lang.String)}.
     */
    @Test
    public void testGetTextValue() {
        Competition comp = Competition.readCompetition("testRead.xml", true, null);
        char testside = comp.getTeams().get(0).getPlayers().get(0).getStats().getSide();
        char sid = 'M';
        String testname = comp.getTeams().get(0).getPlayers().get(0).getName();
        String name = "Roland Alberg";
        assertEquals(testside,sid);
        assertEquals(testname,name);
    }

    /**
     * 
     * Test method for {@link game.XMLRead#parseXMLFile(java.lang.String)}.
     */
    @Test
    public void testParseXMLFile() {
        Competition comp = Competition.readCompetition("testRead.xml", true, null);
        int gks = comp.getTeams().get(0).getPlayers().get(0).getStats().getGkSkill();
        int gskill = 74;
        assertEquals(gks,gskill);
    }

}
