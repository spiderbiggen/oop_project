/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import game.BaseGame;
import game.Competition;
import game.Match;
import game.Team;
import game.ai.lineUp;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Stefan
 *
 */
public class MatchTest {

    Competition comp;
    Competition comp2;
    Team        team1;
    Team        team2;
    Team        team3;
    Match       match1;
    Match       match2;
    Date        date1;
    Date        date2;
    lineUp      lineup;

    /**
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        comp = Competition.readCompetition("competition.xml", true, null);
        BaseGame.setComp(comp);
        team1 = BaseGame.getComp().getTeams().get(0);
        team2 = BaseGame.getComp().getTeams().get(4);
        date1 = BaseGame.getCurrentDate();
        date2 = new Date(15, 3, 15); 
        date2.setMonth(3);
        match1 = new Match(date1, team1, team2);
        match2 = new Match(date2, 3, 2, team2, team1);
        lineup = new lineUp(4, 3, 3);
    }

    /**
     * Test method for
     * {@link game.Match#writeMatch(game.Match, org.w3c.dom.Document, org.w3c.dom.Element)}
     * and {@link game.Match#readMatch(org.w3c.dom.Element)}.
     */
    @Test
    public void testWriteReadMatch() {
        //Tested through competition

    }

    /**
     * Test method for {@link game.Match#compareTo(game.Match)}.
     */
    @Test
    public void testCompareToBefore() {
        match2 = new Match(new Date(date1.getTime() + 5l), team2, team1);
        assertEquals(-1, match1.compareTo(match2));
    }

    /**
     * Test method for {@link game.Match#compareTo(game.Match)}.
     */
    @Test
    public void testCompareToSame() {
        match2 = new Match(new Date(date1.getTime()), team2, team1);
        assertEquals(0, match1.compareTo(match2));
    }

    /**
     * Test method for {@link game.Match#compareTo(game.Match)}.
     */
    @Test
    public void testCompareToAfter() {
        match2 = new Match(new Date(date1.getTime() - 5l), team2, team1);
        assertEquals(1, match1.compareTo(match2));
    }

    /**
     * Test method for {@link game.Match#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObjectSame() {
        assertTrue(match1.equals(match1));
    }

    /**
     * Test method for {@link game.Match#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObjectNull() {
        assertFalse(match1.equals(null));
    }

    /**
     * Test method for {@link game.Match#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObjectDifferent() {
        assertFalse(match1.equals(match2));
    }

    /**
     * Test method for {@link game.Match#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObjectNotMatch() {
        assertFalse(match1.equals(team1));
    }

    /**
     * Test method for {@link game.Match#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObjectSameData() {
        Match match3 = new Match(date1, team1, team2);
        assertTrue(match1.equals(match3));
    }

    /**
     * Test method for {@link game.Match#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObjectDifferentPlayed() {
        Match match3 = new Match(date1, 0, 2, team3, team2);
        assertFalse(match2.equals(match3));
    }
    
    /**
     * Test method for {@link game.Match#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObjectDifferentHomeGoals() {
        Match match3 = new Match(date1, 2, 2, team3, team2);
        assertFalse(match2.equals(match3));
    }

    /**
     * Test method for {@link game.Match#getAwayGoals()}.
     */
    @Test
    public void testGetAwayGoals() {
        assertEquals(match2.getAwayGoals(), 2);
    }

    /**
     * Test method for {@link game.Match#setScore(int, int)}.
     */
    @Test
    public void testSetZero() {
        match1.statsZero();
        assertEquals(-1, match1.getAwayGoals());
        assertEquals(-1, match1.getHomeGoals());
    }

    /**
     * Test method for {@link game.Match#setScore(int, int)}.
     */
    @Test
    public void testSetScore() {
        match1.setScore(3, 2);
        assertEquals(match1.getAwayGoals(), 2);
        assertEquals(3, match1.getHomeGoals());
    }

    /**
     * Test method for {@link game.Match#setShots(int, int)}.
     */
    @Test
    public void testSetShots() {
        match1.setShots(10, 15);
        assertEquals(match1.getAwayShots(), 15);
        assertEquals(10, match1.getHomeShots());
    }

    /**
     * Test method for {@link game.Match#getAwayRed()}.
     * Test method for {@link game.Match#setAwayRed(int)}.
     */
    @Test
    public void testSetGetAwayRed() {
        match2.setAwayRed(3);
        assertEquals(3, match2.getAwayRed());
    }

    /**
     * Test method for {@link game.Match#getAwayShots()}.
     * Test method for {@link game.Match#setAwayShots(int)}.
     */
    @Test
    public void testSetGetAwayShots() {
        match2.setAwayShots(3);
        assertEquals(3, match2.getAwayShots());
    }

    /**
     * Test method for {@link game.Match#getAwayTeam()}.
     * Test method for {@link game.Match#setAwayTeam(Team)}.
     */
    @Test
    public void testSetGetAwayTeam() {
        match2.setAwayTeam(team1);
        assertEquals(team1, match2.getAwayTeam());
    }

    /**
     * Test method for {@link game.Match#getAwayYellow()}.
     * Test method for {@link game.Match#setAwayYellow(int)}.
     */
    @Test
    public void testSetGetAwayYellow() {
        match2.setAwayYellow(5);
        assertEquals(5, match2.getAwayYellow());
    }

    /**
     * Test method for {@link game.Match#getHomeGoals()}.
     * Test method for {@link game.Match#getHomeGoals(int)}.
     */
    @Test
    public void testSetGetHomeGoals() {
        match2.setHomeGoals(5);
        assertEquals(5, match2.getHomeGoals());
    }

    /**
     * Test method for {@link game.Match#getHomeRed()}.
     * Test method for {@link game.Match#setHomeRed(int)}.
     */
    @Test
    public void testSetGetHomeRed() {
        match2.setHomeRed(5);
        assertEquals(5, match2.getHomeRed());
    }

    /**
     * Test method for {@link game.Match#getHomeShots()}.
     * Test method for {@link game.Match#setHomeShots(int)}.
     */
    @Test
    public void testSetGetHomeShots() {
        match2.setHomeShots(3);
        assertEquals(3, match2.getHomeShots());
    }

    /**
     * Test method for {@link game.Match#getHomeTeam()}.
     * Test method for {@link game.Match#setHomeTeam(Team)}.
     */
    @Test
    public void testSetGetHomeTeam() {
        match2.setHomeTeam(team2);
        assertEquals(team2, match2.getHomeTeam());
    }

    /**
     * Test method for {@link game.Match#getHomeYellow()}.
     * Test method for {@link game.Match#setHomeYellow(int)}.
     */
    @Test
    public void testSetGetHomeYellow() {
        match2.setHomeYellow(5);
        assertEquals(5, match2.getHomeYellow());
    }

    /**
     * Test method for {@link game.Match#getPlayDate()}.
     * Test method for {@link game.Match#setPlayDate(Date)}.
     */
    @Test
    public void testSetGetPlayDate() {
        match2.setPlayDate(date1);
        assertEquals(date1, match2.getPlayDate());
    }

    /**
     * Test method for {@link game.Match#isPlayed()}.
     * Test method for {@link game.Match#setPlayed(boolean)}.
     */
    @Test
    public void testIsPlayedTrue() {
        match2.setPlayed(true);
        assertTrue(match2.isPlayed());
    }
    
    /**
     * Test method for {@link game.Match#isPlayed()}.
     * Test method for {@link game.Match#setPlayed(boolean)}.
     */
    @Test
    public void testIsPlayedFalse() {
        match2.setPlayed(false);
        assertFalse(match2.isPlayed());
    }

    /**
     * Test method for {@link game.Match#isPlayed(java.util.Date)}.
     */
    @Test
    public void testIsPlayedDate() {
        assertTrue(match2.isPlayed(new Date(date2.getTime() + 2l)));
    }

    /**
     * Test method for {@link game.Match#statsZero()}.
     */
    @Test
    public void testStatsZero() {
        match2.statsZero();
        assertEquals(match2.getAwayShots(), -1);
        assertEquals(match2.getHomeShots(), -1);
        assertEquals(match2.getHomeYellow(), -1);
        assertEquals(match2.getAwayYellow(), -1);
        assertEquals(match2.getHomeRed(), -1);
        assertEquals(match2.getAwayRed(), -1);
    }

    /**
     * Test method for {@link game.Match#teamSetStats()}.
     */
    @Test
    public void testTeamSetStatsHomeWin() {
        match2.setPlayDate(date1);
        match2.teamSetStats();
        assertEquals(0, BaseGame.getComp().getTeams().get(0).getPoints());
        assertEquals(3, BaseGame.getComp().getTeams().get(4).getPoints());
        assertEquals(1, BaseGame.getComp().getTeams().get(4).getPlayed());
        assertEquals(1, BaseGame.getComp().getTeams().get(0).getPlayed());
        assertEquals(3, BaseGame.getComp().getTeams().get(4).getGoalsFor());
        assertEquals(2, BaseGame.getComp().getTeams().get(4).getGoalsAgainst());
        assertEquals(2, BaseGame.getComp().getTeams().get(0).getGoalsFor());
        assertEquals(3, BaseGame.getComp().getTeams().get(0).getGoalsAgainst());
    }
    
    /**
     * Test method for {@link game.Match#teamSetStats()}.
     */
    @Test
    public void testTeamSetStatsAwayWin() {
        match2.setScore(2,3);
        match2.setPlayDate(date1);
        match2.teamSetStats();
        assertEquals(0, BaseGame.getComp().getTeams().get(4).getPoints());
        assertEquals(3, BaseGame.getComp().getTeams().get(0).getPoints());
        assertEquals(1, BaseGame.getComp().getTeams().get(0).getPlayed());
        assertEquals(1, BaseGame.getComp().getTeams().get(4).getPlayed());
        assertEquals(3, BaseGame.getComp().getTeams().get(0).getGoalsFor());
        assertEquals(2, BaseGame.getComp().getTeams().get(0).getGoalsAgainst());
        assertEquals(2, BaseGame.getComp().getTeams().get(4).getGoalsFor());
        assertEquals(3, BaseGame.getComp().getTeams().get(4).getGoalsAgainst());
    }
    
    /**
     * Test method for {@link game.Match#teamSetStats()}.
     */
    @Test
    public void testTeamSetStatsDraw() {
        match2.setPlayDate(date1);
        match2.setScore(1, 1);
        match2.teamSetStats();
        assertEquals(1, BaseGame.getComp().getTeams().get(0).getPoints());
        assertEquals(1, BaseGame.getComp().getTeams().get(4).getPoints());
        assertEquals(1, BaseGame.getComp().getTeams().get(4).getPlayed());
        assertEquals(1, BaseGame.getComp().getTeams().get(0).getPlayed());
        assertEquals(1, BaseGame.getComp().getTeams().get(4).getGoalsFor());
        assertEquals(1, BaseGame.getComp().getTeams().get(4).getGoalsAgainst());
        assertEquals(1, BaseGame.getComp().getTeams().get(0).getGoalsFor());
        assertEquals(1, BaseGame.getComp().getTeams().get(0).getGoalsAgainst());
    }
    
    /**
     * Test method for {@link game.Match#teamSetStats()}.
     */
    @Test
    public void testTeamSetStatsNotPlayed() {
        match2.setPlayDate(new Date(date1.getTime() + 5l));
        match2.teamSetStats();
        assertEquals(0, BaseGame.getComp().getTeams().get(0).getPoints());
        assertEquals(0, BaseGame.getComp().getTeams().get(4).getPoints());
        assertEquals(0, BaseGame.getComp().getTeams().get(4).getPlayed());
        assertEquals(0, BaseGame.getComp().getTeams().get(0).getPlayed());
        assertEquals(0, BaseGame.getComp().getTeams().get(4).getGoalsFor());
        assertEquals(0, BaseGame.getComp().getTeams().get(4).getGoalsAgainst());
        assertEquals(0, BaseGame.getComp().getTeams().get(0).getGoalsFor());
        assertEquals(0, BaseGame.getComp().getTeams().get(0).getGoalsAgainst());
    }

    /**
     * Test method for {@link game.Match#teamsReady()}.
     */
    @Test
    public void testTeamsReadyTrue() {
        lineUp.autoTeamPos(team1, lineup);
        lineUp.autoTeamPos(team2, lineup);
        match2.setAwayTeam(team1);
        match2.setHomeTeam(team2);
        assertTrue(match2.teamsReady());
    }
    
    /**
     * Test method for {@link game.Match#teamsReady()}.
     */
    @Test
    public void testTeamsReadyFalse() {
        assertFalse(match2.teamsReady());
    }

    /**
     * Test method for {@link game.Match#getHomeInjury()}.
     * Test method for {@link game.Match#setHomeInjury(int)}.
     */
    @Test
    public void testSetGetHomeInjury() {
        match2.setHomeInjury(10);
        assertEquals(10, match2.getHomeInjury());
    }

    /**
     * Test method for {@link game.Match#getAwayInjury()}.
     * Test method for {@link game.Match#setAwayInjury(int)}.
     */
    @Test
    public void testSetGetAwayInjury() {
        match2.setAwayInjury(10);
        assertEquals(10, match2.getAwayInjury());
    }

    /**
     * Test method for {@link game.Match#toString()}.
     */
    @Test
    public void testToString() {
        assertEquals("Match [homeTeam=FC Groningen awayTeam=ADO Den Haag homeGoals=3 awayGoals=2 playDate=Thu Apr 15 00:00:00 CET 1915]", match2.toString());
    }

}
