/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import game.BaseGame;
import game.Competition;
import game.Match;
import game.Player;
import game.Stats;
import game.Team;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Stefan
 *
 */
public class BaseGameTest {

    Stats       stats1;
    Stats       stats2;
    Player      p1;
    Player      p2;
    Team        t1;
    Team        t2;
    Competition comp1;
    Competition comp2;
    Match       m;
    long  date = 1000000L;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        date = sdf.parse("08-08-2013").getTime();
        BaseGame.class.newInstance();
        stats1 = new Stats(80, 40, 70, 60, 0, 15, 'C', 20, 80, 0);
        stats2 = new Stats(40, 80, 65, 60, 0, 15, 'C', 80, 80, 0);
        p1 = new Player("Speler1", 18, "MC", stats1);
        p2 = new Player("Speler2", 18, "MC", stats2);
        t1 = new Team("Ado", "Kees");
        t2 = new Team("Az", "Karel");
        t1.addPlayer(p1);
        t2.addPlayer(p2);
        comp1 = new Competition("Eredivisie");
        comp1.addTeam(t1);
        comp1.addTeam(t2);

        m = new Match(new Date(date), t1, t2);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link game.BaseGame#setComp(game.Competition)} and
     * {@link game.BaseGame#getComp()}.
     */
    @Test
    public void testSetGetComp() {
        BaseGame.setComp(comp1);
        assertEquals(comp1, BaseGame.getComp());

    }

    /**
     * Test method for {@link game.BaseGame#setCurrentDate(java.util.Date)} and
     * {@link game.BaseGame#getComp()}.
     */
    @Test
    public void testSetGetCurrentDate() {
        BaseGame.setCurrentDate(new Date(100200L));
        assertEquals(new Date(100200L), BaseGame.getCurrentDate());
    }

    /**
     * Test method for
     * {@link game.BaseGame#setCurrentTeam(java.lang.String, java.lang.String)}
     * and {@link game.BaseGame#getTeam()}.
     */
    @Test
    public void testSetGetCurrentTeam() {
        BaseGame.setComp(comp1);
        BaseGame.setCurrentTeam("Ado", "Barry");
        t1.setCoach("Barry");
        assertEquals(t1, BaseGame.getTeam());
    }

    /**
     * Test method for {@link game.BaseGame#setTeamName(java.lang.String)} and
     * {@link game.BaseGame#getTeamName()}.
     */
    @Test
    public void testSetGetTeamName() {
        BaseGame.setTeamName("AZ");
        assertEquals("AZ", BaseGame.getTeamName());
    }

    /**
     * Test method for {@link game.BaseGame#BaseGameWrite(java.lang.String)} and
     * {@link game.BaseGame#BaseGameRead(java.lang.String)}.
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     */
    @Test
    public void testBaseGameWriteRead() throws InstantiationException, IllegalAccessException {
        BaseGame.setComp(comp1);
        comp2 = comp1;
        BaseGame.setCurrentTeam("Ado", "Barry");
        BaseGame.setCurrentDate(new Date(date));
        BaseGame.BaseGameWrite("test/test.xml");
        BaseGame.class.newInstance();
        BaseGame.BaseGameRead("test/test.xml");
        comp2.getTeamByName("Ado").setCoach("Barry");
        assertEquals(comp2, BaseGame.getComp());
        assertEquals(new Date(date),BaseGame.getCurrentDate());
    }

    /**
     * Test method for {@link game.BaseGame#getTeamNames()}
     * 
     */
    @Test
    public void testGetTeamNames() {
        BaseGame.setComp(comp1);
        String[] names = { "Ado", "Az" };
        assertArrayEquals(names, BaseGame.getTeamNames());
    }

}
