/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import game.BaseGame;
import game.Match;
import game.Player;
import game.Stats;
import game.Team;
import game.ai.lineUp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Daan
 *
 */
public class TeamTest {
    Team   team1;
    Team   team2;
    Player player1;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        team1 = BaseGame.getTestTeam();
        team2 = BaseGame.getComp().getTeams().get(2);
        String name = "Dirk de boer";
        int offense = 50;
        int defense = 50;
        int stamina = 50;
        int agression = 50;
        int age = 50;
        int injury = 0;
        int backNumber = 11;
        char side = 'L';
        int gkSkill = 50;
        String currentPosition = "MC";
        Stats stats = new Stats(offense, defense, stamina, agression, injury, backNumber, side, gkSkill, 100, 0);
        player1 = new Player(name, age, currentPosition, stats);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link game.Team#addPlayer(game.Player)}.
     */
    @Test
    public void testAddPlayer() {
        team1.addPlayer(player1);
        assertTrue(team1.getPlayers().contains(player1));
    }

    /**
     * Test method for {@link game.Team#compareTo(game.Team)}.
     */
    @Test
    public void testCompareTo() {
        team1.setPoints(20);
        team2.setPoints(30);
        //       System.out.println(team1.compareTo(team2));
        assertEquals(team1.compareTo(team2), 10);
    }

    /**
     * Test method for {@link game.Team#compareTo(game.Team)}.
     */
    @Test
    public void testCompareTo2() {
        team1.setPoints(20);
        team2.setPoints(20);
        //      System.out.println(team1.compareTo(team2));
        assertEquals(team1.compareTo(team2), -22);
    }
    
    

    /**
     * Test method for a bunch of getters and setters
     */
    @Test
    public void testGetAndSet() {
        team1.setBudget(100);
        team1.setGoalsAgainst(20);
        team1.setGoalsFor(40);
        team1.setLost(3);
        team1.setPlayed(50);
        team1.setPoints(40);
        team1.setWon(35);

        assertEquals((int) team1.getBudget(), 100);
        assertEquals(team1.getGoalsAgainst(), 20);
        assertEquals(team1.getGoalsFor(), 40);
        assertEquals(team1.getLost(), 3);
        assertEquals(team1.getPlayed(), 50);
        assertEquals(team1.getPoints(), 40);
        assertEquals(team1.getWon(), 35);
    }

    @Test
    public void testGetGoalKeeper() {
        lineUp.autoTeamPos(team1, new lineUp(4, 3, 3));
        assertEquals("GK", team1.getGoalkeeper().getCurrentPosition());
    }

    @Test
    public void testGetInjuries() {
        Stats stats = player1.getStats();
        stats.setInjury(0);
        player1.setStats(stats);
        team2.addPlayer(player1);
        assertEquals(0, team2.getBestInjuries(3).get(0).getStats().getInjury());
    }

    @Test
    public void testGetPlayerByName() {
        assertEquals(null, team1.getPlayerByName("WrongPlayerName"));
        assertEquals("Roland Alberg", team1.getPlayerByName("Roland Alberg").getName());
    }

    @Test
    public void testGetPlayersByPos() {
        lineUp.autoTeamPos(team1, new lineUp(4, 3, 3));
        assertEquals(team1.getPlayersByPos("M").size(), 3);
        assertEquals(team1.getPlayersByPos("D").size(), 4);

    }

    @Test
    public void testGetTotalOffense() {
        lineUp.autoTeamPos(team1, new lineUp(4, 3, 3));
 //       System.out.println(team1.getTotalOffense());
  //      System.out.println(team1.getTotalDefense());
        assertTrue(team1.getTotalOffense() > 400);
        assertTrue(team1.getTotalDefense() > 400);
    }

    /**
     * Test method for {@link game.Team#readTeam(org.w3c.dom.Element)}.
     */
    @Test
    public void testReadTeam() {

    }

    /**
     * Test method for {@link game.Team#setBudget(float)}.
     */
    @Test
    public void testSetBudget() {
        team1.setBudget(100);
        assertTrue(team1.getBudget() == 100);
    }

    /**
     * Test method for {@link game.Team#testSetPlayers(ArrayList<Player>)}.
     */
    @Test
    public void testSetPlayers() {
        List<Player> list = team1.getPlayers().subList(0, team1.getPlayers().size() - 1);
        team1.setPlayers(list);
    }

    /**
     * Test method for
     * {@link game.Team#Team(java.lang.String, java.lang.String)}.
     */
    @Test
    public void testTeam() {
        team1.setName("AJAX");
        team1.setCoach("BERTJE");
        team2 = new Team("AJAX", "BERTJE");
        assertEquals(team1.getName(), team2.getName());
        assertEquals(team1.getCoach(), team2.getCoach());
    }

    @Test
    public void testToString() {
        team1.toString();
    }

    @Test
    public void testValidateLineUp() {
        assertFalse(team1.validateLineUp());
        lineUp.autoTeamPos(team1, new lineUp(4, 3, 3));
        assertTrue(team1.validateLineUp());
    }


    /**
     * 
     */
    @Test
    public void validatePlayerPositions() {
        Player player1 = team1.getPlayers().get(0);
        Player player2 = team1.getPlayers().get(1);
        player1.setCurrentPosition("MC");
        player2.setCurrentPosition("MC");
        player1.getStats().setCard(0);
        player2.getStats().setCard(0);
        player1.getStats().setInjury(0);
        player2.getStats().setInjury(0);
        
        player1.getStats().setCard(2);
        assertEquals(false, team1.validatePlayerPositions());
        player1.getStats().setCard(0);
        player2.getStats().setCard(1);
        assertEquals(true, team1.validatePlayerPositions());
        player1.getStats().setInjury(3);
        assertEquals(false, team1.validatePlayerPositions());
    }
    
    /**
     * 
     */
    @Test
    public void processMatch() {
        Match m = new Match(new Date(), team1, team2);
        team1.processMatch(m, true);
        team2.processMatch(m, false);
        
        assertNotNull(m.getHomeYellow());
        assertNotNull(m.getHomeRed());
        assertNotNull(m.getHomeInjury());
        assertNotNull(m.getAwayYellow());
        assertNotNull(m.getAwayRed());
        assertNotNull(m.getAwayInjury());

    }

    @Test
    public void getTotalPositions(){
        lineUp lineup = new lineUp(4,3,3);
        lineUp.autoTeamPos(team1, lineup);
        ArrayList<Integer> res = team1.getTotalPositions();       
    }
    
    @Test
    public void testReturnNullGK(){
        assertEquals(team1.getGoalkeeper(),null);
    }
    
    @Test
    public void testEquals(){
        assertEquals(team1,team1);
        assertFalse(team1.equals(player1));
        assertFalse(team1.equals(team2));
        team2.setBudget(team1.getBudget());
        assertFalse(team1.equals(team2));
        team2.setCoach(team1.getCoach());
        assertFalse(team1.equals(team2));
        team2.setGoalsAgainst(team1.getGoalsAgainst());
        assertFalse(team1.equals(team2));
        team2.setGoalsFor(team1.getGoalsFor());
        assertFalse(team1.equals(team2));
        team2.setLost(team1.getLost());
        assertFalse(team1.equals(team2));
        team2.setName(team1.getName());
        assertFalse(team1.equals(team2));
        team2.setPlayed(team1.getPlayed());
        assertFalse(team1.equals(team2));
        team2.setPoints(team1.getPoints());
        assertFalse(team1.equals(team2));
        team2.setWon(team1.getWon());
        assertFalse(team1.equals(team2));
        team2.setPlayers(team1.getPlayers()); 
        assertTrue(team1.equals(team2));
        
    }
    

}
