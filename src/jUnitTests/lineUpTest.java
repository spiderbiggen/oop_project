package jUnitTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import game.BaseGame;
import game.Competition;
import game.Player;
import game.Stats;
import game.Team;
import game.ai.lineUp;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class lineUpTest {
    Team              team   = BaseGame.getTestTeam();

    ArrayList<Player> list   = team.getPlayers();
    Player            player = list.get(0);


    
    
    @Test
    public void autoPlayerPosRawTest() {
        Player temp = player;
        temp.setStats(new Stats(100, 20, 20, 20, 0, 12, 'L', 20, 100, 0));
        //    System.out.println(temp.toString()+ " is now on position: " + temp.getCurrentPosition());
        temp = lineUp.autoPlayerPosRaw(temp);
        //    System.out.println(temp.getCurrentPosition());
        assertEquals(temp.getCurrentPosition(), "FL");
    }

    @Test
    public void autoTeamPosRawTest() {
        Team temp = team;
        //    System.out.println(temp.toString());
        lineUp.autoTeamPosRaw(temp);
        //    System.out.println(temp.toString());
    }

    @Test
    public void autoTeamPosTest() {
        lineUp lineup = new lineUp(4, 3, 3);
        Team temp = team;
              System.out.println("team before positioning: "+temp.toString());
        Team temp2 = lineUp.autoTeamPos(temp, lineup);
        System.out.println("team after positioning:" + temp2.toString());
        assertEquals(temp.getPlayers().size(), temp2.getPlayers().size());
        assertTrue(temp2.validateLineUp());
        //    assertTrue(temp2.);
    }

    @Test
    public void getSubstitutesTest() {
        List<Player> temp = list;
        List<Player> temp2 = lineUp.getSubstitutes(temp);
        assertEquals(temp2.size(), 7);
    }

    @Test
    public void lineUpSetTest1() {
        lineUp lineup1 = new lineUp(2, 2, 6);
        lineup1.setBack(4);
        lineup1.setFront(5);
        lineup1.setMid(1);
        assertEquals(4, lineup1.getBack());
        assertEquals(5, lineup1.getFront());
        assertEquals(1, lineup1.getMid());
    }

    @Test
    public void lineUpTest() {
        new lineUp(4, 3, 3);
    }

    @Test
    public void lineUpTest1() {
        lineUp lineup1 = new lineUp(5, 2, 3);
        assertEquals(5, lineup1.getBack());
        assertEquals(3, lineup1.getFront());
        assertEquals(2, lineup1.getMid());
    }

    @Test
    public void lineUpTest2() {
        lineUp lineup1 = new lineUp(5, 11, -32);
        assertEquals(4, lineup1.getBack());
        assertEquals(3, lineup1.getFront());
        assertEquals(3, lineup1.getMid());
    }

    @Test
    public void removePlayersFromListTest() {
        List<Player> temp = team.getPlayers();
        List<Player> toRemove = temp.subList(0, 5);
        List<Player> list1 = temp.subList(3, 7);

        //       List<Player> list2 = list1.subList(3, 7);
        List<Player> result = lineUp.removePlayersFromList(list1, toRemove);
        for (int i = 0; i < toRemove.size(); i++) {
            if (result.contains(toRemove.get(i))) {
                fail();
            }
        }
    }

    @Test
    public void setPositionListTest1() {
        List<Player> temp = team.getPlayers();
        List<Player> list2 = lineUp.setPositionList(temp, "F");

        assertEquals(list2.size(), temp.size());
        for (int i = 0; i < temp.size() - 1; i++) {
            if (list2.get(i).getCurrentPosition().charAt(0) != 'F') {
                fail();
            }
        }
    }

    @Test
    public void setPositionListTest2() {
        List<Player> temp = team.getPlayers();
        List<Player> list2 = lineUp.setPositionList(temp, "D");

        assertEquals(list2.size(), temp.size());
        for (int i = 0; i < temp.size() - 1; i++) {
            if (list2.get(i).getCurrentPosition().charAt(0) == 'F') {
                fail();
            }
        }
    }

    @Test
    public void testEquals() {
        lineUp lineup = new lineUp(4, 3, 3);
        assertEquals(lineup, lineup);
        assertNotEquals(lineup, new lineUp(5, 1, 4));
        assertNotEquals(lineup, new lineUp(3,3, 4));
        assertNotEquals(lineup, new lineUp(3, 4, 3));
        assertNotEquals(lineup, new lineUp(5, 2, 3));
        assertNotEquals(lineup, new lineUp(4, 1, 5));
        assertEquals(lineup, new lineUp(4,3,4));
        
        assertNotEquals(lineup, player);

    }

    @Test
    public void testSubtract() {
        List<Long> list1 = new ArrayList<Long>();
        list1.add(1L);
        list1.add(2L);
        List<Long> list2 = new ArrayList<Long>();
        list2.add(2L);
        list2.add(3L);
        List<Long> result = lineUp.subtract(list1, list2);
        assertEquals(1, result.size());
        assertEquals(1L, (long) result.get(0));
    }

    @Test
    public void toStringTest() {
        lineUp lineup1 = new lineUp(3, 3, 4);
        //System.out.println(lineup1.toString());
        assertEquals("lineUp [front=4, mid=3, back=3]", lineup1.toString());
    }
    
    @Test
    public void autoLineUpalot(){
        Competition comp = BaseGame.getComp();
        comp.setLineUpAllTeams();
    }
    
    @Test
    public void testAssignSidesByPos(){
        lineUp lineup = new lineUp(4,3,3);
        lineUp.assignSidesByPos(team, lineup , 'J');
    }

}
