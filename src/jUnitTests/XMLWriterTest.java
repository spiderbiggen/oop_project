/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.*;
import game.Competition;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Karan
 *
 */
public class XMLWriterTest {

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link game.XMLWriter#printToFile(org.w3c.dom.Document, java.lang.String)}
     * .
     */
    @Test
    public void testPrintToFile() {
        Competition comp = Competition.readCompetition("testRead.xml", true, null);
        int offense = comp.getTeams().get(0).getPlayers().get(0).getStats().getOffense();
        char sid = comp.getTeams().get(0).getPlayers().get(0).getStats().getSide();
        comp.writeCompetition("testwrite.xml", true, null, null);
        Competition comp2 = Competition.readCompetition("testwrite.xml", true, null);
        int off = comp2.getTeams().get(0).getPlayers().get(0).getStats().getOffense();
        char si = comp.getTeams().get(0).getPlayers().get(0).getStats().getSide();
        assertEquals(offense,off);
        assertEquals(sid,si);
    }

}
