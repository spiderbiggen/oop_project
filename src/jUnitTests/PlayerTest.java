package jUnitTests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import game.BaseGame;
import game.Player;
import game.Stats;
import game.Team;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Timo
 *
 */
public class PlayerTest {
    static Team   t;
    static Player p1;
    Player p2;
    static float  floatdelta  = (float) 0.01; //delta used with float testing
    static double doubledelta = 0.01;        //delta used with double testing

    /**
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        t = BaseGame.getTestTeam();
    }

    /**
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        String name = "Dirk de boer";
        int offense = 50;
        int defense = 50;
        int stamina = 50;
        int agression = 50;
        int age = 50;
        int injury = 0;
        int backNumber = 11;
        char side = 'L';
        int gkSkill = 50;
        String currentPosition = "MC";
        Stats stats = new Stats(offense, defense, stamina, agression, injury, backNumber, side, gkSkill, 100, 0);
        p1 = new Player(name, age, currentPosition, stats);
        p2 = t.getPlayers().get(4);
    }

    /**
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * 
     */
    @Test
    public void testAbbreviationToPosition() {
        String[] abbrevations = { "GK", "DL", "DC", "DR", "ML", "MC", "MR", "FL", "FR", "FC", "SB", "RS", "randomstring" };
        String[] positions = { "Goalkeeper", "Defense left", "Defense center", "Defense right", "Midfield left", "Midfield center", "Midfield right", "Offense left", "Offense right", "Offense center", "Substitute", "Reserve", "Reserve" };
        String[] positionsfromabbrevations = new String[13];
        for (int i = 0; i < 13; i++) {
            positionsfromabbrevations[i] = Player.abbreviationToPosition(abbrevations[i]);
        }

        assertArrayEquals(positions, positionsfromabbrevations);

    }

    /**
     * 
     */
    @Test
    public void testCalculateDefense() {
        p1.setCurrentPosition("MR");
        assertEquals(29.81, p1.calculateDefense(), doubledelta);
        p1.setCurrentPosition("DL");
        assertEquals(72.87, p1.calculateDefense(), doubledelta);
    }

    /**
     * 
     */
    @Test
    public void testCalculateOffense() {
        p1.setCurrentPosition("MR");
        assertEquals(29.81, p1.calculateOffense(), doubledelta);
        p1.setCurrentPosition("DL");
        assertEquals(72.87, p1.calculateOffense(), doubledelta);
    }

    /**
     * 
     */
    @Test
    public void testGetSetAge() {
        int age = 12;
        p1.setAge(age);
        assertEquals(age, p1.getAge());
    }

    /**
     * 
     */
    @Test
    public void testGetSetCurrentPosition() {
        String position = "GK";
        p1.setCurrentPosition(position);
        assertEquals(position, p1.getCurrentPosition());
    }

    /**
     * 
     */
    @Test
    public void testGetSetName() {
        String name = "pieter de groot";
        p1.setName(name);
        assertEquals(name, p1.getName());
    }

    /**
     * 
     */
    @Test
    public void testGetSetSalary() {
        float salary = (float) 10000.00;
        p1.setSalary(salary);
        assertEquals(salary, p1.getSalary(), floatdelta);
    }

    /**
     * 
     */
    @Test
    public void testGetSetStats() {
        int offense = 60;
        int defense = 60;
        int stamina = 60;
        int agression = 60;
        int injury = 1;
        int backNumber = 12;
        char side = 'R';
        int gkSkill = 60;
        Stats newstats = new Stats(offense, defense, stamina, agression, injury, backNumber, side, gkSkill, 100, 0);
        p1.setStats(newstats);
        assertEquals(newstats, p1.getStats());
    }

    /**
     * 
     */
    @Test
    public void testGetSetValue() {
        float value = (float) 10000.00;
        p1.setValue(value);
        assertEquals(value, p1.getValue(), floatdelta);
    }

    /**
     * 
     */
    @Test
    public void testIncreaseCondition() {
        p1.getStats().setCondition(50);
        p1.increaseCondition();
        assertEquals(66, p1.getStats().getCondition());

        p1.getStats().setCondition(100);
        p1.increaseCondition();
        assertEquals(100, p1.getStats().getCondition());
    }

    /**
     * 
     */
    @Test
    public void testIncreaseDefense() {
        p1.getStats().setDefense(50);
        p1.increaseDefense(20);
        assertEquals(70, p1.getStats().getDefense());

        p1.getStats().setDefense(100);
        p1.increaseDefense(20);
        assertEquals(100, p1.getStats().getDefense());
    }

    /**
     * 
     */
    @Test
    public void testIncreaseGkSkill() {
        p1.getStats().setGkSkill(50);
        p1.increaseGKSkill(20);
        assertEquals(70, p1.getStats().getGkSkill());

        p1.getStats().setGkSkill(100);
        p1.increaseGKSkill(20);
        assertEquals(100, p1.getStats().getGkSkill());
    }

    /**
     * 
     */
    @Test
    public void testIncreaseOffense() {
        p1.getStats().setOffense(50);
        p1.increaseOffense(20);
        assertEquals(70, p1.getStats().getOffense());

        p1.getStats().setOffense(100);
        p1.increaseOffense(20);
        assertEquals(100, p1.getStats().getOffense());
    }

    /**
     * 
     */
    @Test
    public void testPlayer() {
        String name = "Dirk de boer";
        int offense = 50;
        int defense = 50;
        int stamina = 50;
        int agression = 50;
        int age = 50;
        int injury = 0;
        int backNumber = 11;
        char side = 'L';
        int gkSkill = 50;
        String currentPosition = "MC";
        Stats stats = new Stats(offense, defense, stamina, agression, injury, backNumber, side, gkSkill, 100, 0);
        p1 = new Player(name, age, currentPosition, stats);

        assertEquals(stats, p1.getStats());
        assertEquals(name, p1.getName());
        assertEquals(age, p1.getAge());
        assertEquals(currentPosition, p1.getCurrentPosition());
    }

    /**
     * 
     */
    @Test
    public void testPositionToAbbreviation() {
        String[] abbrevations = { "GK", "DL", "DC", "DR", "ML", "MC", "MR", "FL", "FR", "FC", "SB", "RS", "RS" };
        String[] positions = { "Goalkeeper", "Defense left", "Defense center", "Defense right", "Midfield left", "Midfield center", "Midfield right", "Offense left", "Offense right", "Offense center", "Substitute", "Reserve", "randomstring" };
        String[] abbrevationsfrompositions = new String[13];
        for (int i = 0; i < 13; i++) {
            abbrevationsfrompositions[i] = Player.positionToAbbreviation(positions[i]);
        }

        assertArrayEquals(abbrevations, abbrevationsfrompositions);
    }

    /**
     * 
     */
    @Test
    public void testReduceCondition() {
        p1.getStats().setCondition(100);
        p1.reduceCondition();
        assertEquals(90, p1.getStats().getCondition());

        p1.getStats().setCondition(0);
        p1.reduceCondition();
        assertEquals(0, p1.getStats().getCondition());
    }

    /**
     * 
     */
    @Test
    public void testToString() {
        assertEquals("Player [name=Dirk de boer, age=50, value=80000.0, salary=9001.0, currentPosition=MC, stats=Stats [offense=50, defense=50, gkSkill=50, stamina=50, condition=100, agression=50, backNumber=11, injury=0, side=L, card=0]]", p1.toString());
    }
    
    @Test
    public void testEquals(){
        assertFalse(p1.equals(t) );
        p1.setSalary(4124123);
        p2.setSalary(23433);
        assertFalse(p1.equals(p2));
        
        
    }

}
