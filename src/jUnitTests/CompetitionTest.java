/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import game.BaseGame;
import game.Competition;
import game.Match;
import game.Player;
import game.Randomness;
import game.Stats;
import game.Team;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * @author Iwan
 *
 */
public class CompetitionTest {
    Competition comp;
    Competition comp2;
    Team team1;
    Team team2;
    Match match1;
    Match match2;
    Date date1;
    Date date2;

    

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        BaseGame.class.newInstance();
        comp = Competition.readCompetition("competition.xml", true, null);
        comp2 = Competition.readCompetition("competition.xml", true, null);
        team1 = BaseGame.getTestTeam();
        team2 = BaseGame.getComp().getTeams().get(2);
        date1 = BaseGame.getCurrentDate();
        date2 = date1;
        date2.setTime(1234512);
        match1 = new Match(date1, team1, team2);
        match2 = new Match(date2, team1, team2);



    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

     
    /**
     * Test method for {@link game.Competition#addMatch(game.Match)}.
     */
    @Test
    public void testAddMatch() {
        comp.addMatch(match1);
        assertTrue(comp.getMatches().contains(match1));
        match1 = new Match(date1, team2, team2);
        assertFalse(comp.getMatches().contains(match1));

    }

    /**
     * Test method for {@link game.Competition#addTeam(game.Team)}.
     */
    @Test
    public void testAddTeam() {
       comp.addTeam(team1);
       assertTrue(comp.getTeams().contains(team1));
       team1 = new Team("newteam","teamjdklsa");
       assertFalse(comp.getTeams().contains(team1));
    }


    /**
     * Test method for {@link game.Competition#Competition(java.lang.String)}.
     */
    @Test
    public void testCompetitionString() {
        comp.setName("Panda");
        Competition comp3 = new Competition("Panda");
        assertEquals(comp.getName(), "Panda");
        assertEquals(comp3.getName(), "Panda");
    }

    /**
     * Test method for
     * {@link game.Competition#getMatchByNameAndDate(java.lang.String, java.util.Date)}
     * .
     */
    @Test
    public void testGetMatchByNameAndDate() {
        comp.addMatch(match1);
        assertEquals(match1, comp.getMatchByNameAndDate("ADO Den Haag", date1));
       // assertTrue(comp.getMatchByNameAndDate(match1.getHomeTeam().getName(), date1).equals(match1));
    }

    /**
     * Test method for {@link game.Competition#getMatches()}.
     */
    @Test
    public void testGetMatches() {
       comp.addMatch(match1);
       comp2.addMatch(match2);
       assertTrue(comp.getMatches().contains(match1));
       assertTrue(comp2.getMatches().contains(match2)); 
    }

    /**
     * Test method for {@link game.Competition#getName()}.
     */
    @Test
    public void testGetName() {
        assertEquals(comp.getName(),"Eredivisie");
    }

    /**
     * Test method for {@link game.Competition#getTeamByName(java.lang.String)}.
     */
    @Test
    public void testGetTeamByName() {
        assertEquals("ADO Den Haag", comp.getTeamByName("ADO Den Haag").getName());
    }

    /**
     * Test method for {@link game.Competition#getTeams()}.
     */
    @Test
    public void testGetTeams() {
        assertTrue(comp.getTeams().contains(team1));
        assertTrue(comp.getTeams().contains(team2)); 
    }

    /**
     * Test method for {@link game.Competition#main(java.lang.String[])}.
     */
    @Test
    public void testMain() {
        Competition.main(null);
        Competition comp = Competition.readCompetition("competition.xml", true, null);
        Competition comp2 = Competition.readCompetition("comp.xml", true, null);
        assertEquals(comp, comp2);
    }

    /**
     * Test method for {@link game.Competition#playMatches(long)}.
     */
    @Test
    public void testPlayMatches() {
        
    }

    /**
     * Test method for {@link game.Competition#roundRobinMatches()}.
     */
    @Test
    public void testRoundRobinMatches() {
        Competition comp3 = new Competition("Henk");
        comp3.addTeam(team1);
        comp3.addTeam(team2);
        Randomness.setNoTest(1);
        comp3.roundRobinMatches();
        Competition comp4 = new Competition("Henk");
        comp4.addTeam(team1);
        comp4.addTeam(team2);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        cal.add(Calendar.DATE, 7);
        Match match1 = new Match(cal.getTime(), team2, team1);
        cal.add(Calendar.DATE, 7);
        Match match2 = new Match(cal.getTime(), team1, team2);
        comp4.addMatch(match1);
        comp4.addMatch(match2);
        assertEquals(comp3.getMatches(),comp3.getMatches());
    }

    /**
     * Test method for {@link game.Competition#setMatches(java.util.ArrayList)}.
     */
    @Test
    public void testSetMatches() {
        ArrayList<Match> m = new ArrayList<Match>();
        m.add(match1);
        m.add(match2);
        comp.setMatches(m);
        assertTrue(comp.getMatches().contains(match1));
        assertFalse(comp2.getMatches().contains(match2));
        
    }

    /**
     * Test method for {@link game.Competition#setName(java.lang.String)}.
     */
    @Test
    public void testSetName() {
        comp.setName("Pandavoetbal");
        assertEquals("Pandavoetbal", comp.getName());
        assertFalse("Koalarugby".equals(comp.getName()));
        assertTrue("Pandavoetbal".equals(comp.getName()));
    }

    /**
     * Test method for {@link game.Competition#setTeams(java.util.ArrayList)}.
     */
    @Test
    public void testSetTeams() {
        ArrayList<Team> t = new ArrayList<Team>()  ;
        t.add(team1);
        t.add(team2);
        comp.setTeams(t);
        assertTrue(comp.getTeams().contains(team1));
        assertTrue(comp.getTeams().contains(team2));
    }

    /**
     * Test method for {@link game.Competition#toString()}.
     */
    @Test
    public void testToString() {
        comp.toString();
    }

    @Test
    public void testGetUnplayedMatches(){
       assertEquals(comp.getMatches().size(),comp.getUnplayedMatches());
    }
    
    @Test
    public void testSetLineUpAllTeams() {
        comp.setLineUpAllTeams();
        comp2.setLineUpAllTeams();
        assertEquals(comp, comp2);
    }
    
    

}
