/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.*;
import game.BaseGame;
import game.Competition;
import game.Player;
import game.Randomness;
import game.Stats;
import game.Team;
import game.ai.BuySellAlgorithm;
import game.ai.lineUp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author daan
 *
 */
public class BuySellAlgorithmTest {

    Team             t1;
    Player           p1;
    Team             t2;
    BuySellAlgorithm buysell;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        BaseGame.class.newInstance();
        BaseGame.setComp(Competition.readCompetition("competition.xml", true, null));
        BaseGame.setCurrentTeam("AFC Ajax", "Henk");
        Randomness.setNoTest(50);
        Stats s1 = new Stats(100, 100, 100, 80, 1, 5, 'M', 70, 100, 0);
        this.p1 = new Player("De Beul", 85, "RS", s1);
        this.t1 = BaseGame.getComp().getTeams().get(0);
        p1.setValue(100000);
        this.t1.addPlayer(p1);
        this.t2 = BaseGame.getComp().getTeams().get(0);
        this.t2.addPlayer(p1);
        buysell = new BuySellAlgorithm();

    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link game.ai.BuySellAlgorithm#Buy(game.Player, bodvalue, game.Team)}.
     */
    @Test
    public void testBuyTrue() {
        buysell.SellPlayerList();
        BaseGame.getComp().getTeams().get(0).setBudget(100000000);
        assertTrue(buysell.Buy(BaseGame.getComp().getTeams().get(0).getPlayers().get(0), 220000, BaseGame.getComp().getTeams().get(1)));
    }

    /**
     * Test method for
     * {@link game.ai.BuySellAlgorithm#BodHighEnough(game.Player, bodvalue)}.
     */
    @Test
    public void testBodHighEnoughVeryHighOffer() {
        assertTrue(BuySellAlgorithm.BodHighEnough(p1, 5000000));
    }

    /**
     * Test method for
     * {@link game.ai.BuySellAlgorithm#BodHighEnough(game.Player, bodvalue)}.
     */
    @Test
    public void testBodHighEnoughDecentOffer() {
        assertTrue(BuySellAlgorithm.BodHighEnough(p1, 110000));
    }

    /**
     * Test method for
     * {@link game.ai.BuySellAlgorithm#BodHighEnough(game.Player, bodvalue)}.
     */
    @Test
    public void testBodHighEnoughTooLowOffer() {
        assertFalse(BuySellAlgorithm.BodHighEnough(p1, 90000));
    }

    /**
     * Test method for {@link game.ai.BuySellAlgorithm#SellPlayer(i)}.
     */
    @Test
    public void testSellPlayerTrue() {
        buysell.SellPlayerList();
        assertTrue(buysell.SellPlayer(0));
    }

    /**
     * Test method for {@link game.ai.BuySellAlgorithm#SellPlayer(i)}.
     */
    @Test
    public void testSellPlayerFalse() {
        buysell.SellPlayerList();
        for (int i = 0; i < 10; i++)
            BaseGame.getTeam().getPlayers().remove(0);
        assertFalse(buysell.SellPlayer(0));
    }
}
