/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.*;

import java.sql.Date;

import game.BaseGame;
import game.Match;
import game.Player;
import game.Randomness;
import game.Stats;
import game.Team;
import game.ai.GameAlgorithm;
import game.ai.lineUp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Daan
 *
 */
public class GameAlgorithmTest {

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        Randomness.setNoTest(50);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link game.ai.GameAlgorithm#amountOfGoalShots(game.Team)}.
     */
    @Test
    public void testAmountOfGoalShots() {
        lineUp li = new lineUp(4, 3, 3);
        Team t = BaseGame.getTestTeam();
        t = lineUp.autoTeamPos(t, li);
        assertTrue(GameAlgorithm.amountOfGoalShots(t) == 9);
    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#didDefenseBlock(game.Team)}.
     */
    @Test
    public void testDidDefenseBlockFalse() {
        Randomness.setNoTest(25);
        lineUp li = new lineUp(4, 3, 3);
        Team t = BaseGame.getTestTeam();
        t = lineUp.autoTeamPos(t, li);
        assertFalse(GameAlgorithm.didDefenseBlock(t));
    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#didDefenseBlock(game.Team)}.
     */
    @Test
    public void testDidDefenseBlockTrue() {
        Randomness.setNoTest(100);
        lineUp li = new lineUp(4, 3, 3);
        Team t = BaseGame.getTestTeam();
        t = lineUp.autoTeamPos(t, li);
        assertTrue(GameAlgorithm.didDefenseBlock(t));
    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#didKeeperBlock(game.Team)}.
     */
    @Test
    public void testDidKeeperBlockTrue() {
        lineUp li = new lineUp(4, 3, 3);
        Team t = BaseGame.getTestTeam();
        t = lineUp.autoTeamPos(t, li);
        assertTrue(GameAlgorithm.didKeeperBlock(t));
    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#didKeeperBlock(game.Team)}.
     */
    @Test
    public void testDidKeeperBlockFalse() {
        Randomness.setNoTest(25);
        lineUp li = new lineUp(4, 3, 3);
        Team t = BaseGame.getTestTeam();
        t = lineUp.autoTeamPos(t, li);
        assertFalse(GameAlgorithm.didKeeperBlock(t));
    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#injury(game.Player)}.
     */
    @Test
    public void testInjuryTrue() {
        Randomness.setNoTest(1);  //erg veel pech
        Stats s1 = new Stats(100, 100, 100, 100, 1, 5, 'M', 70, 100, 0);
        Player p1 = new Player("De Beul", 85, "RS", s1);
        assertTrue(GameAlgorithm.injury(p1));
    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#injury(game.Player)}.
     */
    @Test
    public void testInjuryFalse() {
        Randomness.setNoTest(75);
        Stats s1 = new Stats(100, 100, 100, 80, 1, 5, 'M', 70, 100, 0);
        Player p1 = new Player("De Beul", 85, "RS", s1);
        assertFalse(GameAlgorithm.injury(p1));
    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#Card(game.Player)}.
     */
    @Test
    public void testCardNone() {
        Randomness.setNoTest(50);
        Stats s1 = new Stats(100, 100, 100, 10, 10, 5, 'M', 70, 100, 0);
        Player p1 = new Player("Slappeling", 85, "RS", s1);
        assertTrue(GameAlgorithm.Card(p1) == 0);

    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#Card(game.Player)}.
     */
    @Test
    public void testCardYellow() {
        Randomness.setNoTest(1);
        Stats s1 = new Stats(100, 100, 100, 80, 1, 5, 'M', 70, 100, 0);
        Player p2 = new Player("Wildebras", 85, "RS", s1);
        assertTrue(GameAlgorithm.Card(p2) == 1); //1 == yellow card

    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#Card(game.Player)}.
     */
    @Test
    public void testCardRed() {
        Randomness.setNoTest(1);
        Stats s1 = new Stats(100, 100, 100, 100, 1, 5, 'M', 70, 100, 0);
        Player p3 = new Player("De Beul", 85, "RS", s1);
        assertTrue(GameAlgorithm.Card(p3) == 2); //2 == red card

    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#Card(game.Player)}.
     */
    @Test
    public void testCardTwoYellow() {
        Randomness.setNoTest(1);
        Stats s1 = new Stats(100, 100, 100, 80, 1, 5, 'M', 70, 100, 1);
        Player p2 = new Player("Wildebras", 85, "RS", s1);
        assertTrue(GameAlgorithm.Card(p2) == 2);

    }

    /**
     * Test method for {@link game.ai.GameAlgorithm#playMatch(game.Match)}.
     */
    @Test
    public void playMatchTest() {
        Randomness.setNoTest(25);
        lineUp li = new lineUp(4, 3, 3);

        Team home = BaseGame.getTestTeam();
        home = lineUp.autoTeamPos(home, li);

        Team away = BaseGame.getTestTeam();
        away = lineUp.autoTeamPos(away, li);

        Date d1 = new Date(31, 12, 1995);
        Match m1 = new Match(d1, home, away);

        GameAlgorithm.playMatch(m1);
        
        assertTrue(m1.getHomeGoals() == 7);
        assertTrue(m1.getAwayGoals() == 7);
    }
}
