/**
 * 
 */
package jUnitTests;

import static org.junit.Assert.assertEquals;
import game.Stats;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Timo
 *
 */
public class StatsTest {

    Stats s1;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        int offense = 60;
        int defense = 60;
        int stamina = 60;
        int agression = 60;
        int injury = 1;
        int backNumber = 12;
        char side = 'R';
        int gkSkill = 60;
        s1 = new Stats(offense, defense, stamina, agression, injury, backNumber, side, gkSkill, 100, 0);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link game.Stats#getAgression()}.
     */
    @Test
    public void testGetSetAgression() {
        int i = 42;
        s1.setAgression(i);
        assertEquals(i, s1.getAgression());
    }

    /**
     * Test method for {@link game.Stats#getBackNumber()}.
     */
    @Test
    public void testGetSetBackNumber() {
        int i = 42;
        s1.setBackNumber(i);
        assertEquals(i, s1.getBackNumber());
    }

    
    /**
     * Test method for {@link game.Stats#didNotPlay()}.
     */
    @Test
    public void testDidNotPlay(){
        s1.setCard(3);
        s1.setInjury(3);
        s1.didNotPlay();
        assertEquals(s1.getCard(), 4);
        assertEquals(s1.getInjury(), 2);
        
        s1.setCard(0);
        s1.setInjury(0);
        s1.didNotPlay();
        assertEquals(s1.getCard(), 0);
        assertEquals(s1.getInjury(), 0);
        
        s1.setCard(5);
        s1.didNotPlay();
        assertEquals(s1.getCard(), 0);
        
    }
    
    /**
     * Test method for {@link game.Stats#getCard()}.
     */
    @Test
    public void testGetSetCard() {
        int i = 1;
        s1.setCard(i);
        assertEquals(i, s1.getCard());
    }

    /**
     * Test method for {@link game.Stats#getCondition()}.
     */
    @Test
    public void testGetSetCondition() {
        int i = 42;
        s1.setCondition(i);
        assertEquals(i, s1.getCondition());
    }

    /**
     * Test method for {@link game.Stats#getDefense()}.
     */
    @Test
    public void testGetSetDefense() {
        int i = 42;
        s1.setDefense(i);
        assertEquals(i, s1.getDefense());
    }

    /**
     * Test method for {@link game.Stats#getGkSkill()}.
     */
    @Test
    public void testGetSetGkSkill() {
        int i = 42;
        s1.setGkSkill(i);
        assertEquals(i, s1.getGkSkill());
    }

    /**
     * Test method for {@link game.Stats#getInjury()}.
     */
    @Test
    public void testGetSetInjury() {
        int i = 2;
        s1.setInjury(i);
        assertEquals(i, s1.getInjury());
    }

    /**
     * Test method for {@link game.Stats#getOffense()}.
     */
    @Test
    public void testGetSetOffense() {
        int i = 42;
        s1.setOffense(i);
        assertEquals(i, s1.getOffense());
    }

    /**
     * Test method for {@link game.Stats#getSide()}.
     */
    @Test
    public void testGetSetSide() {
        Character c = 'R';
        s1.setSide(c);
        assertEquals(c.toString(), "" + s1.getSide());
    }

    /**
     * Test method for {@link game.Stats#getStamina()}.
     */
    @Test
    public void testGetSetStamina() {
        int i = 42;
        s1.setStamina(i);
        assertEquals(i, s1.getStamina());
    }

    /**
     * 
     */
    @Test
    public void testStats() {
        int offense = 60;
        int defense = 60;
        int stamina = 60;
        int agression = 60;
        int injury = 1;
        int backNumber = 12;
        char side = 'R';
        int gkSkill = 60;
        Stats newstats = new Stats(offense, defense, stamina, agression, injury, backNumber, side, gkSkill, 100, 0);

        assertEquals(offense, newstats.getOffense());
        assertEquals(defense, newstats.getDefense());
        assertEquals(stamina, newstats.getStamina());
        assertEquals(agression, newstats.getAgression());
        assertEquals(injury, newstats.getInjury());
        assertEquals(backNumber, newstats.getBackNumber());
        assertEquals(side, newstats.getSide());
        assertEquals(gkSkill, newstats.getGkSkill());
    }

    /**
     * 
     */
    @Test
    public void testToString() {
        assertEquals("Stats [offense=60, defense=60, gkSkill=60, stamina=60, condition=100, agression=60, backNumber=12, injury=1, side=R, card=0]", s1.toString());
    }

}
