package game;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.w3c.dom.Document;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

/**
 * @author Karan
 *
 */
public class XMLWriter {

    /**
     * Writes A dom tree to a file.
     * 
     * @param doc {@link Document} the dom tree for the file
     * @param file the Path to the file
     */
    public static void printToFile(Document doc, String file) {

        try {
            
            OutputFormat format = new OutputFormat(doc);
            format.setIndenting(true);

            XMLSerializer serializer = new XMLSerializer(new FileOutputStream(new File(file)), format);

            serializer.serialize(doc);

        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

}
