package game;

import game.ai.GameAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author Stefan
 *
 */
public class Team implements Comparable<Team> {

    private ArrayList<Player> players;

    private String            name;

    private String            coach;
    private float             budget;
    private int               played;

    private int               won;
    private int               lost;
    private int               points;

    private int               goalsFor;
    private int               goalsAgainst;

    /**
     * Initialize a new team object with a coach;
     * 
     * @param name This teams name
     * @param coach This teams coach
     */
    public Team(String name, String coach) {
        players = new ArrayList<Player>();
        this.name = name;
        this.coach = coach;
    }

    /**
     * Creates a new {@link Team} Object from an XML File.
     * 
     * @param el {@link Element}
     * @return {@link Team}
     */
    public static Team readTeam(Element el) {

        String name = XMLRead.getTextValue(el, "teamName");
        String coach = XMLRead.getTextValue(el, "coach");
        Team team = new Team(name, coach);
        team.setBudget(XMLRead.getFloatValue(el, "budget"));

        NodeList nodeList = el.getElementsByTagName("player");
        if (nodeList != null && nodeList.getLength() > 0) {
            for (int i = 0; i < nodeList.getLength(); i++) {

                Element speler = (Element) nodeList.item(i);

                Player p = Player.readPlayer(speler);
                team.addPlayer(p);
            }
        }
        return team;
    }

    /**
     * @param t a {@link Team} Object
     * @param save a {@link Document}
     * @param competition a {@link Element}
     */
    public static void writeTeam(Team t, Document save, Element competition) {
        Element team = save.createElement("team");
        competition.appendChild(team);

        Element name = save.createElement("teamName");
        name.appendChild(save.createTextNode(t.getName()));
        team.appendChild(name);

        Element coach = save.createElement("coach");
        coach.appendChild(save.createTextNode(t.getCoach()));
        team.appendChild(coach);

        Element budget = save.createElement("budget");
        budget.appendChild(save.createTextNode(t.getBudget() + ""));
        team.appendChild(budget);

        for (int i = 0; i < t.getPlayers().size(); i++) {
            Player.writePlayer(t.getPlayers().get(i), save, team);
        }
    }

    /**
     * Adds a player to this team.
     * 
     * @param p The {@link Player} to add to this team
     */
    public void addPlayer(Player p) {
        players.add(p);
    }

    @Override
    public int compareTo(Team o) {
        if (getPoints() - o.getPoints() == 0) {
            if (getGoalsFor() - o.getGoalsFor() == 0) {
                if (o.getGoalsAgainst() - getGoalsAgainst() == 0) {
                    return getName().compareTo(o.getName());
                }
                return getGoalsAgainst() - o.getGoalsAgainst();
            }
            return o.getGoalsFor() - getGoalsFor();
        }
        return o.getPoints() - getPoints();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Team) {
            Team other = (Team) obj;
            if (Float.floatToIntBits(budget) == Float.floatToIntBits(other.budget) 
                    && coach.equals(other.coach) 
                    && goalsAgainst == other.goalsAgainst 
                    && goalsAgainst == other.goalsAgainst 
                    && goalsFor == other.goalsFor 
                    && lost == other.lost 
                    && name.equals(other.name) 
                    && played == other.played 
                    && points == other.points 
                    && won == other.won) {

                for (int i = 0; i < players.size(); i++) {
                    if (!other.players.contains(this.players.get(i))) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
     * A method to get the best X attackers of a team, using the
     * player.COMPARE_BY_OFFENSE comparable
     * 
     * @param amount the amount of players that should be in the list
     * @return the best X attackers of a team
     */
    public List<Player> getBestAttackers(int amount) {
        ArrayList<Player> temp = players;

        Collections.sort(temp, Player.COMPARE_BY_OFFENSE);

        Collections.sort(temp, Player.COMPARE_BY_INJURIES);
        Collections.sort(temp, Player.COMPARE_BY_CARDS);
        return temp.subList(0, amount);
    }

    /**
     * A method to get the best X overall of a team, using the
     * player.COMPARE_BY_DEF_AND_OFF comparable
     * 
     * @param amount the amount of players that should be in the list
     * @return the best X players of a team
     */
    public List<Player> getBestAttDef(int amount) {
        ArrayList<Player> temp = players;

        Collections.sort(temp, Player.COMPARE_BY_DEF_AND_OFF);

        Collections.sort(temp, Player.COMPARE_BY_INJURIES);
        Collections.sort(temp, Player.COMPARE_BY_CARDS);
        return temp.subList(0, amount);
    }

    /**
     * A method to get the best X attackers of a team, using the
     * player.COMPARE_BY_DEFENSE comparable
     * 
     * @param amount the amount of players that should be in the list
     * @return the best X defenders of a team
     */
    public List<Player> getBestDefenders(int amount) {
        ArrayList<Player> temp = players;

        Collections.sort(temp, Player.COMPARE_BY_DEFENSE);

        Collections.sort(temp, Player.COMPARE_BY_INJURIES);
        Collections.sort(temp, Player.COMPARE_BY_CARDS);
        return temp.subList(0, amount);
    }

    /**
     * A method to get the best X keepers of a team, using the
     * player.COMPARE_BY_GKSKILL comparable
     * 
     * @param amount the amount of players that should be in the list
     * @return the best X keepers of a team
     */
    public List<Player> getBestGK(int amount) {
        ArrayList<Player> temp = players;

        Collections.sort(temp, Player.COMPARE_BY_GKSKILL);

        Collections.sort(temp, Player.COMPARE_BY_INJURIES);
        Collections.sort(temp, Player.COMPARE_BY_CARDS);
        return temp.subList(0, amount);
    }

    /**
     * A method to get the best uninjured X players of a team, using the
     * player.COMPARE_BY_INJURIES comparable
     * 
     * @param amount the amount of players that should be in the list
     * @return the best X keepers of a team
     */
    public List<Player> getBestInjuries(int amount) {
        ArrayList<Player> temp = players;

        Collections.sort(temp, Player.COMPARE_BY_INJURIES);

        return temp.subList(0, amount);
    }

    /**
     * @return the budget
     */
    public float getBudget() {
        return budget;
    }

    /**
     * @return the coach
     */
    public String getCoach() {
        return coach;
    }

    /**
     * @return the first player that is a keeper
     */
    public Player getGoalkeeper() {
        for (Player p : players) {
            if (p.getCurrentPosition().equals("GK")) {
                return p;
            }
        }
        return null;
    }

    /**
     * @return the goalsAgainst
     */
    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    /**
     * @return the goalsFor
     */
    public int getGoalsFor() {
        return goalsFor;
    }

    /*
     * /** Returns the arraylist containing all the players
     * @return the arraylist containing the players in this team public
     * ArrayList<Player> getList() { return list; }
     */

    /**
     * @return the lost
     */
    public int getLost() {
        return lost;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the played
     */
    public int getPlayed() {
        return played;
    }

    /**
     * @param s player name
     * @return player with name s
     */
    public Player getPlayerByName(String s) {
        for (Player p : players) {
            if (p.getName().equals(s)) {
                return p;
            }
        }
        return null;
    }

    /**
     * @return the players in this team.
     */
    public ArrayList<Player> getPlayers() {
        return players;
    }

    /**
     * returns a list of all players having this string as position
     * 
     * @param pos the first letter of the position of the player 'F', 'M' or 'D'
     * @return a list of players at this position
     */
    public ArrayList<Player> getPlayersByPos(String pos) {
        ArrayList<Player> res = new ArrayList<Player>();
        for (int i = 0; i < players.size(); i++) {
            Player player = players.get(i);
            if (player.getCurrentPosition().startsWith(pos)) {
                res.add(player);
            }
        }
        return res;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @return the defensivePower of this team
     */
    public double getTotalDefense() {
        double defense = 0;
        for (Player p : players) {
            char position = p.getCurrentPosition().charAt(0);
            if (position == 'M' | position == 'D') {
                defense += p.calculateDefense();
            }
        }
        return defense;
    }

    /**
     * @return the offensivePower of this team
     */
    public double getTotalOffense() {
        double offense = 0;
        for (Player p : players) {
            char position = p.getCurrentPosition().charAt(0);
            if (position == 'M' | position == 'F') {
                offense += p.calculateOffense();
            }
        }
        return offense;
    }

    /**
     * @return the won
     */
    public int getWon() {
        return won;
    }

    /**
     * @param budget the budget to set
     */
    public void setBudget(float budget) {
        this.budget = budget;
    }

    /**
     * @param coach the coach to set
     */
    public void setCoach(String coach) {
        this.coach = coach;
    }

    /**
     * @param goalsAgainst the goalsAgainst to set
     */
    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    /**
     * @param goalsFor the goalsFor to set
     */
    public void setGoalsFor(int goalsFor) {
        this.goalsFor = goalsFor;
    }

    /**
     * @param lost the lost to set
     */
    public void setLost(int lost) {
        this.lost = lost;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param played the played to set
     */
    public void setPlayed(int played) {
        this.played = played;
    }

    /**
     * use a normal list instead of an arraylist to update the list of this team
     * 
     * @param list a list that contains all players of a team
     */
    public void setPlayers(List<Player> list) {
        ArrayList<Player> res = new ArrayList<Player>();
        for (int i = 0; i < list.size(); i++) {
            res.add(list.get(i));
        }
        this.players = res;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * @param won the won to set
     */
    public void setWon(int won) {
        this.won = won;
    }

    @Override
    public String toString() {
        String res = "Team " + name + ": " + coach + System.lineSeparator();
        for (int i = 0; i < players.size(); i++) {
            res += "    " + players.get(i).toString() + System.lineSeparator();
        }
        res = res.substring(0, res.lastIndexOf(System.lineSeparator()));

        return res;
    }

    /**
     * @return true als er maximaal ��n keeper is, 10 veldspelers en 7 mensen op
     *         de bank
     */
    public boolean validateLineUp() {
        int field = 0;
        int goal = 0;
        int bench = 0;
        for (Player p : players) {
            if (!(p.getCurrentPosition() == null || p.getCurrentPosition().equals("RS"))) {
                if (p.getCurrentPosition().equals("GK")) {
                    goal++;
                } else if (p.getCurrentPosition().equals("SB")) {
                    bench++;
                } else {
                    field++;
                }
            }
        }
        return (field == 10 && goal == 1 && bench < 8);
    }

    /**
     * @return true if there are no players with red cards or injurys in the
     *         lineup
     */
    public boolean validatePlayerPositions() {
        for (Player p : players) {
            if ((p.getStats().getCard() > 1 || p.getStats().getInjury() > 0) && !p.getCurrentPosition().equals("RS"))
                return false;
        }
        return true;
    }

    /**
     * @param m the match that is played, will be used to set cards and injurys
     * @param home is the team playing at home?
     */
    public void processMatch(Match m, boolean home) {
        int yellow = 0;
        int red = 0;
        int injury = 0;
        for (Player p : players) {
            String position = p.getCurrentPosition();
            if (!(position.equals("RS") || position.equals("SB"))) {
                p.reduceCondition();
                int card = GameAlgorithm.Card(p);
                if (card == 1)
                    yellow++;
                if (card == 2)
                    red++;
                if (GameAlgorithm.injury(p))
                    injury++;
            } else {
                p.increaseCondition();
                p.getStats().didNotPlay();
            }
        }
        if (home) {
            m.setHomeRed(red);
            m.setHomeYellow(yellow);
            m.setHomeInjury(injury);
        } else
            m.setAwayRed(red);
        m.setAwayYellow(yellow);
        m.setAwayInjury(injury);
    }

    /**
     * @return List of amount of players in certain positions
     */
    public ArrayList<Integer> getTotalPositions() {
        int goalkeepers = 0;
        int offense = 0;
        int midfield = 0;
        int defense = 0;
        int substitutes = 0;
        int reserves = 0;
        String pos = "";
        for (Player p : players) {
            pos = p.getCurrentPosition().substring(0, 1);
            switch (pos) {
                case "G":
                    goalkeepers++;
                    break;
                case "D":
                    defense++;
                    break;
                case "M":
                    midfield++;
                    break;
                case "F":
                    offense++;
                    break;
                case "S":
                    substitutes++;
                    break;
                case "R":
                    reserves++;
                    break;
                 default:
                     break;
            }
        }
        ArrayList<Integer> totals = new ArrayList<Integer>();
        totals.add(goalkeepers);
        totals.add(offense);
        totals.add(midfield);
        totals.add(defense);
        totals.add(substitutes);
        totals.add(reserves);
        return totals;
    }

}
