package game;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Stefan
 *
 */
public class Stats {

    private int  offense;
    private int  defense;
    private int  gkSkill;
    private int  stamina;
    private int  condition;
    private int  agression;
    private int  backNumber;
    private int  injury;
    private char side;
    private int  card;

    /**
     * @param offense the offensive power
     * @param defense the defensive power
     * @param stamina the stamina
     * @param agression the aggressiveness of this player
     * @param backNumber the back number of this player
     * @param injury how injured this player is
     * @param side the side of the field this player prefers
     * @param gkSkill the skill this player has when playing as a goalkeeper.
     * @param condition the condition of this player between 0 and 100
     * @param card the cards this player has
     */
    public Stats(int offense, int defense, int stamina, int agression,
            int injury, int backNumber, char side, int gkSkill, int condition,
            int card) {
        this.offense = offense;
        this.defense = defense;
        this.stamina = stamina;
        this.agression = agression;
        this.injury = injury;
        this.backNumber = backNumber;
        this.side = side;
        this.gkSkill = gkSkill;
        this.condition = condition;
        this.card = card;
    }

    /**
     * @param p a {@link Player} object
     * @param save the Dom tree
     * @param player The document element.
     */
    public static void writeStats(Player p, Document save, Element player) {
        Element attack = save.createElement("offense");
        attack.appendChild(save.createTextNode(p.getStats().getOffense() + ""));
        player.appendChild(attack);

        Element defense = save.createElement("defense");
        defense.appendChild(save.createTextNode(p.getStats().getDefense() + ""));
        player.appendChild(defense);

        Element gkSkill = save.createElement("gkskill");
        gkSkill.appendChild(save.createTextNode(p.getStats().getGkSkill() + ""));
        player.appendChild(gkSkill);

        Element stamina = save.createElement("stamina");
        stamina.appendChild(save.createTextNode(p.getStats().getStamina() + ""));
        player.appendChild(stamina);
        
        Element condition = save.createElement("condition");
        condition.appendChild(save.createTextNode(p.getStats().getCondition() + ""));
        player.appendChild(condition);

        Element agression = save.createElement("agression");
        agression.appendChild(save.createTextNode(p.getStats().getAgression() + ""));
        player.appendChild(agression);

        Element injury = save.createElement("injury");
        injury.appendChild(save.createTextNode(p.getStats().getInjury() + ""));
        player.appendChild(injury);

        Element backn = save.createElement("number");
        backn.appendChild(save.createTextNode(p.getStats().getBackNumber() + ""));
        player.appendChild(backn);
        
        Element side = save.createElement("side");
        side.appendChild(save.createTextNode(p.getStats().getSide() + ""));
        player.appendChild(side);
        
        Element card = save.createElement("card");
        card.appendChild(save.createTextNode(p.getStats().getCard() + ""));
        player.appendChild(card);

    }

    /**
     * Process stats for an unplayed game
     */
    public void didNotPlay(){
        if(injury>0)
            injury--;
        if(card>1)
            card++;
        if(card>4)
            card=0;
    }
    
    /**
     * @return the agression
     */
    public int getAgression() {
        return agression;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Stats) {
            Stats otherstats = (Stats) other;
            if (    this.offense == otherstats.offense
                    & this.defense == otherstats.defense
                    & this.gkSkill == otherstats.gkSkill
                    & this.stamina == otherstats.stamina
                    & this.condition == otherstats.condition
                    & this.agression == otherstats.agression
                    & this.backNumber == otherstats.backNumber
                    & this.injury == otherstats.injury
                    & this.side == otherstats.side
                    & this.card == otherstats.card)
                return true;
        }
        return false;
    }

    /**
     * @return the backnumber
     */
    public int getBackNumber() {
        return backNumber;
    }

    /**
     * @return the cards the player has
     */
    public int getCard() {
        return card;
    }

    /**
     * @return the current condition of this player
     */
    public int getCondition() {
        return condition;
    }

    /**
     * @return the defense
     */
    public int getDefense() {
        return defense;
    }

    /**
     * @return the gkSkill
     */
    public int getGkSkill() {
        return gkSkill;
    }

    /**
     * @return the injury
     */
    public int getInjury() {
        return injury;
    }

    /**
     * @return the offense
     */
    public int getOffense() {
        return offense;
    }

    /**
     * @return the side
     */
    public char getSide() {
        return side;
    }

    /**
     * @return the stamina
     */
    public int getStamina() {
        return stamina;
    }

    /**
     * @param agression the agression to set
     */
    public void setAgression(int agression) {
        this.agression = agression;
    }

    /**
     * @param backnumber the backnumber to set
     */
    public void setBackNumber(int backnumber) {
        backNumber = backnumber;
    }

    /**
     * @param card the card to set
     */
    public void setCard(int card) {
        this.card = card;
    }

    /**
     * @param condition the new condition of the current player
     */
    public void setCondition(int condition) {
        this.condition = condition;
    }

    /**
     * @param defense the defense to set
     */
    public void setDefense(int defense) {
        this.defense = defense;
    }

    /**
     * @param gkSkill the gkSkill to set
     */
    public void setGkSkill(int gkSkill) {
        this.gkSkill = gkSkill;
    }

    /**
     * @param injury the injury to set
     */
    public void setInjury(int injury) {
        this.injury = injury;
    }

    /**
     * @param offense the offense to set
     */
    public void setOffense(int offense) {
        this.offense = offense;
    }

    /**
     * @param side the side to set
     */
    public void setSide(char side) {
        this.side = side;
    }

    /**
     * @param stamina the stamina to set
     */
    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    @Override
    public String toString() {
        return "Stats [offense=" + offense + ", defense=" + defense + ", gkSkill=" + gkSkill + ", stamina=" + stamina + ", condition=" + condition + ", agression=" + agression + ", backNumber=" + backNumber + ", injury=" + injury + ", side=" + side + ", card=" + card + "]";
    }
}
