package game.ai;

import game.Match;
import game.Player;
import game.Randomness;
import game.Team;

/**
 * @author Daan, Yuri
 *
 */
public class GameAlgorithm {
    static Team awayTeam;
    static Team homeTeam;

    /**
     * @param t the team
     * @return how often did they shoot at the goal
     */
    public static int amountOfGoalShots(Team t) {
        double offense = t.getTotalOffense();
        double luck = ((Randomness.randomNumber() + 50.0) / 100.0);
        offense = offense * luck;
        int i = (int) (offense / 50);
        return i;
    }

    /**
     * @param t the team
     * @return chance for the defense to block
     */
    public static boolean didDefenseBlock(Team t) {
        double defense = t.getTotalDefense();
        double luck = ((Randomness.randomNumber() + 50.0) / 100.0);
        defense = defense * luck;

        return defense > 575;

    }

    /**
     * @param t the team
     * @return chance for the keeper to block
     */
    public static boolean didKeeperBlock(Team t) {
        int luck = Randomness.randomNumber();
        int gkscore = t.getGoalkeeper().getStats().getGkSkill();
        int i = luck - (gkscore / 5);

        return i > 30;
    }

    /**
     * @param p chance of a player getting an injury
     * @return true if player gets injured
     */
    public static boolean injury(Player p) {
        double chance = 0.01002 * p.getStats().getAgression();
        if (Randomness.randomNumber() * 0.85 < chance) {
            p.getStats().setInjury(3);
            return true;
        }
        return false;
    }

    /**
     * @param m match to be played
     */
    public static void playMatch(Match m) {
        awayTeam = m.getAwayTeam();
        homeTeam = m.getHomeTeam();

        int awayTeamShots = amountOfGoalShots(awayTeam);
        
        int homeTeamShots = amountOfGoalShots(homeTeam);
        m.setShots(homeTeamShots, awayTeamShots);
        int awayScore = 0;

        while (awayTeamShots > 0) {
            if (!didKeeperBlock(homeTeam) && !didDefenseBlock(homeTeam))
                awayScore++;
            awayTeamShots--;
            
        }
        int homeScore = 0;

        while (homeTeamShots > 0) {
            if (!didKeeperBlock(awayTeam) && !didDefenseBlock(awayTeam))
                homeScore++;
            homeTeamShots--;
        }
        homeTeam.processMatch(m, true);
        awayTeam.processMatch(m, false);
        
        m.setScore(homeScore, awayScore); 
        m.setPlayed(true);  
        
    }

    /**
     * @param p if player gets a yellow card
     * @return 0 if players gets no card, 1 for a yellow, 2 for a red.
     */
    public static int Card(Player p) {
        double ag = p.getStats().getAgression();
        double redcard = ag * 0.01;
        double yellowcard = ag * 0.06;
        int luck = Randomness.randomNumber();
        
        if (luck <= yellowcard && luck > redcard) {
            if (p.getStats().getCard() == 0) {
                p.getStats().setCard(1);
                return 1;
            }
            if (p.getStats().getCard() == 1) {
                p.getStats().setCard(2);
                return 2;
            }
        }
        if (luck <= redcard) {
            p.getStats().setCard(2);
            return 2;
        }
        return 0;
    }

}
