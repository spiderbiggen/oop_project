package game.ai;

import game.BaseGame;
import game.Player;
import game.Randomness;
import game.Team;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Stefan & Iwan
 *
 */
public class BuySellAlgorithm {

    private ArrayList<Player> bidPlayers;
    private ArrayList<Player> players;
    private ArrayList<Team>   teams;
    private ArrayList<Float>  bids;

    /**
     * Checks if the other team agrees with this trade
     * 
     * @param player a {@link Player}
     * @param bodValue the value of the players bid
     * @param otherTeam the {@link Team} you buy from
     * @return true if your bid was accepted false if not
     */
    public boolean Buy(Player player, double bodValue, Team otherTeam) {
        bidPlayers.add(player);
        if (BodHighEnough(player, bodValue) && otherTeam.getPlayers().size() > 18) {
            otherTeam.getPlayers().remove(player);
            BaseGame.getTeam().addPlayer(player);
            BaseGame.getTeam().setBudget(BaseGame.getTeam().getBudget() - (float) bodValue);
            return true;
        } else {
            return false;
        }
    }

    /**
     * checks if the bodValue is enough to buy the player. Returns true if it
     * is, and false if not.
     * 
     * @param p the player that the user bids on
     * @param bodvalue the amount the user offers
     * @return true if your bid was high enough for the other team and player
     */
    public static boolean BodHighEnough(Player p, double bodvalue) {
        double playervalue = p.getValue();
        double procentwaarde = (bodvalue / playervalue) * 100;
        if (procentwaarde >= 90 && procentwaarde < 110) {
            if (Randomness.randomNumber() < (int) (((procentwaarde - 90) * 3) + 20)) {
                return true;
            }
        }
        if (procentwaarde >= 110)
            return true;
        return false;
    }

    /**
     * @param i the index of the player to sell
     * @return true if the player has sold the player false if he didn't have
     *         enough players in his team
     */
    public boolean SellPlayer(int i) {
        if (BaseGame.getTeam().getPlayers().size() > 18) {
            BaseGame.getTeam().getPlayers().remove(players.get(i));
            teams.get(i).addPlayer(players.get(i));
            BaseGame.getTeam().setBudget(BaseGame.getTeam().getBudget() + bids.get(i));

            return true;
        }
        return false;
    }

    /**
     * @return the players
     */
    public ArrayList<Player> getPlayers() {
        return players;
    }

    /**
     * @return the teams
     */
    public ArrayList<Team> getTeams() {
        return teams;
    }

    /**
     * @return the bids
     */
    public ArrayList<Float> getBids() {
        return bids;
    }

    /**
     * Creates a new set of random players usually at the start of a new week(in
     * game)
     */
    public void SellPlayerList() {
        Random rand = new Random();
        bidPlayers = new ArrayList<Player>();
        players = new ArrayList<Player>();
        teams = new ArrayList<Team>();
        bids = new ArrayList<Float>();
        for (int i = 0; i < rand.nextInt(3) + 2; i++) {
            Player p = SelectPlayer();
            players.add(p);
            teams.add(SelectTeam());
            bids.add(getBidValue(p));
        }
    }

    /**
     * used for making random bids by other teams
     * 
     * @return a {@link Player} from the players team
     */
    public Player SelectPlayer() {
        
        
        Player p = null;
        while(p == null){
            int i = (int) Math.floor(Math.random() * BaseGame.getTeam().getPlayers().size());
            p = BaseGame.getTeam().getPlayers().get(i);
            if(players.contains(p)){
                p = null;
            }
        }
        return p;
    }

    /**
     * used for making random bids by other teams
     * 
     * @return a {@link Team} that made the "Bid"
     */
    public Team SelectTeam() {
        int i = (int) Math.floor(Math.random() * BaseGame.getComp().getTeams().size());
        Team a = BaseGame.getComp().getTeams().get(i);
        return a;
    }

    /**
     * used for making random bids by other teams
     * 
     * @param p the player {@link Player} used for the bid value
     * @return a {@link Float} that is the value of the bid.
     */
    public float getBidValue(Player p) {
        float bid = (float) Math.floor((Math.random() * 0.3f + 0.85f) * p.getValue());
        return bid;
    }

    /**
     * removes the player from all lists after he has been sold.
     * 
     * @param i index of the player in the array
     */
    public void remove(int i) {
        players.remove(i);
        teams.remove(i);
        bids.remove(i);
    }

    /**
     * returns the list of {@link Player} Objects that have already been bid on
     * this week
     * 
     * @return the bidPlayers
     */
    public ArrayList<Player> getBidPlayers() {
        return bidPlayers;
    }

}
