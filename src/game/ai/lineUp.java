package game.ai;

import game.Player;
import game.Stats;
import game.Team;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * a lineUpclass used to give a lineup to a team. something like 4-3-3 or 5-2-1
 * used for ai to automatically assign a team in a preferred lineup
 * 
 * excuses for not using a capital letter at lineUp. It might confuse some
 * people
 * 
 * @author sytze
 *
 */
public class lineUp {
    int front;
    int mid;
    int back;

    /**
     * makes a lineup object. if the lineup is not valid (fr+md+bck != 10) it
     * will make a lineup object with a 4-3-3
     * 
     * @param fr amount of players in the front
     * @param md amount of players mid
     * @param bck amount of players in the back
     */
    public lineUp(int bck, int md, int fr) {
        if (fr + md + bck == 10 && fr > 0 && md > 0 && bck > 0) {
            front = fr;
            mid = md;
            back = bck;
        } else {
            front = 3;
            mid = 3;
            back = 4;
        }

    }

    /**
     * runs the assignSidesByPos 3 times, for 'D', 'M' and 'F' as position
     * character so the whole team gets modified
     * 
     * @param team {@link Team} to make a lineup for
     * @param lineup LineUp Format
     */
    public static void assignAllSidesByPos(Team team, lineUp lineup) {
        assignSidesByPos(team, lineup, 'D');
        assignSidesByPos(team, lineup, 'M');
        assignSidesByPos(team, lineup, 'F');
    }

    /**
     * assign a team by sides by looking using the lineup
     * 
     * @param team team that should be modified
     * @param lineup lineup that should me implemented
     * @param pos position you want to apply this method to (should be 'D', 'M'
     *        or 'F')
     */
    public static void assignSidesByPos(Team team, lineUp lineup, char pos) {

        ArrayList<Player> defendersLeft = team.getPlayersByPos(pos + "L");
        ArrayList<Player> defendersMid = team.getPlayersByPos(pos + "C");
        ArrayList<Player> defendersRight = team.getPlayersByPos(pos + "R");

        int def = 4;
        switch (pos) {

            case ('D'):
                def = lineup.getBack();
            break;
            case ('M'):
                def = lineup.getMid();
            break;
            case ('F'):
                def = lineup.getFront();
            break;
        }
        if (pos != 'D' && pos != 'M' && pos != 'F') {
            System.out.println("incorrect input for assignSidesByPos");
            return;
        }
        while (!(defendersLeft.size() == 1 && defendersRight.size() == 1 && defendersMid.size() == def - 2)) {
                defendersLeft = team.getPlayersByPos(pos + "L");
                defendersMid = team.getPlayersByPos(pos + "C");
                defendersRight = team.getPlayersByPos(pos + "R");

                if (defendersLeft.size() > 1) {
                    defendersLeft.get(0).setCurrentPosition(pos + "C");
                } else if (defendersLeft.size() < 1) {
                    if(defendersMid.size()>0){
                        defendersMid.get(0).setCurrentPosition(pos + "L");
                    }else{
                        defendersRight.get(0).setCurrentPosition(pos + "L");
                    }
                } else if (defendersRight.size() > 1) {
                    
                    defendersRight.get(0).setCurrentPosition(pos + "C");
                } else if (defendersRight.size() < 1) {
                    if(defendersMid.size()>0){
                        defendersMid.get(0).setCurrentPosition(pos + "R");
                    }else{
                        defendersLeft.get(0).setCurrentPosition(pos + "R");
                    }
                }
            

        }

    }

    /**
     * sets the currentPosition of a player to a position using the
     * offense/defense/side fields in the players stats. Very raw and only puts
     * a player in front or in the back
     * 
     * @param player the input player
     * @return a player on a position matching the stats
     */
    public static Player autoPlayerPosRaw(Player player) {
        Player res = player;

        Stats stats = res.getStats();

        char side = stats.getSide();
        int offense = stats.getOffense();
        int defense = stats.getDefense();
        int gkSkill = stats.getGkSkill();

        if (gkSkill > offense && gkSkill > defense) {
            res.setCurrentPosition("GK");
        } else if (offense > defense) {
            res.setCurrentPosition("F" + side);
        } else {
            res.setCurrentPosition("D" + side);
        }
        return res;
    }

    /**
     * insert
     * 
     * @param team {@link Team} to set positions for
     * @param lineup Lineup Format
     * @return the best lineup for a team
     */
    public static Team autoTeamPos(Team team, lineUp lineup) {
        Team res = lineUp.autoTeamPosRaw(team);

        int att = lineup.getFront();
        int def = lineup.getBack();
        int mid = lineup.getMid();

        ArrayList<Player> allPlayers = res.getPlayers();

        Player goalkeeper = res.getBestGK(1).get(0);

        List<Player> bestAttackers = res.getBestAttackers(att + 1);
        List<Player> attackersEnd = lineUp.listWithoutPlayer(bestAttackers, goalkeeper).subList(0, att);

        List<Player> bestDefenders = res.getBestDefenders(11);
        ArrayList<Player> defenders = lineUp.listWithoutPlayer(bestDefenders, goalkeeper);

        List<Player> bestMidPlayers = res.getBestAttDef(11);
        ArrayList<Player> midPlayers = lineUp.listWithoutPlayer(bestMidPlayers, goalkeeper);

        //assign the defenders
        List<Player> defendersEnd = lineUp.removePlayersFromList(defenders, attackersEnd).subList(0, def);

        //assign the midplayers
        List<Player> mid1 = lineUp.removePlayersFromList(midPlayers, attackersEnd);
        mid1 = lineUp.removePlayersFromList(mid1, defendersEnd);
        List<Player> midEnd = mid1.subList(0, mid);

        List<Player> substitutes = lineUp.removePlayersFromList(allPlayers, attackersEnd);
        substitutes = lineUp.removePlayersFromList(substitutes, defendersEnd);
        substitutes.remove(goalkeeper);
        substitutes = lineUp.removePlayersFromList(substitutes, midEnd);
        ArrayList<Player> reserves = new ArrayList<Player>();
        ArrayList<Player> substitutes1 = new ArrayList<Player>();
        Collections.sort(substitutes, Player.COMPARE_BY_DEF_AND_OFF);
        Collections.sort(substitutes, Player.COMPARE_BY_CARDS);
        Collections.sort(substitutes, Player.COMPARE_BY_INJURIES);
        for (int i = 0; i < substitutes.size(); i++) {
            if (i < 7) {
                substitutes1.add(substitutes.get(i));
            } else {
                reserves.add(substitutes.get(i));
            }
        }

        //if everything went correct, we now have a few arrays that have no players in common
        attackersEnd = setPositionListWithSide(attackersEnd, "F");
        midEnd = setPositionListWithSide(midEnd, "M");
        defendersEnd = setPositionListWithSide(defendersEnd, "D");
        goalkeeper.setCurrentPosition("GK");
        List<Player> substitutesEnd = setPositionList(substitutes1, "SB");
        List<Player> resEnd = setPositionList(reserves, "RS");

        allPlayers.removeAll(allPlayers);

        allPlayers.add(goalkeeper);
        allPlayers.addAll(attackersEnd);

        allPlayers.addAll(midEnd);
        allPlayers.addAll(defendersEnd);

        allPlayers.addAll(substitutesEnd);
        allPlayers.addAll(resEnd);

        assignAllSidesByPos(res, lineup);

        return res;
    }

    /**
     * uses a for loop to use the autoPlayerPosRaw on the whole team.
     * 
     * @param team input team
     * @return a team where all the players are at a position matching their
     *         stats
     */
    public static Team autoTeamPosRaw(Team team) {
        Team res = team;
        ArrayList<Player> list = res.getPlayers();

        for (int i = 0; i < list.size(); i++) {
            list.set(i, autoPlayerPosRaw(list.get(i)));
        }

        return res;
    }

    /**
     * the best 7 players of a list (for substitutes)
     * 
     * @param list List of {@link Player} Objects
     * @return the best 7 players of a list (for substitutes)
     */
    public static List<Player> getSubstitutes(List<Player> list) {
        Collections.sort(list, Player.COMPARE_BY_DEF_AND_OFF);
        return list.subList(0, 7);
    }

    /**
     * @param inputList 
     * @param player
     * @return
     */
    public static ArrayList<Player> listWithoutPlayer(List<Player> inputList,
            Player player) {
        ArrayList<Player> res = new ArrayList<Player>();
        for (int i = 0; i < inputList.size(); i++) {
            Player temp = inputList.get(i);
            if (!temp.equals(player)) {
                res.add(inputList.get(i));
            }
        }

        return res;
    }

    public static List<Player> removePlayersFromList(List<Player> list1,
            List<Player> list2) {

        return subtract(list1, list2);
    }

    public static List<Player> setPositionList(List<Player> list, String pos) {
        List<Player> res = new ArrayList<Player>();
        for (int i = 0; i < list.size(); i++) {
            Player temp = list.get(i);

            temp.setCurrentPosition("" + pos);

            res.add(temp);
        }

        return res;
    }

    public static List<Player> setPositionListWithSide(List<Player> list,
            String pos) {
        List<Player> res = new ArrayList<Player>();
        for (int i = 0; i < list.size(); i++) {
            Player temp = list.get(i);
            char side = temp.getStats().getSide();
                temp.setCurrentPosition("" + pos + side);
            res.add(temp);
        }

        return res;
    }

    public static <T> List<T> subtract(List<T> list1, List<T> list2) {
        List<T> result = new ArrayList<T>();
        Set<T> set2 = new HashSet<T>(list2);
        for (T t1 : list1) {
            if (!set2.contains(t1)) {
                result.add(t1);
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof lineUp) {
            lineUp newLineUp = (lineUp) obj;
            if (newLineUp.getBack() == getBack() && newLineUp.getFront() == getFront() && newLineUp.getMid() == getMid()) {
                return true;
            }
        }
        return false;
    }

    public int getBack() {
        return back;
    }

    public int getFront() {
        return front;
    }

    public int getMid() {
        return mid;
    }

    public void setBack(int back) {
        this.back = back;
    }

    public void setFront(int front) {
        this.front = front;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    @Override
    public String toString() {
        return "lineUp [front=" + front + ", mid=" + mid + ", back=" + back + "]";
    }
}
