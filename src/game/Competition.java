package game;

import game.ai.GameAlgorithm;
import game.ai.lineUp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author Stefan
 *
 */
public class Competition {

    private ArrayList<Team>  teams;
    private ArrayList<Match> matches;
    private String           name;

    /**
     * creates a new Competition Object
     */
    public Competition() {
        teams = new ArrayList<Team>();
    }

    /**
     * creates a new Competition Object
     * 
     * @param name The name of this competition
     */
    public Competition(String name) {
        teams = new ArrayList<Team>();
        matches = new ArrayList<Match>();
        setName(name);
    }

    /**
     * this is a conversion method not used in game
     * 
     * @param args stuff
     */
    public static void main(String[] args) {
        Competition comp = readCompetition("competition.xml", true, null);
        comp.writeCompetition("comp.xml", true, null, null);
    }

    /**
     * reads xml file to competition
     * 
     * @param compFile file that contains the competition
     * @param newGame specifies if this file is supposed to be read to create a
     *        new game or to create a game from a save file
     * @param saveGame Parent Element used if this is a saveGame
     * @return new Competition instance
     */
    public static Competition readCompetition(String compFile, boolean newGame,
            Element saveGame) {
        Element element;
        if (newGame) {
            Document dom = XMLRead.parseXMLFile(compFile);
            element = dom.getDocumentElement();

        } else {
            element = saveGame;
        }
        BaseGame.setCurrentDate(XMLRead.getDateValue(element, "date"));
        String compName = element.getAttribute("name");
        compName = Character.toUpperCase(compName.charAt(0)) + compName.substring(1);

        BaseGame.setTeamName(element.getAttribute("playerTeam"));
        Competition compo = new Competition(compName);
        NodeList nl = element.getElementsByTagName("team");

        if (nl != null && nl.getLength() > 0) {
            for (int i = 0; i < nl.getLength(); i++) {

                Element team = (Element) nl.item(i);

                Team t = Team.readTeam(team);
                compo.addTeam(t);
            }
        }
        return compo;
    }

    /**
     * reads xml file to matches
     * 
     * @param matchFile file that contains the matches
     * @param newGame specifies if this file is supposed to be read to create a
     *        new game or to create a game from a save file
     * @param saveGame Parent Element used if this is a saveGame
     * @return list of matches
     */
    public static ArrayList<Match> readMatches(String matchFile,
            boolean newGame, Element saveGame) {
        Element element;
        if (newGame) {
            Document dom = XMLRead.parseXMLFile(matchFile);
            element = dom.getDocumentElement();

        } else {
            element = saveGame;
        }

        NodeList nl = element.getElementsByTagName("match");

        ArrayList<Match> matches = new ArrayList<Match>();

        if (nl != null && nl.getLength() > 0) {
            for (int i = 0; i < nl.getLength(); i++) {

                Element match = (Element) nl.item(i);

                Match m = Match.readMatch(match);
                matches.add(m);
            }
        }
        return matches;
    }

    /**
     * Adds a match to
     * 
     * @param match the match to add
     */
    public void addMatch(Match match) {
        getMatches().add(match);
    }

    /**
     * @param team {@link Team} Object that should be added to the competition
     */
    public void addTeam(Team team) {
        teams.add(team);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }        
        if (obj instanceof Competition){
            Competition other = (Competition) obj;
            if(other.teams.equals(this.teams) &&
                    other.matches.equals(this.matches) &&
                    other.name.equals(this.name)){
                return true;
            }
        }
        return false;
            
    }

    /**
     * @param teamName team to look for
     * @param currentDate the date of the match
     * @return the match containing the right team name and on this date.
     */
    public Match getMatchByNameAndDate(String teamName, Date currentDate) {
        for (Match m : matches) {
            if ((m.getHomeTeam().getName().equals(teamName) || m.getAwayTeam().getName().equals(teamName)) && m.getPlayDate().equals(currentDate)) {
                return m;
            }
        }
        return null;
    }

    /**
     * @return the matches
     */
    public ArrayList<Match> getMatches() {
        return matches;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name the team to be returned has
     * @return the team
     */
    public Team getTeamByName(String name) {
        for (Team t : teams) {
            if (t.getName().trim().equals(name.trim())) {
                return t;
            }
        }
        return null;
    }

    /**
     * @return the teams
     */
    public ArrayList<Team> getTeams() {
        return teams;
    }

    /**
     * @param date play all unplayed matches before date
     */
    public void playMatches(long date) {
        Date d = new Date(date);
        for (Match match : getMatches()) {
            if (!match.isPlayed() && match.isPlayed(d)) {
                GameAlgorithm.playMatch(match);
            }
        }
    }

    /**
     * creates a game schedule
     */
    public void roundRobinMatches() {
        ArrayList<Match> schedule = new ArrayList<Match>();
        ArrayList<String> teamList = new ArrayList<String>();
        for (int i = 0; i < getTeams().size(); i++) {
            teamList.add(getTeams().get(i).getName());
        }

        for (int i = 0; i < Randomness.randomNumber(); i++) {
            teamList.add(teamList.get(0));
            teamList.remove(0);
        }
        if (getTeams().size() % 2 == 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            
            Date d = null;
            if (BaseGame.getCurrentDate() == null) {
                try {
                    d = sdf.parse("02-08-2014");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                d = BaseGame.getCurrentDate();
            }
            
            BaseGame.getCal().add(Calendar.DATE, 7);
            int i = 0;
            while (schedule.size() < getTeams().size() * (getTeams().size() - 1)) {

                List<String> teamList1 = new ArrayList<String>();
                List<String> teamList2 = new ArrayList<String>();

                int g = teamList.size() / 2;

                teamList1 = teamList.subList(0, g);
                teamList2 = teamList.subList(g, teamList.size());
                teamList2 = reverse(teamList2);

                if (i % 2 == 0) {
                    schedule.addAll(createMatches(BaseGame.getCal().getTime().getTime(), teamList1, teamList2));
                } else {
                    schedule.addAll(createMatches(BaseGame.getCal().getTime().getTime(), teamList2, teamList1));
                }

                teamList2 = reverse(teamList2);
                BaseGame.getCal().add(Calendar.DATE, 7);
                teamList.add(teamList.get(0));
                teamList.remove(0);
                i++;
            }
            BaseGame.getCal().setTime(d);
        }

        setMatches(schedule);
    }

    /**
     * sets the lineup for all teams in the competition to a correct format,
     * using the lineUp.autoTeamPos method uses a 4,3,3 lineUp
     */
    public void setLineUpAllTeams() {
        lineUp lineup = new lineUp(4, 3, 3);
        for (int i = 0; i < teams.size(); i++) {            
             lineUp.autoTeamPos(teams.get(i), lineup);           
        }
    }
    
    /**
     * sets the lineup for all teams in the competition to a correct format,
     * using the lineUp.autoTeamPos method uses a 4,3,3 lineUp
     */
    public void setLineUpAllAiTeams() {
        lineUp lineup = new lineUp(4, 3, 3);
        for (int i = 0; i < teams.size(); i++) {
            if(!teams.get(i).equals(BaseGame.getTeam())){
                lineUp.autoTeamPos(teams.get(i), lineup);
            }
        }
    }

    /**
     * @param matches the matches to set
     */
    public void setMatches(ArrayList<Match> matches) {
        this.matches = matches;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param teams the teams to set
     */
    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }

    /**
     * at the start of a game and after each match sets the scores for the teams
     * according to the results of all the matches.
     */
    public void teamStatsFromMatches() {
        for (Team t : teams) {
            t.setWon(0);
            t.setLost(0);
            t.setPlayed(0);
            t.setGoalsFor(0);
            t.setGoalsAgainst(0);
            t.setPoints(0);
        }
        for (Match m : matches) {
            m.teamSetStats();
        }
    }

    @Override
    public String toString() {
        String res = getName() + System.lineSeparator();
        for (int i = 0; i < getTeams().size(); i++) {
            res += "    " + getTeams().get(i).toString() + System.lineSeparator();
        }
        res = res.substring(0, res.lastIndexOf(System.lineSeparator()));
        return res;

    }

    /**
     * Creates a XML File From this competition.
     * 
     * @param file File path
     * @param newGame specifies if this file is supposed to be read to create a
     *        new game or to create a game from a save file
     * @param doc The dom tree used if this is not a saveGame
     * @param saveGame Parent Element used if this is a saveGame
     */
    public void writeCompetition(String file, boolean newGame, Document doc,
            Element saveGame) {

        try {
            Document save;
            if (newGame) {
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder;
                docBuilder = docFactory.newDocumentBuilder();
                save = docBuilder.newDocument();
            } else {
                save = doc;
            }
            Element competition = save.createElement("competition");
            competition.setAttribute("name", getName());
            competition.setAttribute("playerTeam", BaseGame.getTeamName() + "");
            if (newGame) {
                save.appendChild(competition);
            } else {
                saveGame.appendChild(competition);
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Element name = save.createElement("date");
            name.appendChild(save.createTextNode(sdf.format(BaseGame.getCurrentDate())));
            competition.appendChild(name);

            for (int i = 0; i < getTeams().size(); i++) {
                Team.writeTeam(getTeams().get(i), save, competition);
            }
            if (newGame) {
                XMLWriter.printToFile(save, file);
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

    }

    /**
     * Creates a XML File From the matches in this competition.
     * 
     * @param file File path
     * @param newGame specifies if this file is supposed to be read to create a
     *        new game or to create a game from a save file
     * @param doc The dom tree used if this is not a saveGame
     * @param saveGame Parent Element used if this is a saveGame
     */
    public void writeMatches(String file, boolean newGame, Document doc,
            Element saveGame) {

        try {
            Document save;
            if (newGame) {
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder;
                docBuilder = docFactory.newDocumentBuilder();
                save = docBuilder.newDocument();
            } else {
                save = doc;
            }
            Element matches = save.createElement("matches");
            if (newGame) {
                save.appendChild(matches);
            } else {
                saveGame.appendChild(matches);
            }

            for (int i = 0; i < getMatches().size(); i++) {
                Match.writeMatch(getMatches().get(i), save, matches);
            }
            if (newGame) {
                XMLWriter.printToFile(save, file);
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

    }

    private ArrayList<Match> createMatches(long date, List<String> teamList1,
            List<String> teamList2) {
        ArrayList<Match> res = new ArrayList<Match>();
        for (int i = 0; i < teamList1.size(); i++) {
            res.add(new Match(new Date(date), getTeamByName(teamList1.get(i)), getTeamByName(teamList2.get(i))));
        }

        return res;
    }

    private List<String> reverse(List<String> teamList2) {
        int left = 0;
        int right = teamList2.size() - 1;

        while (left < right) {
            // swap the values at the left and right indices
            String temp = teamList2.get(left);
            teamList2.set(left, teamList2.get(right));
            teamList2.set(right, temp);

            // move the left and right index pointers in toward the center
            left++;
            right--;
        }
        return teamList2;
    }
    
    /**
     * gets the number of matches in this competition that are yet unplayed
     * @return an int that represents the amount of matches that are unplayed
     */
    public int getUnplayedMatches(){
        int res = 0;
        for(int i=0;i<matches.size();i++){
            if(!matches.get(i).isPlayed()){
                res++;
            }
        }
        return res;
    }

}
