package game;

import java.util.Comparator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Stefan
 * 
 */
public class Player {

    private String                   name;
    private int                      age;
    private float                    value;
    private float                    salary;
    private String                   currentPosition;
    private Stats                    stats;

    /**
     * a comparator used for sorting the team on gkskill*condition
     */
    public static Comparator<Player> COMPARE_BY_GKSKILL     = new Comparator<Player>() {
        @Override
        public int compare(
                Player other,
                Player one) {
            return Integer.compare(one.getStats().getGkSkill() * one.getStats().getCondition(), other.getStats().getGkSkill() * other.getStats().getCondition());
        }
    };

    /**
     * a comparator used for sorting the team on injuries
     */
    public static Comparator<Player> COMPARE_BY_INJURIES    = new Comparator<Player>() {
        @Override
        public int compare(
                Player one,
                Player other) {
            return Integer.compare(one.getStats().getInjury(), other.getStats().getInjury());
        }
    };

    /**
     * a comparator used for sorting the team on offense*condition
     */
    public static Comparator<Player> COMPARE_BY_OFFENSE     = new Comparator<Player>() {
        @Override
        public int compare(
                Player other,
                Player one) {
            return Integer.compare(one.getStats().getOffense() * one.getStats().getCondition(), other.getStats().getOffense() * other.getStats().getCondition());
        }
    };

    /**
     * a comparator used for sorting the team on defense*condition
     */
    public static Comparator<Player> COMPARE_BY_DEFENSE     = new Comparator<Player>() {
        @Override
        public int compare(
                Player other,
                Player one) {
            return Integer.compare(one.getStats().getDefense() * one.getStats().getCondition(), other.getStats().getDefense() * other.getStats().getCondition());
        }
    };

    /**
     * a comparator used for sorting the team on defense*offense*condition
     */
    public static Comparator<Player> COMPARE_BY_DEF_AND_OFF = new Comparator<Player>() {
        @Override
        public int compare(
                Player other,
                Player one) {
            return Integer.compare(one.getStats().getOffense() * one.getStats().getDefense() * one.getStats().getCondition(), other.getStats().getOffense() * other.getStats().getDefense() * other.getStats().getCondition());
        }
    };
    
    /**
     * a comparator used for sorting the team on defense*offense*condition
     */
    public static Comparator<Player> COMPARE_BY_CARDS = new Comparator<Player>() {
        @Override
        public int compare(
                Player other,
                Player one) {
            return Integer.compare(other.getStats().getCard(), one.getStats().getCard());
        }
    };

    /**
     * initializes a new player Object
     * 
     * @param name the name of this player
     * @param stats the stats of the player, see stats class
     * @param age the age of this player
     * @param currentposition The position the player has in the current lineup
     */
    public Player(String name, int age, String currentposition, Stats stats) {
        this.stats = stats;
        setName(name);
        setAge(age);
        setValue(priceCalculator());
        salary = 9001; // not used at the moment
        currentPosition = currentposition;
    }

    /**
     * @param s the abbrevation of which the position should be returned
     * @return String of the position
     */
    public static String abbreviationToPosition(String s) {
        String position;
        if(s.equals("GK")){
            position = "Goalkeeper";
            
        }else if(s.equals("DL")){
            position = "Defense left";
            
        }else if(s.equals("DC")){
            position = "Defense center";
            
        }else if(s.equals("DR")){
            position = "Defense right";
            
        }else if(s.equals("ML")){
            position = "Midfield left";
            
        }else if(s.equals("MC")){
            position = "Midfield center";
            
        }else if(s.equals("MR")){
            position = "Midfield right";
            
        }else if(s.equals("FL")){
            position = "Offense left";
            
        }else if(s.equals("FR")){
            position = "Offense right";
            
        }else if(s.equals("FC")){
            position = "Offense center";
            
        }else if(s.equals("SB")){

            position = "Substitute";
        }else{

            position = "Reserve";
        }
        return position;
    }

    /**
     * @param s the position of which the abbrevation should be returned
     * @return String of the abbrevation
     */
    public static String positionToAbbreviation(String s) {
        String position;
        
        if(s.equals("Goalkeeper")){
            position = "GK";
            
        }else if(s.equals("Defense left")){
            position = "DL";
            
        }else if(s.equals("Defense center")){
            position = "DC";
            
        }else if(s.equals("Defense right")){
            position = "DR";
            
        }else if(s.equals("Midfield left")){
            position = "ML";
            
        }else if(s.equals("Midfield center")){
            position = "MC";
            
        }else if(s.equals("Midfield right")){
            position = "MR";
            
        }else if(s.equals("Offense left")){
            position = "FL";
            
        }else if(s.equals("Offense right")){
            position = "FR";
            
        }else if(s.equals("Offense center")){
            position = "FC";
            
        }else if(s.equals("Substitute")){

            position = "SB";
        }else{

            position = "RS";
        }
        return position;
    }

    /**
     * Creates a new {@link Player} from a xml element.
     * 
     * @param el a {@link Element}
     * @return {@link Player} Object
     */
    public static Player readPlayer(Element el) {
        String name = XMLRead.getTextValue(el, "name");
        int att = XMLRead.getIntValue(el, "offense");
        int def = XMLRead.getIntValue(el, "defense");
        int sta = XMLRead.getIntValue(el, "stamina");
        int con = XMLRead.getIntValue(el, "condition");
        int agr = XMLRead.getIntValue(el, "agression");
        int age = XMLRead.getIntValue(el, "age");
        int inj = XMLRead.getIntValue(el, "injury");
        int num = XMLRead.getIntValue(el, "number");
        char side = XMLRead.getTextValue(el, "side").charAt(0);
        int gkSkill = XMLRead.getIntValue(el, "gkskill");
        int crd = XMLRead.getIntValue(el, "card");
        String curp = XMLRead.getTextValue(el, "curPos");
        Stats stats = new Stats(att, def, sta, agr, inj, num, side, gkSkill, con, crd);
        Player p = new Player(name, age, curp, stats);

        return p;
    }

    /**
     * @param p a {@link Player} Object
     * @param save a {@link Document}
     * @param team a {@link Element}
     */
    public static void writePlayer(Player p, Document save, Element team) {
        Element player = save.createElement("player");
        team.appendChild(player);

        Element name = save.createElement("name");
        name.appendChild(save.createTextNode(p.getName()));
        player.appendChild(name);

        Element age = save.createElement("age");
        age.appendChild(save.createTextNode(p.getAge() + ""));
        player.appendChild(age);

        Element cp = save.createElement("curPos");
        cp.appendChild(save.createTextNode(p.getCurrentPosition()));
        player.appendChild(cp);

        Stats.writeStats(p, save, player);
    }

    /**
     * @return the defensive power of this player
     */
    public double calculateDefense() {
        char currentSide = currentPosition.charAt(1);
        double n = stats.getDefense() * ((double) stats.getCondition() / 100) * (((100) + (stats.getAgression() * 0.65)) / 100);
        if (currentSide == stats.getSide()) {
            n = n * 1.1;
        } else {
            n = n * 0.9;
        }
        //check if position is mid
        if (currentPosition.charAt(0) == 'M') {
            return n * 0.5;
        } else {
            return n;
        }
    }

    /**
     * @return the offensive power of this player
     */
    public double calculateOffense() {
        char currentSide = currentPosition.charAt(1);
        double n = stats.getOffense() * ((double) stats.getCondition() / 100) * (((100) + (stats.getAgression() * 0.65)) / 100);
        if (currentSide == stats.getSide()) {
            n = n * 1.1;
        } else {
            n = n * 0.9;
        }
        //check if position is mid
        if (currentPosition.charAt(0) == 'M') {
            return n * 0.5;
        } else {
            return n;
        }
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Player) {
            Player otherplayer = (Player) other;
            if (    this.name.equals(otherplayer.getName())
                    & this.age == otherplayer.getAge()
                    & Float.floatToIntBits(value) == Float.floatToIntBits(otherplayer.getValue())
                    & Float.floatToIntBits(salary) == Float.floatToIntBits(otherplayer.getSalary())
                    & this.currentPosition.equals(otherplayer.currentPosition)
                    & this.stats.equals(otherplayer.getStats()))
                return true;
        }
        
        return false;
    }

    /**
     * returns the age of this person
     * 
     * @return age as an integer
     */
    public int getAge() {
        return age;
    }

    /**
     * @return player position
     */
    public String getCurrentPosition() {
        return currentPosition;
    }

    /**
     * Returns the name of this person
     * 
     * @return String the name of this person
     */
    public String getName() {
        return name;
    }

    /**
     * @return the players salary
     */
    public float getSalary() {
        return salary;
    }

    /**
     * @return {@link Stats}
     */
    public Stats getStats() {
        return stats;
    }

    /**
     * @return the players value
     */
    public float getValue() {
        return value;
    }

    /**
     * Method to be called after an unplayed match
     */
    public void increaseCondition() {
        int newCondition = stats.getCondition() + stats.getStamina() / 3;
        if (newCondition > 100) {
            newCondition = 100;
        }
        stats.setCondition(newCondition);
    }

    /**
     * @param increment the amount with which to increase the players defense
     */
    public void increaseDefense(int increment) {
        int newDefense = getStats().getDefense() + increment;
        if (newDefense > 100) {
            newDefense = 100;
        }
        getStats().setDefense(newDefense);
        setValue(priceCalculator());
    }

    /**
     * @param increment the amount with which to increase the players defense
     */
    public void increaseGKSkill(int increment) {
        int newGKSkill = getStats().getGkSkill() + increment;
        if (newGKSkill > 100) {
            newGKSkill = 100;
        }
        getStats().setGkSkill(newGKSkill);
        setValue(priceCalculator());
    }

    /**
     * @param increment the amount with which to increase the players offense
     */
    public void increaseOffense(int increment) {
        int newOffense = getStats().getOffense() + increment;
        if (newOffense > 100) {
            newOffense = 100;
        }
        getStats().setOffense(newOffense);
        setValue(priceCalculator());
    }

    /**
     * Method to be called after a played match
     */
    public void reduceCondition() {
        int newCondition = stats.getCondition() - 500 / stats.getStamina();
        if (newCondition < 0) {
            newCondition = 0;
        }
        stats.setCondition(newCondition);
    }

    /**
     * sets the age of this person
     * 
     * @param age the new age of this person as an integer
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @param currentPosition new position for the player
     */
    public void setCurrentPosition(String currentPosition) {
        this.currentPosition = currentPosition;
    }

    /**
     * Sets the name of this person
     * 
     * @param name the new name of this person
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param salary the salary to be set
     */
    public void setSalary(float salary) {
        this.salary = salary;
    }

    /**
     * @param stats the stats to be set
     */
    public void setStats(Stats stats) {
        this.stats = stats;
    }

    /**
     * sets the value of this player object to price
     * 
     * @param value the value of this player
     */
    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Player [name=" + name + ", age=" + age + ", value=" + value + ", salary=" + salary + ", currentPosition=" + currentPosition + ", stats=" + stats + "]";
    }

    private float priceCalculator() {
        return 5000 + (getStats().getOffense() + getStats().getDefense() + getStats().getGkSkill()) * 500;
    }

}
