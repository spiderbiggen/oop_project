package game;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Timo
 *
 */
public class Match implements Comparable<Match> {
    private Team    homeTeam;
    private Team    awayTeam;
    private int     homeGoals;
    private int     awayGoals;
    private Date    playDate;
    private int     awayShots;
    private int     homeShots;
    private int     homeYellow;
    private int     awayYellow;
    private int     homeInjury;
    private int     awayInjury;
    private int     homeRed;
    private int     awayRed;
    private boolean played;

    /**
     * @param playDate constructor for the match with homeGoals and awayGoals
     *        parameter
     * @param homeGoals the amount of goals for the hometeam
     * @param awayGoals the amount of goals for the awayteam
     * @param home the hometeam
     * @param away the awayteam
     */
    public Match(Date playDate, int homeGoals, int awayGoals, Team home,
            Team away) {
        this.playDate = playDate;
        setScore(homeGoals, awayGoals);
        homeTeam = home;
        awayTeam = away;
        played = false;
    }

    /**
     * @param playDate constructor for the match, homeGoals and awayGoals will
     *        be set to -1 for setting scores use the setScore method
     * @param home the hometeam
     * @param away the awayteam
     */
    public Match(Date playDate, Team home, Team away) {
        this.playDate = playDate;
        setScore(-1, -1);
        homeTeam = home;
        awayTeam = away;
        played = false;
    }

    /**
     * @param el the element containing the match
     * @return The match out of the element
     */
    public static Match readMatch(Element el) {
        int homeGoals = XMLRead.getIntValue(el, "homeGoals");
        int awayGoals = XMLRead.getIntValue(el, "awayGoals");
        String homeTeamString = XMLRead.getTextValue(el, "homeTeam");
        String awayTeamString = XMLRead.getTextValue(el, "awayTeam");
        Team homeTeam = BaseGame.getComp().getTeamByName(homeTeamString);
        Team awayTeam = BaseGame.getComp().getTeamByName(awayTeamString);
        Date d = XMLRead.getDateValue(el, "playDate");
        Match m = new Match(d, homeGoals, awayGoals, homeTeam, awayTeam);
        m.setPlayed(XMLRead.getBooleanValue(el, "isPlayed"));
        return m;
    }

    /**
     * @param m The match that needs adding to the file
     * @param save XML document
     * @param competition Element
     */
    public static void writeMatch(Match m, Document save, Element competition) {
        //add match node to the competition
        Element match = save.createElement("match");
        competition.appendChild(match);

        //add homeTeam node to match
        Element homeTeam = save.createElement("homeTeam");
        homeTeam.appendChild(save.createTextNode(m.homeTeam.getName()));
        match.appendChild(homeTeam);

        //add awayTeam node to match
        Element awayTeam = save.createElement("awayTeam");
        awayTeam.appendChild(save.createTextNode(m.awayTeam.getName()));
        match.appendChild(awayTeam);

        //add homeGoals node to match
        Element homeGoals = save.createElement("homeGoals");
        homeGoals.appendChild(save.createTextNode("" + m.getHomeGoals()));
        match.appendChild(homeGoals);

        //add awayGoals node to match
        Element awayGoals = save.createElement("awayGoals");
        awayGoals.appendChild(save.createTextNode("" + m.getAwayGoals()));
        match.appendChild(awayGoals);

        //add playDate node to match
        Element playDate = save.createElement("playDate");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = sdf.format(m.getPlayDate());
        playDate.appendChild(save.createTextNode(date));
        match.appendChild(playDate);

        //add hasBeenPlayed node to match
        Element hasBeenPlayed = save.createElement("isPlayed");
        hasBeenPlayed.appendChild(save.createTextNode("" + m.isPlayed()));
        match.appendChild(hasBeenPlayed);
    }

    @Override
    public int compareTo(Match o) {
        if (getPlayDate().getTime() > o.getPlayDate().getTime()) {
            return 1;
        } else if (getPlayDate().getTime() < o.getPlayDate().getTime()) {
            return -1;
        } else {
            return 0;
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            if (this == obj) {
                return true;
            }
            if ((obj instanceof Match)) {
                Match other = (Match) obj;
                if (awayGoals == other.awayGoals && 
                        awayTeam.equals(other.awayTeam) && 
                        homeGoals == other.homeGoals && 
                        homeTeam.equals(other.homeTeam) && 
                        playDate.equals(other.playDate)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return awayGoals as int
     */
    public int getAwayGoals() {
        return awayGoals;
    }

    /**
     * @return the amount of red cards on the away team
     */
    public int getAwayRed() {
        return awayRed;
    }

    /**
     * @return the away team's goals
     */
    public int getAwayShots() {
        return awayShots;
    }

    /**
     * @return the awayTeam
     */
    public Team getAwayTeam() {
        return awayTeam;
    }

    /**
     * @return the amount of yellow cards on the away team
     */
    public int getAwayYellow() {
        return awayYellow;
    }

    /**
     * @return homeGoals as int
     */
    public int getHomeGoals() {
        return homeGoals;
    }

    /**
     * @return the amount of red cards on the home team
     */
    public int getHomeRed() {
        return homeRed;
    }

    /**
     * @return the home team's goals
     */
    public int getHomeShots() {
        return homeShots;
    }

    /**
     * @return the homeTeam
     */
    public Team getHomeTeam() {
        return homeTeam;
    }

    /**
     * @return the amount of yellow cards on the home team
     */
    public int getHomeYellow() {
        return homeYellow;
    }

    /**
     * @return playDate as Date
     */
    public Date getPlayDate() {
        return playDate;
    }

    /**
     * @return the hasBeenPlayed
     */
    public boolean isPlayed() {
        return played;
    }

    /**
     * @param currentdate the date of the current game
     * @return true if the match date is before the current date
     */
    public boolean isPlayed(Date currentdate) {
        return playDate.before(currentdate);
    }

    /**
     * @param awayTeam the team playing on the away side
     */
    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    /**
     * @param homeTeam the team playing on the home side
     */
    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    /**
     * @param played the hasBeenPlayed to set
     */
    public void setPlayed(boolean played) {
        this.played = played;
    }

    /**
     * @param homeGoals the amount of goals for the hometeam
     * @param awayGoals the amount of goals for the awayteam
     */
    public void setScore(int homeGoals, int awayGoals) {
        this.homeGoals = homeGoals;
        this.awayGoals = awayGoals;
    }

    /**
     * @param homeTeamShots shots on goal by homeTeam
     * @param awayTeamShots shots on goal by awayTeam
     */
    public void setShots(int homeTeamShots, int awayTeamShots) {
        awayShots = awayTeamShots;
        homeShots = homeTeamShots;

    }

    /**
     * the baseStats to be returned if a match has not been played
     */
    public void statsZero() {
        awayShots = -1;
        homeShots = -1;
        homeYellow = -1;
        awayYellow = -1;
        homeRed = -1;
        awayRed = -1;
    }

    /**
     * Sets the scores of a team according to the results of the match
     */
    public void teamSetStats() {

        if (isPlayed(new Date(BaseGame.getCurrentDate().getTime() + 2L))) {
            Team homeTeam = BaseGame.getComp().getTeamByName(this.homeTeam.getName());
            int homeIndex = BaseGame.getComp().getTeams().indexOf(homeTeam);
            Team awayTeam = BaseGame.getComp().getTeamByName(this.awayTeam.getName());
            int awayIndex = BaseGame.getComp().getTeams().indexOf(awayTeam);

            if (homeGoals > awayGoals) {
                homeTeam.setWon(homeTeam.getWon() + 1);
                awayTeam.setLost(awayTeam.getLost() + 1);
                homeTeam.setPoints(homeTeam.getPoints() + 3);
            } else if (awayGoals > homeGoals) {
                homeTeam.setLost(homeTeam.getLost() + 1);
                awayTeam.setWon(awayTeam.getWon() + 1);
                awayTeam.setPoints(awayTeam.getPoints() + 3);
            } else {
                homeTeam.setPoints(homeTeam.getPoints() + 1);
                awayTeam.setPoints(awayTeam.getPoints() + 1);
            }

            homeTeam.setPlayed(homeTeam.getPlayed() + 1);
            awayTeam.setPlayed(awayTeam.getPlayed() + 1);

            homeTeam.setGoalsFor(homeTeam.getGoalsFor() + homeGoals);
            homeTeam.setGoalsAgainst(homeTeam.getGoalsAgainst() + awayGoals);

            awayTeam.setGoalsFor(awayTeam.getGoalsFor() + awayGoals);
            awayTeam.setGoalsAgainst(awayTeam.getGoalsAgainst() + homeGoals);

            BaseGame.getComp().getTeams().set(homeIndex, homeTeam);
            BaseGame.getComp().getTeams().set(awayIndex, awayTeam);
            setPlayed(true);
        }
    }

    /**
     * checks if both teams have a correct lineUp
     * 
     * @return true if both teams are ready to play the match
     */
    public boolean teamsReady() {
        return homeTeam.validateLineUp() && awayTeam.validateLineUp();

    }

    /**
     * sets the amount of goals the home team has
     * 
     * @param homeGoals amount of goals for the home Team
     */
    public void setHomeGoals(int homeGoals) {
        this.homeGoals = homeGoals;
    }

    /**
     * sets the amount of goals the away team has
     * 
     * @param awayGoals amount of goals for the away Team
     */
    public void setAwayGoals(int awayGoals) {
        this.awayGoals = awayGoals;
    }

    /**
     * sets the date this match should be played
     * 
     * @param playDate match play date
     */
    public void setPlayDate(Date playDate) {
        this.playDate = playDate;
    }

    /**
     * sets the amount of goals the away team has
     * 
     * @param awayShots amount of shots
     */
    public void setAwayShots(int awayShots) {
        this.awayShots = awayShots;
    }

    /**
     * sets the amount of homeshots
     * 
     * @param homeShots the amount of homeshots
     */
    public void setHomeShots(int homeShots) {
        this.homeShots = homeShots;
    }

    /**
     * sets the amount of yellow cards for the home team
     * 
     * @param homeYellow the amount of yellow cards
     */
    public void setHomeYellow(int homeYellow) {
        this.homeYellow = homeYellow;
    }

    /**
     * sets the amount of yellow cards for the away team
     * 
     * @param awayYellow amount of yellow cards
     */
    public void setAwayYellow(int awayYellow) {
        this.awayYellow = awayYellow;
    }

    /**
     * sets the amount of red cards for the home team
     * 
     * @param homeRed amount of red cards
     */
    public void setHomeRed(int homeRed) {
        this.homeRed = homeRed;
    }

    /**
     * sets the amount of red cards for the away team
     * 
     * @param awayRed amount of cards
     */
    public void setAwayRed(int awayRed) {
        this.awayRed = awayRed;
    }

    /**
     * @return returnes the amount of injuries for the home team
     */
    public int getHomeInjury() {
        return homeInjury;
    }

    /**
     * sets the amount of injuriess for the home team
     * 
     * @param homeInjury the amount of injuries
     */
    public void setHomeInjury(int homeInjury) {
        this.homeInjury = homeInjury;
    }

    /**
     * @return returnes the amount of injuries for the away team
     */
    public int getAwayInjury() {
        return awayInjury;
    }

    /**
     * sets the amount of injuries for the away team
     * 
     * @param awayInjury amount of injuries
     */
    public void setAwayInjury(int awayInjury) {
        this.awayInjury = awayInjury;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Match [" + "homeTeam=" + homeTeam.getName() + " awayTeam=" + awayTeam.getName() + " homeGoals=" + homeGoals + " awayGoals=" + awayGoals + " playDate=" + playDate + "]";
    }
}
