package game;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author Stefan
 *
 */
public class XMLRead {

    /**
     * Reads the value from the node and converts it to a {@link Boolean} Object
     * 
     * @param el Element that this node should be a child of
     * @param tagName the name of the node
     * @return Creates a {@link Boolean} from the value in the node
     */
    public static boolean getBooleanValue(Element el, String tagName) {
        return Boolean.parseBoolean(getTextValue(el, tagName));
    }

    /**
     * Reads the value from the node and converts it to a {@link Date} Object
     * 
     * @param el Element that this node should be a child of
     * @param tagName the name of the node
     * @return a {@link Date} Object corresponding to the date in the node
     */
    public static Date getDateValue(Element el, String tagName) {
        //NEEDS TESTING
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = getTextValue(el, tagName);
        Date d = null;
        try {
            d = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     * Reads the value from the node and converts it to a {@link Float} Object
     * 
     * @param el Element that this node should be a child of
     * @param tagName the name of the node
     * @return Creates a {@link Float} from the value in the node
     */
    public static float getFloatValue(Element el, String tagName) {
        return Float.parseFloat(getTextValue(el, tagName));
    }

    /**
     * Reads the value from the node and converts it to a {@link Integer} Object
     * 
     * @param el Element that this node should be a child of
     * @param tagName the name of the node
     * @return Creates a {@link Integer} from the value in the node
     */
    public static int getIntValue(Element el, String tagName) {
        return Integer.parseInt(getTextValue(el, tagName));
    }

    /**
     * @param ele Element that this node should be a child of
     * @param tagName the name of the node
     * @return The {@link String} from the node value
     */
    public static String getTextValue(Element ele, String tagName) {
        String textVal = null;										// sets the resulting value to null
        NodeList nodeList = ele.getElementsByTagName(tagName);		// Creates a list of nodes that have the <tagName> as tag
        if (nodeList != null && nodeList.getLength() > 0) {			// checks if there are such nodes
            Element el = (Element) nodeList.item(0);				// stores the first element in this list as an Element
            textVal = el.getFirstChild().getNodeValue();			// getFirstChild grabs the actual node we are looking for and getNodeValue gets us the value between the 2 tags
        }
        textVal = Character.toUpperCase(textVal.charAt(0)) + textVal.substring(1);
        return textVal;
    }

    /**
     * Creates a dom tree to be parsed
     * 
     * @param file the file that should be read to create the competition
     * @return a new Dom tree
     */
    public static Document parseXMLFile(String file) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document dom = null;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();

            dom = db.parse(file);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dom;
    }

}
