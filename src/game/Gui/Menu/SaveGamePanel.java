package game.Gui.Menu;

import game.BaseGame;
import game.Gui.MainGui;
import game.Gui.StandardPanel;
import game.Gui.SquadPanels.SquadPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class SaveGamePanel extends StandardPanel {

    public SaveGamePanel() {
        JTitle("Save Game", this);
        textStandard("Select a file to load");
        String[] savegames = ScanSaveGames();

        //We add a text field that is writable, but also changes whenever an item is selected from the selectbox
        final JTextField loadField = new JTextField("Select a save game");
        loadField.setFont(inputFont);
        loadField.setSize(loadField.getPreferredSize());
        
        loadField.setLocation(getWidth() / 2 - loadField.getWidth() / 2, getHeight() / 3);
        this.add(loadField);

        //add a combobox that contains all the savegames in the savegame folder
        final JComboBox<String> savebox = new JComboBox<>(savegames);
        savebox.setFont(inputFont);
        savebox.setSize(savebox.getPreferredSize());
        
        savebox.setLocation(getWidth() / 2 - savebox.getWidth() / 2, getHeight() / 4);
        this.add(savebox);

        savebox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                String selected = (String) savebox.getSelectedItem();
                loadField.setText(selected);
            }
        });

        //add the button which will save the game selected in the loadbox
        JButton btnLoadGame = menuButton("Save game", this);
        btnLoadGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {

                String fileToSave = loadField.getText();
                //we don't want to save the file by accident to one of these filenames, so therefore an if statement
                if (!fileToSave.equals("Game saved!") && !fileToSave.equals("Please type text") && !fileToSave.equals("Select a save game") && fileToSave.length()<10) {
                    try {
                        BaseGame.BaseGameWrite("savegames/" + fileToSave);
                        loadField.setText("Game saved!");
                    } catch (NullPointerException e) {
                        loadField.setText("Please type text");
                    }
                }else if(fileToSave.length()>=10){
                    loadField.setText("enter less characters");
                }
            }
        });
        btnLoadGame.setLocation(getWidth() / 2 - btnLoadGame.getWidth() / 2, getHeight() / 2);
        this.add(btnLoadGame);

        //add a button that will take the user back to the game
        JButton btnGoBack = menuButton("Go back to game", this);
        btnGoBack.setLocation((getWidth() - btnGoBack.getWidth()) / 2, getHeight() * 2 / 3);
        btnGoBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(SquadPanel.class);
            }
        });
    }

    /**
     * This method should be the same as the LoadGamePanel function Therefore it
     * returns the value from the static method 'LoadGamePanel.ScanSavegames()'
     * 
     * @return a string[] with all the names of the save games
     */
    public String[] ScanSaveGames() {
        return LoadGamePanel.ScanSavegames();
    }
}
