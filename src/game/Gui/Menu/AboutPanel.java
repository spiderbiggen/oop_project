package game.Gui.Menu;

import game.Gui.MainGui;
import game.Gui.StandardPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextPane;

public class AboutPanel extends StandardPanel {
    String text = "Football manager 64 is a game made by 7 students Computer Science from the Technical University Of Delft\n"+
                "Our goal was to make our own version of the Football Manager game in 10 weeks.\n"+
                "This game was made for educational purposes and is therefore free to play for anyone\n\n"+
                "Our team consisted of the following students:\n\n"+
                    "Timo van Leest\n"+
                "Stefan Breetveld\n"+
                    "Iwan Hoogenboom\n"+
                "Karan Ramsodit\n"+
                    "Daan van der Werf\n"+
                "Yuri Groeneveld\n"+
                    "Sytze Andringa\n\n"+
                "(c) 2015";

    public AboutPanel() {
        //adds the title
        JLabel title = JTitle("About FOOTBALL MANAGER 64", this);
        this.add(title);
        JTextPane textPane = textStandard(text);
        this.add(textPane);

        //adds a menu to go back to the menu
        JButton toMenu = menuButton("back to menu", this);
        toMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(MainMenuPanel.class);
            }
        });
        
        toMenu.setLocation(getWidth() / 2 - buttonWidth / 2, (getHeight() / 5) * 4);
    }
    
}
