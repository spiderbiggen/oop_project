package game.Gui.Menu;

import game.Gui.MainGui;
import game.Gui.StandardPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class InstructionsPanel extends StandardPanel {
    String text =   "Welcome to football manager 64.\n"+
                    "You play the role the coach of a dutch football team."+ 
                    "Your goal is to get as high as possible in the rankings."+
                    "You do this by playing matches. \n"+
                    "Winning a match gives you 3 points, ties gives both teams 1 point and loosing gives a team none. "+
                    "The matches that are planned this competition are shown in the 'upcoming matches' panel."+
                    "For the matches that are played, a score is shown.\n\n"+
                    
                    "Before each match you can do a number of things.\n"+
                    "You can line-up your team to your liking. Note that a team can have a maximum of 11 players including one goalkeeper on the field and 7 substitutes max."+
                    "Players with red cards or injuries can't be set-up and should be set to reserves. In time, these will vanish."+
                    
                    "Having a lineup that consists of 4 defenders, 3 mid-players and 3 attackers is a solid choice.\n"+
                    
                    "Players that play a lot of matches get tired quickly. The condition stat tells you how much energy each player got."+
                    "100 is optimal. Below 60 you should consider switching lineup with an other player.\n\n"+
                    
                    "There are a number of stats each player got.\n\n"+
                    
                    "Position: the current position of a player\n"+
                    "Value: the value of a player, used for buying and selling players. \n"+
                    "Players with high stats have a high value.\n"+
                    "Condition: Players that play a lot of matches get tired quickly. The condition stat tells you how much energy each player got."+
                    "100 is optimal. If it gets low, this can have serious impact on the players preformance.\n"+
                    "Offense: Players with high offense skills should be put in front of your lineup.\n"+
                    "Defense: Players with high defense skills should be punt in the back of your lineup.\n"+
                    "GK skill: The goalkeeper skill. This tells us how good a specific player is in goalkeeping.\n"+
                    "Try to put players with a high GK skill as goalkeeper for the best match results.\n"+
                    "Stamina: Players with a low stamina gets tired more often.\n"+
                    "Aggression: Players with high aggresion get more cards and injure other players more often.\n"+
                    "Favorite side: For best results, try to place players on their favorite side.\n"+
                    "For instance, players with a favorite side L should be put left.\n"+
                    "Status: This field tells us if the player is injured.\n"+
                    "Card:  This field tells us what card the player has and can either be empty, yellow or red.\n\n"+
                    
                    "Each round you can try to buy a player. You can place one offer for each player each turn."+
                    "The coach of the other team can either accept or reject your offer.\n\n"+
                    
                    "Each round a few other teams might place a bid on one of your players. You can see these in the Sell Players tab."+
                    "You can decide to accept the offer for that specific value.\n\n"+
                    "We wish you the best of luck!";

    public InstructionsPanel() {
        //adds the title
        JLabel title = JTitle("Instructions for FOOTBALL MANAGER 64", this);
        this.add(title);
        JTextPane textArea = textStandard(text);

        textArea.setSize(this.getWidth()*8/9,this.getHeight()*3/5);
        textArea.setLocation(this.getWidth()/2-textArea.getWidth()/2,this.getHeight()/5);
        textArea.setCaretPosition(0);
        JScrollPane scrollText = new JScrollPane(textArea);
        scrollText.setSize(textArea.getSize());
        scrollText.setLocation(textArea.getLocation());
        this.add(scrollText);

        //adds a menu to go back to the menu
        JButton toMenu = menuButton("Back to Menu", this);
        toMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(MainMenuPanel.class);
            }
        });
        toMenu.setLocation(getWidth() / 2 - buttonWidth / 2, (getHeight() / 6) * 5);
  //      this.add(toMenu);
    }
}
