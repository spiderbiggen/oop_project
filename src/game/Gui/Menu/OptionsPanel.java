package game.Gui.Menu;

import game.XMLRead;
import game.XMLWriter;
import game.Gui.MainGui;
import game.Gui.StandardFrame;
import game.Gui.StandardPanel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Options panel that is shown if the user clicks on options in the main menu a
 * different is shown, called GameOptions, if the user clicks on the options
 * button in the squadpanel
 * 
 * @author sytze
 *
 */
public class OptionsPanel extends StandardPanel {
    String                            text;
    public static int                 colCurrentIndex;
    public static int                 resCurrentIndex;
    public static boolean             muteSelected;
    public static int                 effectVolumeIndex;
    public static int                 musicVolumeIndex;
    

    protected JButton                 confirm;
    protected JButton                 backBtn;
    protected JButton                 defaultSet;

    protected final JCheckBox         muteCheck;
    protected final JComboBox<String> selectColor;
    protected final JSlider           volume;
    protected final JSlider           effectVolume;
    protected final JComboBox<String> resSelect;

    protected ActionListener          back;

    public OptionsPanel() {
        text = "Adjust the Options as you'd like";
        //if cheat mode is enabled, adds a cheat box and button
        cheatVoid(StandardPanel.cheat);

        //adds the title
        JLabel title = JTitle("Options", this);
        this.add(title);

        //adds a text saying "choose a background color"
        JLabel chooseColor = new JLabel("Choose a background color");
        chooseColor.setFont(textFont);
        chooseColor.setForeground(textColor);
        chooseColor.setSize(chooseColor.getPreferredSize());
        chooseColor.setLocation(this.getWidth() * 3 / 4 - chooseColor.getWidth() / 2, this.getHeight() / 2);
        this.add(chooseColor);

        //Adds a Combobox to select a background color
        String[] colors = {"Green", "Dark Gray", "Gray", "Black", "Blue", "Cyan", "Orange", "Red", "Purple", "Random" };
        selectColor = new JComboBox<String>(colors);
        selectColor.setSelectedIndex(colCurrentIndex);
        selectColor.setSize(selectColor.getPreferredSize());
        this.add(selectColor);
        selectColor.setLocation(this.getWidth() * 3 / 4 - selectColor.getWidth() / 2, this.getHeight() / 7 * 4);

        //add a checkbox to mute sound
        muteCheck = new JCheckBox("Mute sound");
        muteCheck.setFont(textFont);
        muteCheck.setBackground(backGroundColor);
        muteCheck.setForeground(textColor);
        muteCheck.setSelected(muteSelected);
        muteCheck.setSize(muteCheck.getPreferredSize());
        muteCheck.setLocation(this.getWidth() / 4 - muteCheck.getWidth() / 2, this.getHeight() / 3);
        this.add(muteCheck);

        //adds a volume slider
        JLabel volumeText = new JLabel("Volume");
        volumeText.setFont(textFont);
        volumeText.setForeground(textColor);
        volumeText.setSize(volumeText.getPreferredSize());
        volumeText.setLocation(this.getWidth() * 3 / 4 - volumeText.getWidth() / 2, this.getHeight() / 3 - 15);
        this.add(volumeText);

        volume = new JSlider(SwingConstants.HORIZONTAL, 0, 100, StandardFrame.volume);
        volume.setMajorTickSpacing(10);
        volume.setPaintTicks(true);
        volume.setSize(150, 25);
        volume.setLocation(this.getWidth() * 3 / 4 - volume.getWidth() / 2, this.getHeight() / 3 + 15);
        volume.setBackground(backGroundColor);
        volume.setForeground(textColor);
        volume.setValue(musicVolumeIndex);
        this.add(volume);

        //adds a volume slider for sound effects
        JLabel effectVolumeText = new JLabel("Effect Volume");
        effectVolumeText.setFont(textFont);
        effectVolumeText.setForeground(textColor);
        effectVolumeText.setSize(effectVolumeText.getPreferredSize());
        effectVolumeText.setLocation(this.getWidth() * 3 / 4 - effectVolumeText.getWidth() / 2, this.getHeight() / 12 * 5 - 15);
        this.add(effectVolumeText);

        effectVolume = new JSlider(SwingConstants.HORIZONTAL, 0, 100, StandardFrame.effectVolume);
        effectVolume.setMajorTickSpacing(10);
        effectVolume.setPaintTicks(true);
        effectVolume.setSize(150, 25);
        effectVolume.setLocation(this.getWidth() * 3 / 4 - effectVolume.getWidth() / 2, this.getHeight() / 12 * 5 + 15);
        effectVolume.setBackground(backGroundColor);
        effectVolume.setForeground(textColor);
        effectVolume.setValue(effectVolumeIndex);
        this.add(effectVolume);

        //adds a text saying "choose a background color"
        JLabel chooseResText = new JLabel("Choose a resolution for the frame");
        chooseResText.setFont(textFont);
        chooseResText.setForeground(textColor);
        chooseResText.setSize(chooseResText.getPreferredSize());
        chooseResText.setLocation(this.getWidth() / 4 - chooseResText.getWidth() / 2, this.getHeight() / 2);
        this.add(chooseResText);

        //Adds a Combobox to select a resolution
        String[] resolutions = { "1024x768", "1200x900", "1400x1077" };
        resSelect = new JComboBox<String>(resolutions);
        resSelect.setSelectedIndex(resCurrentIndex);
        resSelect.setSize(resSelect.getPreferredSize());
        this.add(resSelect);
        resSelect.setLocation(this.getWidth() / 4 - resSelect.getWidth() / 2, this.getHeight() / 7 * 4);

        //adds a menu to go back to the menu
        backBtn = menuButton("back", this);
        back = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(MainMenuPanel.class);
            }
        };
        backBtn.addActionListener(back);
        backBtn.setLocation(this.getWidth() / 4 - buttonWidth / 2, (this.getHeight() / 3) * 2);

        //adds a menu to go back to the menu and confirm the changes
        confirm = menuButton("Confirm", this);
        confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                saveChanges();
            }
        });
        confirm.setLocation(3 * this.getWidth() / 4 - buttonWidth / 2, (this.getHeight() / 3) * 2);

        //adds a button to go back to the default setting
        defaultSet = menuButton("Default Settings", this);
        defaultSet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                defaultSettings();
            }
        });
        defaultSet.setLocation(3 * this.getWidth() / 4 - buttonWidth / 2, (this.getHeight() / 5) * 4);
    }

    /**
     * changes the background color. Input should represent a color. (Grey,Blue,Black etc)
     * @param selected
     */
    public static void changeColor(String selected) {
        //       System.out.println("color changed to: " + selected);
        switch (selected) {
            case "Blue":
                backGroundColor = Color.blue;
                break;
            case "Cyan":
                backGroundColor = Color.cyan;
                break;
            case "Gray":
                backGroundColor = Color.gray;
                break;
            case "Black":
                backGroundColor = Color.black;
                break;
            case "Dark Gray":
                backGroundColor = Color.darkGray;
                break;
            case "Orange":
                Color theRealOrange = new Color(255, 140, 15);
                backGroundColor = theRealOrange;
                break;
            case "Red":
                backGroundColor = Color.RED;
                break;
            case "Purple":
                Color Purple = new Color(200, 20, 125);
                backGroundColor = Purple;
                break;
            case "Green":
                backGroundColor = Color.GREEN;
                break;
            case "Random":
                Color Random = new Color((int) (Math.random() * 256), (int) (Math.random() * 256), (int) (Math.random() * 256));
                backGroundColor = Random;
                break;
        }
    }

    /**
     * saves the changes in the options
     */
    public void saveChanges() {

        muteSelected = muteCheck.isSelected();
        colCurrentIndex = selectColor.getSelectedIndex();
        resCurrentIndex = resSelect.getSelectedIndex();
        musicVolumeIndex = volume.getValue();
        effectVolumeIndex = effectVolume.getValue();
        StandardFrame.adjustVolume(musicVolumeIndex);
        StandardFrame.adjustEffectVolume(effectVolumeIndex);
        changeColor((String) selectColor.getSelectedItem());
        changeRes(resCurrentIndex);
        changeMute(muteCheck.isSelected());
        writeConfig();
        MainGui.frame.refresh();
        MainGui.frame.browseTo(this.getClass());
    }

    /**
     * mutes the sound depending on the boolean
     * 
     * @param check boolean input. if true, mutes sound. if false, sound starts
     */
    public static void changeMute(boolean check) {
        if (check) {
            //        System.out.println("sound muted");
            MainGui.frame.clip.stop();
        } else {
            //        System.out.println("sound resumed");
            MainGui.frame.clip.loop(Integer.MAX_VALUE);
        }
    }

    /**
     * changes the resolution, depending on an int.
     * 
     * @param selected represents the index of the combobox that selects the
     *        resolution
     */
    public static void changeRes(int selected) {
   //     System.out.println("res changed to: " + selected);
        switch (selected) {
            case 0:
                StandardFrame.xRes = 1024;
                StandardFrame.yRes = 768;
                break;
            case 1:
                StandardFrame.xRes = 1200;
                StandardFrame.yRes = 900;
                break;
            case 2:
                StandardFrame.xRes = 1400;
                StandardFrame.yRes = 1077;
                break;
            default:
                StandardFrame.xRes = 1024;
                StandardFrame.yRes = 768;
                break;
        }
    }

    /**
     * This method is called in the mainframe, to read the preferences of the
     * user
     */
    public static void readConfig(String fileToRead) {
        try {
            Document dom = XMLRead.parseXMLFile(fileToRead);
            Element element = dom.getDocumentElement();
            colCurrentIndex = Integer.parseInt(element.getAttribute("colCurrentIndex"));
            resCurrentIndex = Integer.parseInt(element.getAttribute("resCurrentIndex"));
            muteSelected = Boolean.parseBoolean(element.getAttribute("muteSelected"));
            int bckRed = Integer.parseInt(element.getAttribute("bckRed"));
            int bckGreen = Integer.parseInt(element.getAttribute("bckGreen"));
            int bckBlue = Integer.parseInt(element.getAttribute("bckBlue"));
            effectVolumeIndex = Integer.parseInt(element.getAttribute("effectVolume"));
            musicVolumeIndex = Integer.parseInt(element.getAttribute("volume"));
            StandardPanel.backGroundColor = new Color(bckRed, bckGreen, bckBlue);
            
            changeRes(resCurrentIndex);
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * reads the defaultSettings from baseConfig.xml to overwrite the current
     * settings.
     */
    public void defaultSettings() {
        readConfig("baseConfig.xml");
        writeConfig();
        MainGui.frame.refresh();
        MainGui.frame.browseTo(this.getClass());
    }

    public static void writeConfig() {
        String fileToWrite = "config.xml";
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder;
            docBuilder = docFactory.newDocumentBuilder();
            Document config = docBuilder.newDocument();
            Element cnfEl = config.createElement("config");

            cnfEl.setAttribute("bckRed", "" + StandardPanel.backGroundColor.getRed());
            cnfEl.setAttribute("bckGreen", "" + StandardPanel.backGroundColor.getGreen());
            cnfEl.setAttribute("bckBlue", "" + StandardPanel.backGroundColor.getBlue());
            cnfEl.setAttribute("colCurrentIndex", "" + colCurrentIndex);
            cnfEl.setAttribute("resCurrentIndex", "" + resCurrentIndex);
            cnfEl.setAttribute("muteSelected", "" + muteSelected);
            cnfEl.setAttribute("volume", "" + StandardFrame.volume);
            cnfEl.setAttribute("effectVolume", "" + StandardFrame.effectVolume);

            config.appendChild(cnfEl);
            XMLWriter.printToFile(config, fileToWrite);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * returns muteSelected
     * 
     * @return
     */
    public static boolean getMuteSelected() {
        return muteSelected;
    }
}
