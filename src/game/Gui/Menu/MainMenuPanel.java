package game.Gui.Menu;

import game.BaseGame;
import game.Competition;
import game.Gui.MainGui;
import game.Gui.StandardPanel;
import game.Gui.gamePanels.NewGamePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * @author Timo
 *
 */
public class MainMenuPanel extends StandardPanel {

    /**
     * Constructor Define new buttons in here
     */
    public MainMenuPanel() {
        //This makes the title
        JLabel title = JTitle("FOOTBAL MANAGER 64", this);
        this.add(title);

        //New Game Button
        JButton btnNewGame = menuButton("Start New Game", this);
        btnNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                BaseGame.setComp(Competition.readCompetition("competition.xml", true, null));
                MainGui.frame.browseTo(NewGamePanel.class);
            }
        });

        //Load Game Button
        JButton btnLoadGame = menuButton("Load Saved Game", this);
        btnLoadGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(LoadGamePanel.class);
            }
        });

        //Options Button
        JButton btnOptions = menuButton("Options", this);
        btnOptions.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(OptionsPanel.class);
            }
        });

        //Instructions Button
        JButton btnInstructions = menuButton("Instructions", this);
        btnInstructions.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(InstructionsPanel.class);
            }
        });

        //About Button
        JButton btnAbout = menuButton("About", this);
        btnAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(AboutPanel.class);
            }
        });

        //Quit Button
        JButton btnQuit = menuButton("Quit", this);
        btnQuit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                System.exit(0);
            }
        });
    }

}
