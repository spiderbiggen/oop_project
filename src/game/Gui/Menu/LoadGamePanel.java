package game.Gui.Menu;

import game.BaseGame;
import game.Gui.MainGui;
import game.Gui.StandardPanel;
import game.Gui.SquadPanels.SquadPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;

/**
 * This is the panel, shown when clicked on the button load game in the main
 * menu Inherited from this, is the GameLoadPanel. The difference between these
 * two panels is that the GameLoadPanel is accesed in-game, and therefore has a
 * 'back to menu button'.
 * 
 * @author sytze
 *
 */
public class LoadGamePanel extends StandardPanel {
    public JButton btnGoBack;
    public JButton btnLoadGame;

    public LoadGamePanel() {
        JTitle("Load Game", this);
        textStandard("Select a file to load");
        String[] savegames = ScanSavegames();

        //add a combobox that contains all the savegames in the savegame folder
        final JComboBox<String> savebox = new JComboBox<>(savegames);
        savebox.setFont(inputFont);
        savebox.setSize(savebox.getPreferredSize());
        savebox.setLocation(getWidth() / 2 - savebox.getWidth() / 2, getHeight() / 3);
        this.add(savebox);

        //add the button which will load the game selected in the loadbox
        btnLoadGame = menuButton("Load game", this);
        btnLoadGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {

                String fileToLoad = (String) savebox.getSelectedItem();

                BaseGame.BaseGameRead("savegames/" + fileToLoad);
                BaseGame.getComp().teamStatsFromMatches();
                BaseGame.getSell().SellPlayerList();
                MainGui.frame.browseTo(SquadPanel.class);
            }
        });
        btnLoadGame.setLocation(getWidth() / 2 - btnLoadGame.getWidth() / 2, getHeight() / 2);
        this.add(btnLoadGame);

        //add a button that will take the user back to the main menu
        btnGoBack = menuButton("Go back to menu", this);
        btnGoBack.setLocation((getWidth() - btnGoBack.getWidth()) / 2, getHeight() * 2 / 3);
        btnGoBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(MainMenuPanel.class);
            }
        });
    }

    /**
     * Scans all save games from the savegames folder
     * 
     * @return all names of the files in the savegames folder
     */
    public static String[] ScanSavegames() {
        ArrayList<String> results = new ArrayList<String>();
        File folder = new File("savegames");
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null && listOfFiles.length > 0) {
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    results.add(file.getName());
                }
            }
        }

        //now we replace the ArrayList to a normal Array, so it can be read by comboboxes
        String[] res = new String[results.size()];
        for (int i = 0; i < results.size(); i++) {
            res[i] = results.get(i);
        }
        return res;
    }
}
