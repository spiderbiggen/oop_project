package game.Gui;

import game.Gui.Menu.MainMenuPanel;
import game.Gui.Menu.OptionsPanel;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * represents the main frame of the appliction
 * 
 * @author sytze
 *
 */
public class StandardFrame extends JFrame {
    public static int   xRes         = 1024;
    public static int   yRes         = 768;
    //these 3 are used for audio volume control
    static FloatControl gainControl;
    static FloatControl gainEffControl;
    public static int   volume       = 10;
    public static int   effectVolume = 10;
    public Clip         clip;
    public Clip         clipEff;

    /**
     * The constructor constructs a StandardFrame, used across the whole
     * application
     */
    public StandardFrame() {

        OptionsPanel.readConfig("Config.xml");
        playSound("music/MusicAndCrowd2.wav");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        OptionsPanel.readConfig("Config.xml");
        refresh();
        //       playSound("music/crowdLong.wav");
        //       playSoundEffect("music/crowd1.wav");
    }

    public static void adjustVolume(float volumeChange) {
        gainControl.setValue(-40 + 20 * (float) Math.log10(volumeChange));
        if (volumeChange == 0) {
            MainGui.frame.clip.stop();
        }
        volume = (int) volumeChange;
    }

    public static void adjustEffectVolume(float volumeChange) {
        gainEffControl.setValue(-40 + 20 * (float) Math.log10(volumeChange));
        if (volumeChange == 0) {
            MainGui.frame.clipEff.stop();
        }
        effectVolume = (int) volumeChange;
    }

    /**
     * browse to a certain panel by getting the right class
     * 
     * @param c should be a class of the panel to browse. For instance, for the
     *        MainMenuPanel this can be obtained by MainMenuPanel.class
     */
    public void browseTo(Class c) {
        try {
            JPanel toBrowse = (JPanel) c.newInstance();
            setContentPane(toBrowse);
        } catch (Exception e) {
            System.out.println("could not browse");
            e.printStackTrace();
        }
    }

    public void playSound(String filename) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filename).getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            adjustVolume(volume);
            clip.loop(Integer.MAX_VALUE);
            if (!OptionsPanel.getMuteSelected()) {
                //          System.out.println("sound resumed");
                clip.loop(Integer.MAX_VALUE);
            } else {
                clip.stop();
                //           System.out.println("sound muted");
            }

            adjustVolume(OptionsPanel.musicVolumeIndex);
        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }

    public void playSoundEffect(String filename) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filename).getAbsoluteFile());
            clipEff = AudioSystem.getClip();
            clipEff.open(audioInputStream);
            if (!OptionsPanel.getMuteSelected()) {
                clipEff.start();
            } else {
                clipEff.stop();
            }
            gainEffControl = (FloatControl) clipEff.getControl(FloatControl.Type.MASTER_GAIN);
            adjustEffectVolume(OptionsPanel.effectVolumeIndex);
        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }

    /**
     * refreshes the frame, to adjust the resolution.
     */
    public void refresh() {
        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(((int) screensize.getWidth() - xRes) / 2, ((int) screensize.getHeight() - yRes) / 2, xRes, yRes);

    }

    /**
     * runs the frame
     */
    public void run() {
        //setBounds(100, 100, 1024, 768);
        browseTo(MainMenuPanel.class);
    }

}
