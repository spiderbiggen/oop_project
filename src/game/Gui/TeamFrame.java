package game.Gui;

import game.Player;
import game.Team;
import game.Gui.SquadPanels.Squad_Tab_Squad.CellRendererCards;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 * @author Timo
 *
 */
public class TeamFrame extends JFrame {

    DefaultTableModel model;
    JTable            table;

    /**
     * @param t
     */
    public TeamFrame(Team t) {
        setSize(800, 490);
        setLocationRelativeTo(null); //centers the window
        setResizable(false);
        model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false except position
                if (column != 1) {
                    return false;
                }
                return true;
            }
          //this makes sure that sorting goes correct
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 1: case 2: case 3: case 4: case 5: case 7:
                        return Float.class;
                    default:
                        return String.class;
                }
            }
        };
        
        /**
         * @author Timo innerclass to set the card colors
         */
        class CellRendererCards extends JLabel implements TableCellRenderer {
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus, int row,
                    int column) {
                switch ((int) value) {
                    case 0:
                        setOpaque(false);
                        break;
                    case 1:
                        setOpaque(true);
                        setBackground(Color.YELLOW);
                        break;
                    default:
                        setOpaque(true);
                        setBackground(Color.RED);
                        break;
                }
                return this;
            }
        }
        
        table = new JTable(model);
        //enables sorting
        table.setRowHeight(20);

        // Create the columns
        model.addColumn("Name");
        model.addColumn("Value");
        model.addColumn("Condition");
        model.addColumn("Offense");
        model.addColumn("Defense");
        model.addColumn("GK Skill");
        model.addColumn("Favorite Side");
        model.addColumn("Aggression");
        model.addColumn("Status");
        model.addColumn("Card");

        //set the name column to a decent width
        table.getColumnModel().getColumn(0).setPreferredWidth(200);
        table.getTableHeader().setReorderingAllowed(false);
        //fill the rows with the players
        for (int i = 0; i < t.getPlayers().size(); i++) {
            Player p = t.getPlayers().get(i);
            String injury = "";
            if (p.getStats().getInjury() > 0)
                injury = "injured";
            model.addRow(new Object[] { p.getName(), p.getValue(), p.getStats().getCondition(), p.getStats().getOffense(), p.getStats().getDefense(), p.getStats().getGkSkill(), p.getStats().getSide(), p.getStats().getAgression(), injury, p.getStats().getCard() });
        }
        //put it all in a scrollpane
        JScrollPane s = new JScrollPane(table);
        this.add(s);
        
        //render the card colors
        TableColumn cardsColumn = table.getColumnModel().getColumn(9);
        CellRendererCards cardrenderer = new CellRendererCards();
        cardsColumn.setCellRenderer(cardrenderer);
        
        //get rid of the frame when focus is lost.
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
                setVisible(false);
                dispose();
            }
        });
    }
}
