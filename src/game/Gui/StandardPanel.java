package game.Gui;

import game.BaseGame;
import game.Competition;
import game.Player;
import game.Stats;
import game.Team;
import game.Gui.gamePanels.endCompetitionPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

/**
 * @author Timo
 *
 */
public abstract class StandardPanel extends JPanel {
    public static Color buttonColor        = Color.orange;
    public static Color backGroundColor    = Color.darkGray;
    public static Color titleColor         = Color.WHITE;
    public static Color textColor          = Color.white;
    public static Font  titleFont          = new Font("Calibri", Font.BOLD, 50);
    public static Font  textFont           = new Font("Calibri", Font.PLAIN, 18);
    public static Font  inputFont          = new Font("Calabri", Font.PLAIN, 18);
    public int          buttonWidth        = 200;
    int                 buttonHeight       = 60;
    int                 nextButtonPosition = MainGui.frame.getHeight() / 6;      //Where the buttons start
    int                 buttonDistance     = MainGui.frame.getHeight() / 8;      //distance between buttons
    JTextField    cheatNameField;
    public static boolean cheat;
    /**
     * Constructor Define new buttons in here
     */
    public StandardPanel() {
        this.setSize(new Dimension(StandardFrame.xRes, StandardFrame.yRes));
        setLayout(null);
        setBackground(backGroundColor);
        cheat = true;
    }

    //From here we implement some buttons, texts and titles we are going to use often, or adjust slightly
    //They can be used easily in the panels that are extensions of the StandardPanel by just calling them

    /**
     * Makes a label text, though this is only one line of text. Should be
     * replaced with textStandard
     * 
     * @param textToAdd
     * @param panelToAdd the panel this component should be added to
     * @return
     */
    public JLabel JText(String textToAdd, JPanel panelToAdd) {
        JLabel res = new JLabel(textToAdd);
        res.setFont(textFont);
        res.setSize(res.getPreferredSize());
        res.setLocation(getWidth() / 2 - res.getWidth() / 2, (getHeight() / 3));
        res.setForeground(titleColor);
        panelToAdd.add(res);
        return res;
    }

    /**
     * Makes a title using the standard font color and size
     * 
     * @param titleText the text the title should show
     * @return a JLabel object representing the title
     */
    public JLabel JTitle(String titleText, JPanel panelToAdd) {
        JLabel res = new JLabel(titleText);
        res.setFont(titleFont);
        res.setSize(res.getPreferredSize());
        res.setLocation(getWidth() / 2 - res.getWidth() / 2, 50);
        res.setForeground(titleColor);
        panelToAdd.add(res);
        return res;
    }

    /**
     * This method represents a button to add to the main menu
     * 
     * @param name the name of the button
     * @param panelToAdd the panel this component should be added to
     * @return a JButton to be added to a certain panel!
     */
    public JButton menuButton(String name, JPanel panelToAdd) {
        JButton res = new JButton(name);
        res.setSize(buttonWidth, buttonHeight);
        panelToAdd.add(res);
        res.setLocation(panelToAdd.getWidth() / 2 - buttonWidth / 2, nextButtonPosition);
        nextButtonPosition += buttonDistance;
        res.setBackground(buttonColor);
        res.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.playSoundEffect("music/kick1.wav");;
            }
        });
        
        
        return res;
    }

    /**
     * This method represents a smaller button Location should be set manual
     * using the setLocation(x,y) method
     * 
     * @param name the name of the button
     * @param panelToAdd the panel this component should be added to
     * @return a JButton to be added to a certain panel!
     */
    public JButton MenuButtonSmall(String name, JPanel panelToAdd) {
        JButton res = new JButton(name);
        res.setSize(buttonWidth / 2, buttonHeight / 2);
        panelToAdd.add(res);
        res.setLocation(panelToAdd.getWidth() / 2 - buttonWidth / 2, nextButtonPosition);
        res.setBackground(buttonColor);
        return res;
    }

    /**
     * An easy way to add text as a Jcomponent using the colour of our
     * application
     * 
     * @param textToAdd
     * @return
     */
    public JTextPane textStandard(String textToAdd) {
        JTextPane res = new JTextPane();
        res.setEditable(false);
        res.setText(textToAdd);
        res.setFont(textFont);
        res.setSize(res.getPreferredSize());
        res.setLocation(getWidth() / 2 - res.getWidth() / 2, (getHeight() / 3));
        res.setBackground(backGroundColor);
        res.setForeground(titleColor);
        return res;
    }
    
    public void cheatVoid(boolean cheatenable){
        if(!cheat){
            return;
        }
        String enterCheat = "Cheat input";
        cheatNameField = new JTextField(enterCheat);
        cheatNameField.setSize(getWidth() / 5, getWidth() / 30);
        cheatNameField.setLocation(getWidth() / 2 - cheatNameField.getWidth() / 2, getHeight() / 4);
        cheatNameField.setFont(inputFont);
        this.add(cheatNameField);
        
        JButton cheatBtn = menuButton("enter cheat", this); 
        cheatBtn.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                resolveCheat(cheatNameField.getText());
    //            MainGui.frame.browseTo(endCompetitionPanel.class);
            }
        });
    }
    
    public void resolveCheat(String cheat){
        switch(cheat){
            case "endcomp":
                MainGui.frame.browseTo(endCompetitionPanel.class);
                break;
            case "demMoneys":
                Team team = BaseGame.getTeam();
                team.setBudget(Integer.MAX_VALUE);
                break;
            case "BigAndy":
                Stats stat = new Stats(10000,10000,10000,10000,0,1,'M',10000,10000,0);
                Player andy = new Player("Andy Zaidman",Integer.MAX_VALUE,"MC",stat);
                if(!BaseGame.getTeam().getPlayers().contains(andy)){
                    BaseGame.getTeam().addPlayer(andy);
                }
                break;
            case "autolineup":
                Competition comp = BaseGame.getComp();
                comp.setLineUpAllTeams();
                BaseGame.setComp(comp);               
                break;
                
            default:
                cheatNameField.setText("unknown cheat");
                break;
                
        }
    }
    
}
