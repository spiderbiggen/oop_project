package game.Gui.gamePanels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import game.*;
import game.Gui.*;
import game.Gui.Menu.*;
import game.Gui.SquadPanels.*;

public class endRankingPanel extends endCompetitionPanel{
    public endRankingPanel(){
        super();
        Competition comp = BaseGame.getComp();
        comp.teamStatsFromMatches();
        BaseGame.setComp(comp);
        Squad_Tab_Competition competitionTab = new Squad_Tab_Competition(this.getWidth()/3*2, this.getHeight()*3/7);
        competitionTab.setLocation(this.getWidth()/2-competitionTab.getWidth()/2, this.getHeight()*2/6);  
        this.add(competitionTab);
        text.setVisible(false);
        Team playerTeam = BaseGame.getTeam();
        text = textStandard("Coach:"+playerTeam.getCoach()+".\nYour team: "+playerTeam.getName()+"."
                + "\nPoints scored: "+playerTeam.getPoints());
        text.setLocation(this.getWidth()/2-text.getWidth()/2, this.getHeight()/10);
        this.add(text);
        title.setVisible(false);
        btnCon.setVisible(false);
        btnCon = menuButton("go to main menu", this);
        btnCon.setLocation(btnCon.getX(), this.getHeight()*5/6);
        btnCon.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                MainGui.frame.browseTo(MainMenuPanel.class);                
            }
        });
        
    }
}
