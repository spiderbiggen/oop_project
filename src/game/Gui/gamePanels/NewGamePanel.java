package game.Gui.gamePanels;

import game.BaseGame;
import game.Competition;
import game.Gui.MainGui;
import game.Gui.StandardPanel;
import game.Gui.Menu.MainMenuPanel;
import game.Gui.SquadPanels.SquadPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class NewGamePanel extends StandardPanel {

    public NewGamePanel() {
        JTitle("New Game", this);
        //adds a TextField for the user to type a coach name
        String enterCoach = "Enter your coach name";
        final JTextField coachNameField = new JTextField(enterCoach);
        coachNameField.setSize(getWidth() / 3, getWidth() / 20);
        coachNameField.setLocation(getWidth() / 2 - coachNameField.getWidth() / 2, getHeight() / 4);
        coachNameField.setFont(inputFont);
        
        this.add(coachNameField);

        //adds a selectbox to select a team
        final JComboBox<String> teamSelectBox = new JComboBox<String>(BaseGame.getTeamNames());
        teamSelectBox.setFont(inputFont);
        teamSelectBox.setSize(getWidth() / 3, getWidth() / 20);
        teamSelectBox.setLocation(getWidth() / 2 - teamSelectBox.getWidth() / 2, getHeight() / 2);
        this.add(teamSelectBox);

        //add a button to go back to the main menu
        JButton btnGoBack = menuButton("Go back to menu", this);
        btnGoBack.setLocation((getWidth() - btnGoBack.getWidth()) / 2, getHeight() * 4 / 4);
        btnGoBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(MainMenuPanel.class);
            }
        });

        //add a button to start the game
        JButton btnStart = menuButton("Start the game!", this);
        btnStart.setLocation((getWidth() - btnStart.getWidth()) / 2, getHeight() * 2 / 3);
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {

                String coach = coachNameField.getText();
                if(!coach.equals("Enter your coach name") && !coach.equals("Enter a valid name") && (coach.length() < 20)){
                    int currentTeamIndex = teamSelectBox.getSelectedIndex();
                    String currentTeam = BaseGame.getComp().getTeams().get(currentTeamIndex).getName();
                BaseGame.setCurrentTeam(currentTeam, coach);
                BaseGame.getComp().roundRobinMatches();
                BaseGame.getComp().setLineUpAllTeams();
                Competition comp = BaseGame.getComp();
                BaseGame.setComp(comp);
                BaseGame.getSell().SellPlayerList();
                MainGui.frame.browseTo(SquadPanel.class);
                }else{
                    coachNameField.setText("Enter a valid name");
                }
            }
        });
    }
}
