package game.Gui.gamePanels;

import game.Gui.MainGui;
import game.Gui.Menu.LoadGamePanel;
import game.Gui.SquadPanels.SquadPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This is the panel, shown when clicked on the button load game in the
 * squad_menupanel Inherited from this, is the GameLoadPanel. The difference
 * between these two panels is that the GameLoadPanel is accesed in-game, and
 * therefore has a 'back to menu button'.
 * 
 * @author sytze
 *
 */
public class GameLoadPanel extends LoadGamePanel {
    public GameLoadPanel() {
        super();
        btnGoBack.setVisible(false);
        btnGoBack = menuButton("back to game", this);
        btnGoBack.setLocation((getWidth() - btnGoBack.getWidth()) / 2, getHeight() * 2 / 3);
        btnGoBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(SquadPanel.class);
            }
        });
    }
}
