package game.Gui.gamePanels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import game.Gui.*;

public class endCompetitionPanel extends StandardPanel {

    JLabel title;
    JTextPane text;
    JButton btnCon;
    
    public endCompetitionPanel(){
        title = JTitle("end of the season", this);
        title.setPreferredSize(title.getPreferredSize());
        text = textStandard("It's the end of the season! You and your players played a long season and now it is time to move on to the next.\n"
                +"Of course, you as a coach are more than happy to see the final rankings.\nClick the button below to see the final scores.");
        this.add(text);
        btnCon = menuButton("Continue", this);
        btnCon.setLocation(this.getWidth()/2-btnCon.getWidth()/2, this.getHeight()/3*2);
        btnCon.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                MainGui.frame.browseTo(endRankingPanel.class);              
            }
        });
    }
}
