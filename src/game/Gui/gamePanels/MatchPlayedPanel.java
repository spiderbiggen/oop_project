package game.Gui.gamePanels;

import game.BaseGame;
import game.Gui.MainGui;
import game.Gui.SquadPanels.SquadPanel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * @author
 *
 */
public class MatchPlayedPanel extends MatchPanel {
    JPanel dataPanel;
    int    nextLine;

    /**
     * 
     */
    public MatchPlayedPanel() {
        super();
        dataPanel = new JPanel();
        dataPanel.setSize(getWidth() * 8 / 9, getHeight() * 4 / 9);
        dataPanel.setLocation(getWidth() / 2 - dataPanel.getWidth() / 2, getHeight() / 3);
        dataPanel.setBackground(backGroundColor);
        dataPanel.setLayout(null);
        addData();
        this.add(dataPanel);

        btnRun.setText("Continue...");
        btnRun.removeActionListener(actionMenu);
        ActionListener actionSquad = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BaseGame.getComp().teamStatsFromMatches();
                //MainGui.frame.refresh();
                BaseGame.getSell().SellPlayerList();
                MainGui.frame.browseTo(SquadPanel.class);
                

            }
        };
        btnRun.addActionListener(actionSquad);
    }

    /**
     * adds 3 text boxes to the dataPanel. These contain all kind of values from
     * the match
     */
    public void addData() {
        //TODO: when match method is more complete, update this method so it contains the correct information

        JComponent leftText = middleText(match.getHomeGoals() + "\n" + match.getHomeShots() + "\n" + match.getHomeYellow() + "\n" + match.getHomeRed() + "\n" + match.getHomeInjury());
        leftText.setLocation(dataPanel.getWidth() / 6 - leftText.getWidth() / 2, 10);
        leftText.setForeground(textColor);
        leftText.setBackground(backGroundColor);
        dataPanel.add(leftText);

        JComponent centerText = middleText(" Goals \n Shots on Goal \n Yellow Cards \n Red Cards \n Injuries");
        centerText.setLocation(dataPanel.getWidth() / 2 - centerText.getWidth() / 2, 10);
        centerText.setForeground(textColor);
        centerText.setBackground(backGroundColor);
        dataPanel.add(centerText);

        JComponent rightText = middleText(match.getAwayGoals() + "\n" + match.getAwayShots() + "\n" + match.getAwayYellow() + "\n" + match.getAwayRed() + "\n" + match.getAwayInjury());
        rightText.setLocation(dataPanel.getWidth() * 5 / 6 - rightText.getWidth() / 2, 10);
        rightText.setForeground(textColor);
        rightText.setBackground(backGroundColor);
        dataPanel.add(rightText);
    }

    /**
     * text used for the dataPanel.
     * 
     * @param text text that should be displayed
     * @return a JComponent text field
     */
    public JComponent middleText(String text) {
        JComponent res = textStandard(text);
        res.setBackground(Color.white);
        res.setFont(inputFont);
        res.setForeground(Color.BLACK);
        res.setSize(res.getPreferredSize());
        return res;
    }
}
