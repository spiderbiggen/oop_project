package game.Gui.gamePanels;

import game.Gui.MainGui;
import game.Gui.Menu.MainMenuPanel;
import game.Gui.Menu.OptionsPanel;
import game.Gui.SquadPanels.SquadPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * This is the options menu that will pop up when the user is in-game. It should
 * contain all the options from the main options panel.
 * 
 * @author sytze
 *
 */
public class GameOptions extends OptionsPanel {
    public GameOptions() {
        super();

        backBtn.removeActionListener(back);
        backBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(SquadPanel.class);
            }
        });
        JButton quit = menuButton("back to main menu", this);
        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.refresh();
                MainGui.frame.browseTo(MainMenuPanel.class);
            }
        });
        quit.setLocation(getWidth() / 2 - buttonWidth / 2, (getHeight() / 5) * 4);

    }
}
