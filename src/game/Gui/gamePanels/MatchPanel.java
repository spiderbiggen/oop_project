package game.Gui.gamePanels;

import game.BaseGame;
import game.Competition;
import game.Match;
import game.Gui.MainGui;
import game.Gui.StandardPanel;
import game.Gui.SquadPanels.SquadPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import javax.swing.JButton;
import javax.swing.JLabel;

public class MatchPanel extends StandardPanel {

    Match          match;
    JButton        btnRun;
    ActionListener actionMenu;
    Competition comp;

    /**
     * 
     */
    public MatchPanel() {

        setLineUpAi();
        getData();
        SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy");
        JTitle(sdf.format(match.getPlayDate()), this);

        //3 text boxes, these display the 2 teams names and the "V.S."
        int vsHeight = getHeight() / 4;
        JLabel teamOneText = JTitle(match.getHomeTeam().getName(), this);
        teamOneText.setLocation(getWidth() / 5 - teamOneText.getWidth() / 2, vsHeight);
        JLabel teamTwoText = JTitle(match.getAwayTeam().getName(), this);
        teamTwoText.setLocation(getWidth() * 4 / 5 - teamTwoText.getWidth() / 2, vsHeight);
        JLabel versus = JTitle("V.S.", this);
        versus.setLocation(getWidth() / 2 - versus.getWidth() / 2, vsHeight);

        JButton btnBack = menuButton("Return to team panel", this);
        btnBack.setLocation(getWidth() / 2 - btnBack.getWidth() / 2, getHeight() * 4 / 5);
        btnBack.setVisible(!match.teamsReady());
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainGui.frame.browseTo(SquadPanel.class);

            }
        };
        btnBack.addActionListener(action);

        btnRun = menuButton("start the match", this);
        btnRun.setLocation(getWidth() / 2 - btnRun.getWidth() / 2, getHeight() * 4 / 5);
        btnRun.setVisible(match.teamsReady());
        actionMenu = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BaseGame.getComp().playMatches(BaseGame.getCurrentDate().getTime() + 2L);
                MainGui.frame.browseTo(MatchPlayedPanel.class);
                BaseGame.setLineUpAllAiTeams();

            }
        };
        btnRun.addActionListener(actionMenu);
    }

    /**
     * get the data of the match that should be played
     */
    public void getData() {
        match = BaseGame.getComp().getMatchByNameAndDate(BaseGame.getTeam().getName(), BaseGame.getCurrentDate());
    }
    
    /**
     * set all the AI teams in the competition to a smart setup
     */
    public void setLineUpAi(){
        comp = BaseGame.getComp();
        comp.setLineUpAllAiTeams();
        BaseGame.setComp(comp);
    }
}
