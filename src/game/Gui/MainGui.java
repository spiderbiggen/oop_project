package game.Gui;

import java.io.File;

public class MainGui {
    public static StandardFrame frame = new StandardFrame();

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                frame.setVisible(true);
                frame.run();

                String dirname = "savegames";
                File dir = new File(dirname);
                //creates the directory if it doesnt exist.
                if (!dir.exists())
                    dir.mkdir();
            }
        });
    }
}
