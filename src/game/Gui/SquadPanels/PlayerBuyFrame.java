package game.Gui.SquadPanels;

import game.BaseGame;
import game.Player;
import game.Team;
import game.Gui.MainGui;
import game.Gui.StandardFrame;
import game.Gui.StandardPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class PlayerBuyFrame extends JFrame {

    JPanel     content;
    JTextField inputText;
    JButton    btnSubmit;
    JButton    btnClose;
    Player     player;
    Team       team;

    public PlayerBuyFrame(Player p, Team t) {
        player = p;
        team = t;
        this.setLocation(MainGui.frame.getX() + StandardFrame.xRes / 5, MainGui.frame.getY() + StandardFrame.yRes / 3);
        this.setSize(400, 300);
        this.setResizable(false);
        setPanelContent();
    }

    public void setPanelContent() {
        DecimalFormat d = new DecimalFormat("�###,###,###.##");
        //adds a text field for the input
        StandardPanel content = new StandardPanel() {
        };
        content.setSize(this.getSize());
        
        //       content.setBackground(StandardPanel.backGroundColor);
        inputText = new JTextField("Place your offer");
        inputText.setFont(StandardPanel.inputFont);
        inputText.setSize(300, inputText.getFont().getSize() + 10);
        inputText.setLocation(content.getWidth() / 2 - inputText.getWidth() / 2, content.getHeight() / 2);
        content.add(inputText);

        //adds a button
        btnSubmit = content.MenuButtonSmall("submit offer", content);
        btnSubmit.setSize(btnSubmit.getPreferredSize());
        btnSubmit.setLocation(content.getWidth() / 4 - btnSubmit.getWidth() / 2, content.getHeight() * 4 / 5);
        btnSubmit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                float offer;
                try {
                    offer = Float.parseFloat(inputText.getText());
                    if (Float.floatToIntBits(offer) < Float.floatToIntBits(BaseGame.getTeam().getBudget())) {
                        if (BaseGame.getSell().Buy(player, offer, team)) {
                            inputText.setText("Your Offer has been accepted");
                            MainGui.frame.browseTo(SquadPanel.class);
                        } else {
                            inputText.setText("Your Offer has not been accepted");
                        }
                        btnSubmit.setEnabled(false);
                    } else {
                        inputText.setText("your offer was higher than your budget");
                    }
                } catch (Exception exc) {
                    inputText.setText("invalid value, please type again");
                }

            }
        });

        btnClose = content.MenuButtonSmall("Close", content);
        btnClose.setSize(btnSubmit.getPreferredSize());
        btnClose.setLocation(content.getWidth() / 4 * 3 - btnClose.getWidth() / 2, content.getHeight() * 4 / 5);
        btnClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        
        //adds a text above the input
        JTextPane text = content.textStandard("You are now bidding on: " + player.getName() + "\nFrom: " + team.getName() + ".\nHis value is: " + d.format(player.getValue()) + "\nYour budget is:" + d.format(BaseGame.getTeam().getBudget()));
        text.setLocation(content.getWidth() / 2 - text.getWidth() / 2, text.getHeight() / 3);
        content.add(text);
        
        if(BaseGame.getSell().getBidPlayers().contains(player)){
            btnSubmit.setEnabled(false);
            inputText.setVisible(false);
            text.setText("You tried to buy: "+ player.getName() +".\nYou already tried to do this."+ ".\nPlease try again after the next match.");
        }

        
        
        this.add(content);
        
      //get rid of the frame when focus is lost.
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
                setVisible(false);
                dispose();
            }
        });
    }
}