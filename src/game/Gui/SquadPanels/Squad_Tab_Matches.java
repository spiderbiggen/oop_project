package game.Gui.SquadPanels;

import game.BaseGame;
import game.Competition;
import game.Match;
import game.Gui.StandardPanel;

import java.awt.Color;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Collections;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 * @author Stefan
 *
 */
public class Squad_Tab_Matches extends JPanel {
    DefaultTableModel model;
    JTable            table;

    /**
     * @param width
     * @param height
     */
    public Squad_Tab_Matches(int width, int height) {
        setSize(width, height);
        setLayout(null);

        //the table model
        model = new DefaultTableModel() {
            //sets the editable status of columns
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        table = new JTable(model)
        {   //this adds a gray color to the team of the player
            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
            {
                Component c = super.prepareRenderer(renderer, row, column);
                setOpaque(true);
                if (model.getValueAt(row, 0).equals(BaseGame.getTeam().getName()) || model.getValueAt(row, 1).equals(BaseGame.getTeam().getName()))
                    c.setBackground(Color.LIGHT_GRAY);
                else
                    c.setBackground(Color.WHITE);
                return c;
            }
        };
        table.getTableHeader().setReorderingAllowed(false);
        table.setRowHeight(20);
        // Create the columns
        model.addColumn("Home");
        model.addColumn("Away");
        model.addColumn("Play Date");
        model.addColumn("Home Goals");
        model.addColumn("Away Goals");

        //Append a row
        Competition c = BaseGame.getComp();
        Collections.sort(c.getMatches());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        for (int i = 0; i < c.getMatches().size(); i++) {
            Match m = c.getMatches().get(i);
            String homegoals = "" + m.getHomeGoals();
            String awaygoals = "" + m.getAwayGoals();
            if (homegoals.equals("-1")) {
                homegoals = "";
                awaygoals = "";
            }
            model.addRow(new Object[] { m.getHomeTeam().getName(), m.getAwayTeam().getName(), sdf.format(m.getPlayDate()), homegoals, awaygoals });
        }

        //this makes the headers visible, and might allow scrolling later
        JScrollPane s = new JScrollPane(table);
        s.getViewport().setBackground(StandardPanel.backGroundColor);
        //small correction to compensate the JTabbedPane occupied space
        s.setBounds(0, 0, width - 3, height - 50);
        this.add(s);

    }

}
