package game.Gui.SquadPanels;

import game.BaseGame;
import game.Competition;
import game.Team;
import game.Gui.StandardPanel;
import game.Gui.TeamFrame;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 * @author Stefan
 *
 */
public class Squad_Tab_Competition extends JPanel {
    DefaultTableModel model;
    JTable            table;

    /**
     * @param width
     * @param height
     */
    public Squad_Tab_Competition(int width, int height) {
        setSize(width, height);
        setLayout(null);

        //the table model, non editable
        model = new DefaultTableModel() {
            //sets the editable status of columns
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            //this makes sure that sorting goes correct
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 1:
                        return String.class;
                    default:
                        return Integer.class;
                }
            }
        };

        table = new JTable(model)
        {   //this adds a gray color to the team of the player
            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
            {
                Component c = super.prepareRenderer(renderer, row, column);
                setOpaque(true);
                if (model.getValueAt(row, 1).equals(BaseGame.getTeam().getName()))
                    c.setBackground(Color.LIGHT_GRAY);
                else
                    c.setBackground(Color.WHITE);
                return c;
            }
        };
        table.getTableHeader().setReorderingAllowed(false);
        //enables sorting
        table.setAutoCreateRowSorter(true);
        table.setRowHeight(20);
        // Create the columns
        model.addColumn("Rank");
        model.addColumn("Team");
        model.addColumn("Played");
        model.addColumn("Won");
        model.addColumn("Loss");
        model.addColumn("Draw");
        model.addColumn("Points");
        model.addColumn("Goals For");
        model.addColumn("Goals Against");
        model.addColumn("Goal Difference");
        table.getColumnModel().getColumn(1).setPreferredWidth(200);

        //Append a row
        Competition c = BaseGame.getComp();
        Collections.sort(c.getTeams());
        for (int i = 0; i < c.getTeams().size(); i++) {
            Team t = c.getTeams().get(i);
            model.addRow(new Object[] { i + 1, t.getName(), t.getPlayed(), t.getWon(), t.getLost(), t.getPlayed() - t.getWon() - t.getLost(), t.getPoints(), t.getGoalsFor(), t.getGoalsAgainst(), t.getGoalsFor() - t.getGoalsAgainst() });
        }

        //this makes the headers visible, and might allow scrolling later
        JScrollPane s = new JScrollPane(table);
        s.getViewport().setBackground(StandardPanel.backGroundColor);
        //small correction to compensate the JTabbedPane occupied space
        s.setBounds(0, 0, width - 3, height - 50);
        this.add(s);

        table.addMouseListener(new MouseAdapter() {
            //set the hand-cursor to show that the table is clickable
            @Override
            public void mouseEntered(MouseEvent e) {
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }

            //return the normal cursor
            @Override
            public void mouseExited(MouseEvent e) {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

                /*
                 * public void onMouseOver(MouseEvent e){ setCursor
                 * (Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)); }
                 */
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int row = table.getSelectedRow();
                    row = table.convertRowIndexToModel(row);
                    Team t = BaseGame.getComp().getTeamByName((String) model.getValueAt(row, 1));
                    TeamFrame teamframe = new TeamFrame(t);
                    teamframe.setVisible(true);

                }
            }
        });
    }
}
