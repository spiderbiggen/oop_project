package game.Gui.SquadPanels;

import game.BaseGame;
import game.Player;
import game.Team;
import game.Gui.StandardPanel;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 * @author Timo
 *
 */
public class Squad_Tab_Squad extends JPanel implements TableModelListener {
    DefaultTableModel model;
    JTable            table;
    int nextLabelY = 0;
    JLabel goalkeepers;
    JLabel offense;
    JLabel midfield;
    JLabel defense;
    JLabel substitutes;
    JLabel reserves;

    /**
     * @author Timo innerclass to set the correct values of the combobox
     */
    public class CellRendererPosition extends JComboBox implements
            TableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column) {
            setSelectedItem(value);
            setBackground(Color.white);
            setUI(new BasicComboBoxUI());
            return this;
        }
    }

    /**
     * @author Timo innerclass to set the card colors
     */
    public static class CellRendererCards extends JLabel implements TableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column) {
            switch ((int) value) {
                case 0:
                    setOpaque(false);
                    break;
                case 1:
                    setOpaque(true);
                    setBackground(Color.YELLOW);
                    break;
                default:
                    setOpaque(true);
                    setBackground(Color.RED);
                    break;
            }
            return this;
        }
    }

    /**
     * @param width the width of the panel
     * @param height the height of the panel
     */
    public Squad_Tab_Squad(int width, int height) {
        setSize(width, height);
        setLayout(null);
        Team t = BaseGame.getTeam();

        //the table model
        model = new DefaultTableModel() {
            //sets the editable status of columns
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column != 1) {
                    return false;
                }
                return true;
            }
            //this makes sure that sorting goes correct
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 2:
                        return Float.class;
                    case 3: case 4: case 5: case 6: case 7: case 8: case 11:
                        return Integer.class;
                    case 9:
                        return Character.class;
                    default:
                        return String.class;
                }
            }
        };
        table = new JTable(model);
        table.getTableHeader().setReorderingAllowed(false);
        //enables sorting
        table.setAutoCreateRowSorter(true);
        table.setRowHeight(20);
        
        // Create the columns
        model.addColumn("Name");
        model.addColumn("Position");
        model.addColumn("Value");
        model.addColumn("Condition");
        model.addColumn("Offense");
        model.addColumn("Defense");
        model.addColumn("GK Skill");
        model.addColumn("Stamina");
        model.addColumn("Aggression");
        model.addColumn("Favorite Side");
        model.addColumn("Status");
        model.addColumn("Card");

        //set the name and position column to a decent width
        table.getColumnModel().getColumn(0).setPreferredWidth(200);
        table.getColumnModel().getColumn(1).setPreferredWidth(175);

        //add the rows
        for (int i = 0; i < t.getPlayers().size(); i++) {
            Player p = t.getPlayers().get(i);
            String position = Player.abbreviationToPosition(p.getCurrentPosition());
            String injury = "";
            if (p.getStats().getInjury() > 0)
                injury = "injured";
            model.addRow(new Object[] { p.getName(), position, p.getValue(), p.getStats().getCondition(), p.getStats().getOffense(), p.getStats().getDefense(), p.getStats().getGkSkill(), p.getStats().getStamina(), p.getStats().getAgression(), p.getStats().getSide(), injury, p.getStats().getCard() });
        }

        //this makes the headers visible, and allows scrolling
        JScrollPane s = new JScrollPane(table);
        s.getViewport().setBackground(StandardPanel.backGroundColor);
        //small correction to compensate the JTabbedPane occupied space
        s.setBounds(0, 100, width - 3, height - 150);
        this.add(s);
        
        addLabel("Goalkeepers:", 0);
        addLabel("Offense:", 0);
        addLabel("Midfield:", 0);
        addLabel("Defense:", 0);
        addLabel("Substites:", 0);
        addLabel("Reserves:", 0);
        
        nextLabelY = 0;
        
        goalkeepers = addLabel("0", 100);
        offense = addLabel("0", 100);
        midfield = addLabel("0", 100);
        defense = addLabel("0", 100);
        substitutes = addLabel("0", 100);
        reserves = addLabel("0", 100);
        
        UpdateLabels();
        
        
        //makes the positioncolumn editable with a combobox
        TableColumn positionColumn = table.getColumnModel().getColumn(1);
        String[] items = { "Goalkeeper", "Defense left", "Defense center", "Defense right", "Midfield left", "Midfield center", "Midfield right", "Offense left", "Offense right", "Offense center", "Substitute", "Reserve" };
        JComboBox<String> comboBoxPosition = new JComboBox<String>(items);
        comboBoxPosition.setBackground(Color.white);
        comboBoxPosition.setUI(new BasicComboBoxUI());
        positionColumn.setCellEditor(new DefaultCellEditor(comboBoxPosition));

        //render the combobox in the cells
        DefaultComboBoxModel dmodel = new DefaultComboBoxModel(items);
        CellRendererPosition renderer = new CellRendererPosition();
        renderer.setModel(dmodel);
        positionColumn.setCellRenderer(renderer);

        //render the card colors
        TableColumn cardsColumn = table.getColumnModel().getColumn(11);
        CellRendererCards cardrenderer = new CellRendererCards();
        cardsColumn.setCellRenderer(cardrenderer);
        
        //adds listener to the table
        model.addTableModelListener(this);
    }
    
    /**
     * update all the labels with the amount of players in that position
     */
    public void UpdateLabels (){
        ArrayList<Integer> totals = BaseGame.getTeam().getTotalPositions();
        setLabelGoalkeepers(totals.get(0));
        setLabelOffense(totals.get(1));
        setLabelMidfield(totals.get(2));
        setLabelDefense(totals.get(3));
        setLabelSubstitutes(totals.get(4));
        setLabelReserves(totals.get(5));
    }
    
    /**
     * @param title value the label should display
     * @param x x position the label should be in
     * @return the JLabel created
     */
    public JLabel addLabel(String title, int x){
        JLabel lab = new JLabel(title, SwingConstants.LEFT);
        lab.setBounds(x, nextLabelY, 100, 20);
        add(lab);
        nextLabelY+=20;
        return lab;
    }
    
    /**
     * @param x the amount of goalkeepers
     */
    public void setLabelGoalkeepers(int x){
        goalkeepers.setText(""+x);
    }
    
    /**
     * @param x the amount of offense players
     */
    public void setLabelOffense(int x){
        offense.setText(""+x);
    }
    
    /**
     * @param x the amount of midfield players
     */
    public void setLabelMidfield(int x){
        midfield.setText(""+x);
    }
    
    /**
     * @param x the amount of defense players
     */
    public void setLabelDefense(int x){
        defense.setText(""+x);
    }
    
    /**
     * @param x the amount of substitutes
     */
    public void setLabelSubstitutes(int x){
        substitutes.setText(""+x);
    }
    
    /**
     * @param x the amount of reserves
     */
    public void setLabelReserves(int x){
        reserves.setText(""+x);
    }
    

    @Override
    public void tableChanged(TableModelEvent e) {
        //get the selected row
        int row = e.getFirstRow();
        //get the player
        Player p = BaseGame.getTeam().getPlayerByName((String) model.getValueAt(row, 0));
        //change the current position
        if (p != null)
            p.setCurrentPosition(Player.positionToAbbreviation((String) model.getValueAt(row, 1)));
        UpdateLabels();
    }
}
