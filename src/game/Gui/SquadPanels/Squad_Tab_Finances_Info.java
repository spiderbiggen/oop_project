package game.Gui.SquadPanels;

import game.BaseGame;
import game.Gui.StandardPanel;

import java.text.DecimalFormat;

import javax.swing.JLabel;

public class Squad_Tab_Finances_Info extends StandardPanel {

    public Squad_Tab_Finances_Info(int width,int height) {
        setSize(width, height);
    //    JTitle("Budget:", this);
        JLabel coach = JTitle("Coach: " + BaseGame.getTeam().getCoach(),this);        
        
        //adds a text
        DecimalFormat d = new DecimalFormat("�###,###,###.##");
        JLabel budget = JTitle("Budget: " + d.format(BaseGame.getTeam().getBudget()), this);
        budget.setSize(budget.getPreferredSize());
        budget.setLocation(getWidth() / 2 - budget.getWidth() / 2, getHeight() / 5);
        
        
     
    }
}