package game.Gui.SquadPanels;

import game.Gui.StandardPanel;

/**
 * @author Timo
 *
 */
public class SquadPanel extends StandardPanel {

    int positionYdevision = getHeight() / 7;
    int positionXdevision = getWidth() / 5 * 4;

    /**
     * Constructor
     */
    public SquadPanel() {
        //adds the menu to the top
        Squad_MenuPanel smp = new Squad_MenuPanel(getWidth(), positionYdevision);
        this.add(smp);
        //adds the panel with tabs
        Squad_TabsPanel stp = new Squad_TabsPanel(getWidth()-5, getHeight() - positionYdevision);
        this.add(stp);
        stp.setLocation(0, positionYdevision);
    }

}
