package game.Gui.SquadPanels;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;

/**
 * @author Timo
 *
 */
public class Squad_TabsPanel extends SquadSubPanel {

    /**
     * @param width
     * @param height
     */
    public Squad_TabsPanel(int width, int height) {
        super(width, height);
        setOpaque(false);  //transparant background
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setSize(getWidth(), getHeight());

        JComponent panel1 = new Squad_Tab_Squad(width, height);
        tabbedPane.addTab("Squad", panel1);
        JComponent panel2 = new Squad_Tab_BuyPlayers(width, height);
        tabbedPane.addTab("Buy Players", panel2);
        JComponent panel3 = new Squad_Tab_SellPlayers(width, height);
        tabbedPane.addTab("Sell Players", panel3);
        JComponent panel4 = new Squad_Tab_Competition(width, height);
        tabbedPane.addTab("View Competition", panel4);
        JComponent panel5 = new Squad_Tab_Matches(width, height);
        tabbedPane.addTab("Upcoming Matches", panel5);
        JComponent panel6 = new Squad_Tab_Finances_Info(width, height);
        tabbedPane.addTab("Finances and Info", panel6);
        
        //Add the JTabbedPane to the Panel
        add(tabbedPane);
        //Enables scrolling
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

    }
}
