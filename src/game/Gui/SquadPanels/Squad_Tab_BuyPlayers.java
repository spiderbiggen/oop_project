package game.Gui.SquadPanels;

import game.BaseGame;
import game.Competition;
import game.Player;
import game.Team;
import game.Gui.StandardPanel;
import game.Gui.SquadPanels.Squad_Tab_Squad.CellRendererCards;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 * 
 * @author Iwan
 *
 */
public class Squad_Tab_BuyPlayers extends JPanel {
    DefaultTableModel model;
    JTable            table;

    public Squad_Tab_BuyPlayers(int width, int height) {
        setSize(width, height);
        setLayout(null);

        //the table model, non editable
        model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            //this makes sure that sorting goes correct
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 2:
                        return Float.class;
                    case 3:
                    case 4:
                    case 5:
                    case 7:
                    case 8:
                    case 11:
                        return Integer.class;
                    case 9:
                        return Character.class;
                    default:
                        return String.class;
                }
            }
        };

        table = new JTable(model);
        table.getTableHeader().setReorderingAllowed(false);
        //enables sorting
        table.setAutoCreateRowSorter(true);
        table.setRowHeight(20);
        //create the columns
        model.addColumn("Name");
        model.addColumn("Team");
        model.addColumn("Value");
        model.addColumn("Condition");
        model.addColumn("Offense");
        model.addColumn("Defense");
        model.addColumn("GK Skill");
        model.addColumn("Stamina");
        model.addColumn("Aggression");
        model.addColumn("Favorite Side");
        model.addColumn("Status");
        model.addColumn("Card");

        //append a row
        Competition c = BaseGame.getComp();
        for (int i = 0; i < c.getTeams().size(); i++) {
            Team t = c.getTeams().get(i);
            if (!t.equals(BaseGame.getTeam())) {
                for (int j = 0; j < t.getPlayers().size(); j++) {
                    Player p = t.getPlayers().get(j);
                    String injury = "";
                    if (p.getStats().getInjury() > 0)
                        injury = "injured";
                    model.addRow(new Object[] { p.getName(), t.getName(), p.getValue(), p.getStats().getCondition(), p.getStats().getOffense(), p.getStats().getDefense(), p.getStats().getGkSkill(), p.getStats().getStamina(), p.getStats().getAgression(), p.getStats().getSide(), injury, p.getStats().getCard() });
                }
            }
        }

        //this makes the headers visible, and might allow scrolling later
        JScrollPane s = new JScrollPane(table);
        s.getViewport().setBackground(StandardPanel.backGroundColor);
        //small correction to compensate the JTabbedPane occupied space
        s.setBounds(0, 0, width - 3, height - 50);
        this.add(s);

        //set the name and position column to a decent width
        table.getColumnModel().getColumn(0).setPreferredWidth(200);
        table.getColumnModel().getColumn(1).setPreferredWidth(200);

        //popups player clicked on to do a offer
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int row = table.getSelectedRow();
                    row = table.convertRowIndexToModel(row);
                    Team t = BaseGame.getComp().getTeamByName((String) model.getValueAt(row, 1));
                    Player p = t.getPlayerByName((String) model.getValueAt(row, 0));
                    JFrame playerBuyFrame = new PlayerBuyFrame(p, t);
                    playerBuyFrame.setVisible(true);

                }
            }

            //set the hand-cursor to show that the table is clickable
            @Override
            public void mouseEntered(MouseEvent e) {
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }

            //return the normal cursor
            @Override
            public void mouseExited(MouseEvent e) {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }

        });

        TableColumn cardsColumn = table.getColumnModel().getColumn(11);
        CellRendererCards cardrenderer = new Squad_Tab_Squad.CellRendererCards();
        cardsColumn.setCellRenderer(cardrenderer);
    }
}
