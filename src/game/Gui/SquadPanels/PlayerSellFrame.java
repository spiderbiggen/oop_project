package game.Gui.SquadPanels;

import game.BaseGame;
import game.Player;
import game.Team;
import game.Gui.MainGui;
import game.Gui.StandardFrame;
import game.Gui.StandardPanel;
import game.ai.BuySellAlgorithm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class PlayerSellFrame extends JFrame {

    JPanel     content;
    JTextField inputText;
    JButton    btnAccept;
    JButton    btnDeny;
    Player     player;
    Team       team;
    float      bid;
    int        index;

    /**
     * @param i
     */
    public PlayerSellFrame(int i) {
        BuySellAlgorithm sell = BaseGame.getSell();
        player = sell.getPlayers().get(i);
        team = sell.getTeams().get(i);
        this.bid = sell.getBids().get(i);
        index = i;
        this.setLocation(MainGui.frame.getX() + StandardFrame.xRes / 5, MainGui.frame.getY() + StandardFrame.yRes / 3);
        this.setSize(400, 300);
        this.setResizable(false);
        setPanelContent();
    }

    public void setPanelContent() {
        final DecimalFormat d = new DecimalFormat("�###,###,###.##");
        //adds a text field for the input
        StandardPanel content = new StandardPanel() {
        };
        content.setSize(this.getSize());

        //adds a text above the input
        final JTextPane text = content.textStandard("Do you want to sell: " + player.getName() + "\nTo: " + team.getName() + "\nFor: � " + (int)bid + "\nHis value is: � " + (int)player.getValue());
        text.setLocation(content.getWidth() / 2 - text.getWidth() / 2, text.getHeight() / 3);
        content.add(text);

        //adds a button
        btnDeny = content.MenuButtonSmall("Close", content);
        btnDeny.setSize(btnDeny.getPreferredSize());
        btnDeny.setLocation(content.getWidth() / 4 * 3 - btnDeny.getWidth() / 2, content.getHeight() * 4 / 5);
        btnDeny.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });

        //adds a button
        btnAccept = content.MenuButtonSmall("Yes", content);
        btnAccept.setSize(btnDeny.getPreferredSize());
        btnAccept.setLocation(content.getWidth() / 4 - btnAccept.getWidth() / 2, content.getHeight() * 4 / 5);
        btnAccept.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (BaseGame.getSell().SellPlayer(index)) {
                    BaseGame.getSell().remove(index);
                    btnAccept.setVisible(false);
                    btnDeny.setText("Close");
                    MainGui.frame.browseTo(SquadPanel.class);
                    text.setText("You have sold " + player.getName() + "\nTo: " + team.getName() + "\nFor: " + d.format(bid) + "\nYour budget is now: " + d.format(BaseGame.getTeam().getBudget()));
                }
            }
        });
        btnDeny.setText("No");
        this.add(content);
        //get rid of the frame when focus is lost.
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
                setVisible(false);
                dispose();
            }
        });

    }
}
