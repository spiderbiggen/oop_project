package game.Gui.SquadPanels;

import game.Gui.StandardPanel;

public class SquadSubPanel extends StandardPanel {
    int width;
    int height;

    public SquadSubPanel(int width, int height) {
        setBackground(backGroundColor);
        this.width = width;
        this.height = height;
        this.setSize(this.width, this.height);
        setLayout(null);
    }
}
