package game.Gui.SquadPanels;

import game.BaseGame;
import game.Gui.MainGui;
import game.Gui.Menu.SaveGamePanel;
import game.Gui.gamePanels.GameLoadPanel;
import game.Gui.gamePanels.GameOptions;
import game.Gui.gamePanels.MatchPanel;
import game.Gui.gamePanels.endCompetitionPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 * @author Timo
 *
 */
public class Squad_MenuPanel extends SquadSubPanel {

    int buttonWidth;
    int buttonHeight        = 30;
    int buttonPositionY     = 0;
    int nextButtonPositionX = 50;

    /**
     * Constructor
     * 
     * @param width the width of the panel
     * @param height the height of the panel
     */
    public Squad_MenuPanel(int width, int height) {
        super(width, height);
        buttonPositionY = (this.height - buttonHeight) / 2;

        SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy");
        String currentDate = sdf.format(BaseGame.getCurrentDate());
        JTitle(currentDate, this);

        //save game button
        JButton saveBtn = addButton("Save");
        saveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(SaveGamePanel.class);
            }
        });
        //load game button
        JButton loadBtn = addButton("Load");
        loadBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(GameLoadPanel.class);
            }
        });
        //options button
        JButton optionsBtn = addButton("Options");
        optionsBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                MainGui.frame.browseTo(GameOptions.class);
            }
        });
        
        JButton btnNextMatch = menuButton("Go to next match!", this);
        btnNextMatch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(BaseGame.getTeam().validatePlayerPositions()){
                if (BaseGame.getTeam().validateLineUp()) {
                    //checks if the competition is over
                    if(BaseGame.getComp().getUnplayedMatches() > 0){
                        BaseGame.getCal().add(Calendar.DATE, 7);
                        MainGui.frame.browseTo(MatchPanel.class);
                        
                    }else{
                        MainGui.frame.browseTo(endCompetitionPanel.class);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Your squad is not correct.\nMake sure you have:\n10 fieldplayers\n1 goalkeeper\nmaximal 7 substitutes");
                }
                }
                else
                    JOptionPane.showMessageDialog(null, "Players with Red cards or injurys in your line-up");
            }
        });
        btnNextMatch.setLocation(getWidth()-btnNextMatch.getWidth() - 100, buttonPositionY);
    }

    /**
     * @param name
     * @return
     */
    public JButton addButton(String name) {
        JButton res = MenuButtonSmall(name, this);
        res.setSize(res.getPreferredSize());
        res.setLocation(nextButtonPositionX, buttonPositionY);
        buttonWidth = res.getWidth();
        nextButtonPositionX += buttonWidth;
        return res;
    }
}
