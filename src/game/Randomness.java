package game;

import java.util.Random;

/**
 * @author Yuri, Daan
 *
 */
public class Randomness {

    private static int noTest = 0;

    /**
     * @return generates a random number between 0 and 100
     */
    public static int randomNumber() {
        if (noTest == 0) {
            int n = 0;

            Random randomGenerator = new Random();
            n = randomGenerator.nextInt(100);
            return n;

        } else if (noTest == 50)
            return 50;

        else if (noTest == 25)
            return 25;
        
        else if (noTest == 100)
            return 100;
        
        else if(noTest == 5)
            return 5;
        
        else if(noTest == 75)
            return 75;
        
        else if(noTest == 1)
            return 1;
        
        else
            return 0;
    }

    /**
     * @return returns the noTest
     */
    public static int getNoTest() {
        return noTest;
    }

    /**
     * @param noTest sets the NoTest
     */
    public static void setNoTest(int noTest) {
        Randomness.noTest = noTest;
    }

}