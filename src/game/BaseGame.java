package game;

import game.ai.BuySellAlgorithm;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author Stefan
 *
 */
public class BaseGame {
    //private static Date             CurrentDate;
    private static Competition      comp;
    private static String           teamName;
    private static BuySellAlgorithm sell = new BuySellAlgorithm();
    private static Calendar         cal  = Calendar.getInstance();

    /**
     * Reads a save game from the location in fileToRead starting in the
     * savegames folder
     * 
     * @param fileToRead path to save game
     */
    public static void BaseGameRead(String fileToRead) {
        try {
            Document dom = XMLRead.parseXMLFile(fileToRead);
            Element element = dom.getDocumentElement();
            NodeList nl = element.getElementsByTagName("competition");
            comp = Competition.readCompetition(null, false, (Element) nl.item(0));
            nl = element.getElementsByTagName("matches");
            comp.setMatches(Competition.readMatches(null, false, (Element) nl.item(0)));
            setCurrentTeam(teamName, comp.getTeamByName(teamName).getCoach());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the game to the location in fileToWrite starting in the savegames
     * folder
     * 
     * @param fileToWrite path to save game
     */
    public static void BaseGameWrite(String fileToWrite) {

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder;
            docBuilder = docFactory.newDocumentBuilder();
            Document save = docBuilder.newDocument();
            Element saveGame = save.createElement("save");
            save.appendChild(saveGame);
            comp.writeCompetition(null, false, save, saveGame);
            comp.writeMatches(null, false, save, saveGame);
            XMLWriter.printToFile(save, fileToWrite);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main {@link Competition} Object
     * 
     * @return the {@link Competition}
     */
    public static Competition getComp() {
        return comp;
    }

    /**
     * Returns the main {@link Date} Object
     * 
     * @return the currentDate
     */
    public static Date getCurrentDate() {
        return cal.getTime();
    }

    /**
     * @return the player's team
     */
    public static Team getTeam() {
        return comp.getTeamByName(teamName);
    }

    /**
     * @return the teamName
     */
    public static String getTeamName() {
        return teamName;
    }

    /**
     * Creates a String[] of team names
     * 
     * @return StringArray of team names in correct order
     */
    public static String[] getTeamNames() {
        ArrayList<String> teamNames = new ArrayList<String>();
        for (int i = 0; i < comp.getTeams().size(); i++) {
            teamNames.add(comp.getTeams().get(i).getName());
        }
        return teamNames.toArray(new String[teamNames.size()]);
    }

    /**
     * Gets the first team in the competition mostly used for testing
     * 
     * @return {@link Team}
     */
    public static Team getTestTeam() {
        comp = Competition.readCompetition("competition.xml", true, null);
        return comp.getTeams().get(0);
    }

    /**
     * @return the sell
     */
    public static BuySellAlgorithm getSell() {
        return sell;
    }

    /**
     * @param sell the sell to set
     */
    public static void setSell(BuySellAlgorithm sell) {
        BaseGame.sell = sell;
    }

    /**
     * Sets the main Competition to this comp
     * 
     * @param comp the {@link Competition} to set as the main competition
     */
    public static void setComp(Competition comp) {
        BaseGame.comp = comp;
    }

    /**
     * @param currentDate the currentDate to set
     */
    public static void setCurrentDate(Date currentDate) {
        cal.setTime(currentDate);
    }

    /**
     * @return Calendar
     */
    public static Calendar getCal(){
        return cal;
    }
    
    /**
     * Used for new Game to select the players team and change it's coach
     * 
     * @param currentTeam the name of a team in {@link Competition}
     * @param coach The name of the coach that the player wanted
     */
    public static void setCurrentTeam(String currentTeam, String coach) {
        setTeamName(currentTeam);
        Team team = comp.getTeamByName(currentTeam);
        int i = comp.getTeams().indexOf(team);
        team.setCoach(coach);
        comp.getTeams().set(i, team);
    }

    /**
     * @param teamName the teamName to set
     */
    public static void setTeamName(String teamName) {
        BaseGame.teamName = teamName;
    }

    /**
     * sets the lineup for all teams, by using the comp.setLineUpAllTeams()
     * method
     */
    public static void setLineUpAllTeams() {
        comp.setLineUpAllTeams();
    }

    /**
     * sets the lineup for all teams, by using the comp.setLineUpAllTeams()
     * method
     */
    public static void setLineUpAllAiTeams() {
        comp.setLineUpAllAiTeams();
    }
}
